/**
 * @license
 * Copyright (c) 2018 amCharts (Antanas Marcelionis, Martynas Majeris)
 *
 * This sofware is provided under multiple licenses. Please see below for
 * links to appropriate usage.
 *
 * Free amCharts linkware license. Details and conditions:
 * https://github.com/amcharts/amcharts4/blob/master/LICENSE
 *
 * One of the amCharts commercial licenses. Details and pricing:
 * https://www.amcharts.com/online-store/
 * https://www.amcharts.com/online-store/licenses-explained/
 *
 * If in doubt, contact amCharts at contact@amcharts.com
 *
 * PLEASE DO NOT REMOVE THIS COPYRIGHT NOTICE.
 * @hidden
 */
am4internal_webpackJsonp(["f6f5"], {
    JjGr: function(e, t, c) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var s = c("01H4"),
            n = c("8ZqG"),
            i = function(e) {
                Object(s.a)(e, "ColorSet") && (e.list = [Object(n.c)("#EC7063"),
                										Object(n.c)("#AF7AC5"), 
                										Object(n.c)("#5DADE2"), 
                										Object(n.c)("#48C9B0"), 
                										Object(n.c)("#F4D03F"), 
                										Object(n.c)("#E67E22"), 
                										Object(n.c)("#A6ACAF"), 
                										Object(n.c)("#1A5276"), 
                										Object(n.c)("#142c6d")], e.reuse = !1, e.stepOptions = {
                    lightness: .05,
                    hue: 0
                }, e.passOptions = {})
            };
        window.am4themes_dataviz = i
    }
}, ["JjGr"]);//# sourceMappingURL=dataviz.js.map
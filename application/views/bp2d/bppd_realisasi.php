<!DOCTYPE html>

<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
    
    <!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
               <!-- MENU SIDEBAR -->
               <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
    
                <!-- Row -->
  
 

<div class="col-md-12" id="chart_opening">
  <CENTER><h1 id="header_page_1">TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG</h1></CENTER>
  <br><br>

        <!-- Row -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">REALISASI DAN TARGET PAJAK</h4>
                                    <div class="ml-auto">
                                    </div>
                                </div>
                            </div>
                           <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                      <thead id="t_head">
                        
                      </thead>
                      <tbody id="t_body">
                        
                      </tbody>
                        
                    </table>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                 <div class="d-flex no-block">
                                    <h4 class="card-title">GRAFIK PERBANDINGAN REALISASI DAN TARGET PAJAK</h4>
                                    
                                </div>
                                <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                            <tr>
                                <td>
                                    <div id="chartdiv_jenis" style="height: 500px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>


            <!-- /.box-body -->
    </center>
</div>

<div class="col-md-12" id="chart_opening">
    <center>
        <div class="col-md-12">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_all" style="height: 1050px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </center>
</div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.init.js") ?>"></script>



    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>



     <script type="text/javascript">
         function surya(id){
            console.log(id);

            $("#surya").modal('show');
         }
     </script>

 <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        var data_json_perbandingan = JSON.parse('<?php print_r($data_json_perbandingan);?>');
        $(document).ready(function(){
          set_all_data();
          set_graph_all(data_main);
          set_graph_bulat(data_json_perbandingan[0]);
        });

        function currency(x){
          return x.toLocaleString('us-EG');
        }


        function set_all_data(){
          $("#header_page_1").html("TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG");

          var title_header = data_main.title;

          var header_table = "";

          header_table += "<tr>"+
              "<th>No.</th>"+
              "<th>Jenis Pajak</th>"+
              "<th>Target</th>"+
              "<th>Realisasi</th>";

          
          header_table += "</tr>";

          $("#t_head").html(header_table);

          var str_body = "";
          var t_target = 0;
          var t_realisasi = 0;
          for (let element_item in data_main) {
            // console.log(data_main[element_item]);
            if(element_item == 0){
              str_body += "<tr><td>"+(parseInt(element_item)+1)+"</td>"+
                "<td>"+
                "<input type=\"radio\" checked=\"\" name=\"choose\" id=\"choose_"+element_item+"\" value=\""+element_item+"\" onclick=\"set_graph('"+element_item+"')\">"+
                "<label for=\"choose_"+element_item+"\">&nbsp;"+data_main[element_item].jenis_pajak+"</label></td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].target))+"</td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].realisasi))+"</td></tr>";
            }else{
              str_body += "<tr><td>"+(parseInt(element_item)+1)+"</td>"+
                "<td>"+
                "<input type=\"radio\" name=\"choose\" id=\"choose_"+element_item+"\" value=\""+element_item+"\" onclick=\"set_graph('"+element_item+"')\">"+
                "<label for=\"choose_"+element_item+"\">&nbsp;"+data_main[element_item].jenis_pajak+"</label></td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].target))+"</td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].realisasi))+"</td></tr>";
            }
            

            t_target += parseFloat(data_main[element_item].target);
            t_realisasi += parseFloat(data_main[element_item].realisasi);
          }

          // str_body += "<tr><td colspan='2'><b>Total</b></td><td align=\"right\">Rp. "+currency(t_target)+"</td><td align=\"right\">Rp. "+currency(t_realisasi)+"</td></tr>";


          $("#t_body").html(str_body);
        }

        function set_graph_all(data_val){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_dataviz);
          am4core.useTheme(am4themes_animated);
          am4core.useTheme(am4themes_myTheme);
          // Themes end

           // Create chart instance
          var chart = am4core.create("chartdiv_all", am4charts.XYChart);

          // Add data
          chart.data = data_val;

          // Create axes
          var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "jenis_pajak";
          categoryAxis.numberFormatter.numberFormat = "#";
          categoryAxis.renderer.inversed = true;
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.cellStartLocation = 0.1;
          categoryAxis.renderer.cellEndLocation = 0.9;

          var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
          valueAxis.renderer.opposite = true;

          // Create series
          function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "jenis_pajak";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueX}";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            categoryLabel.label.text = "{name}";
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
          }

          createSeries("realisasi", "Realisasi");
          createSeries("target", "Target");

          });
        }

        function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          // warna biru
          am4core.color("#00BFFF"),
            
          // warna orange
          am4core.color("#FF8C00"), 

          // warna hijau
          am4core.color("#32CD32")
        ];
      }
    }

        function set_graph_bulat(data_val){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_dataviz);
          am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_jenis", am4charts.XYChart);
          chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

          chart.data = data_val;

          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.dataFields.category = "title";
          categoryAxis.renderer.minGridDistance = 40;
          categoryAxis.fontSize = 11;
          categoryAxis.renderer.labels.template.dy = 5;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.min = 0;
          valueAxis.renderer.minGridDistance = 30;
          valueAxis.renderer.baseGrid.disabled = true;


          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.categoryX = "title";
          series.dataFields.valueY = "val";
          series.columns.template.tooltipText = "{valueY.value}";
          series.columns.template.tooltipY = 0;
          series.columns.template.strokeOpacity = 0;

          // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
          series.columns.template.adapter.add("fill", function(fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
          });

          });
        }

        function set_graph(param){

          // $("#choose_"+param).prop("checked", true);
          // data_main
          $("#grafik_detail").html("GRAFIK PERBANDINGAN REALISASI DAN TARGET PAJAK");

          set_graph_bulat(data_json_perbandingan[param]);
          // console.log("ok");
        }

    </script>
<?php if (isset($script_plus)) echo $script_plus ?>

    </body>
</html>


  <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="http://localhost/malang/Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                         <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="http://localhost/malang/halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                <li><a href="http://localhost/malang/pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="http://localhost/malang/halaman_kependudukan" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kependudukan</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="http://localhost/malang/bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="http://localhost/malang/index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="http://localhost/malang/show_report/Showpdam/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="http://localhost/malang/show_report/Showpdam/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="http://localhost/malang/show_report/Showpdam/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="http://localhost/malang/show_report/Showpdam/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                          <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="http://localhost/malang/show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="http://localhost/malang/show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                               
                            </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
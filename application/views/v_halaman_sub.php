<!DOCTYPE html>

<html lang="en"><!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://rawgithub.com/darkskyapp/skycons/master/skycons.js"></script>
<style type="text/css">
    /* Style all font awesome icons */

/* Add a hover effect if you want */
.fa:hover {
  opacity: 0.7;
}

/* Set a specific color for each brand */

/* Facebook */
.fa-facebook {
  background: #3B5998;
  color: white;
   padding: 20px;
  font-size: 30px;
  width: 100px;
 /* height: 100px;*/
  text-align: center;
  text-decoration: none;
}

/* Twitter */
.fa-twitter {
  background: #55ACEE;
  color: white;
   padding: 20px;
  font-size: 30px;
  width: 100px;
 /* height: 100px;*/
  text-align: center;
  text-decoration: none;
}
.fa-instagram {
  background: #f40083;
  color: white;
   padding: 20px;
  font-size: 30px;
  width: 100px;
 /* height: 100px;*/
  text-align: center;
  text-decoration: none;
}
.fa-youtube {
  background: #bb0000;
  color: white;
   padding: 20px;
  font-size: 30px;
  width: 100px;
 /* height: 100px;*/
  text-align: center;
  text-decoration: none;
}
.fa-android {
  background: #a4c639;
  color: white;
   padding: 20px;
  font-size: 30px;
  width: 100px;
 /* height: 100px;*/
  text-align: center;
  text-decoration: none;
}

</style>
    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php print_r(base_url());?>index.php/Halaman_utama">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
           <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                       <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>   
                         </ul>
                        </li>
                         <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">874890</h3>
                                    <h6 class="card-subtitle">Jumlah Pertumbuhan Penduduk Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-success"></i></h2>
                                    <h3 class="">431483</h3>
                                    <h6 class="card-subtitle">Jumlah Penduduk Jenis Kelamin Pria</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-purple"></i></h2>
                                    <h3 class="">443407</h3>
                                    <h6 class="card-subtitle">Jumlah Penduduk Jenis Kelamin Wanita</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class="">145.28 km<sup>2</sup> (56.09 sq mi)</h3>
                                    <h6 class="card-subtitle">Luas Wilayah</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    
                       
                    </div>
                       <div class="row">
                    <!-- column -->
                    <div class="col-lg-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                   <div class="col-12" style="height: 630px;">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126438.28548094159!2d112.56174241362163!3d-7.978639465299347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd62822063dc2fb%3A0x78879446481a4da2!2sMalang%2C%20Kota%20Malang%2C%20Jawa%20Timur!5e0!3m2!1sid!2sid!4v1575609356735!5m2!1sid!2sid" class="card-img-top img-responsive"  frameborder="0" style="border:0; height: 630px; " allowfullscreen=""></iframe>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                   <div class="col-lg-5">
                       <div class="card">
                       
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">Weather Report</h4>
                                    <div class="ml-auto">
                                        <select class="custom-select">
                                            <option selected="">Today</option>
                                            <option value="1">Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex  p-l-20 bg-light align-items-center flex-row ">
                                <div class="p-2 display-5 text-info">
                                    <canvas class="partly-cloudy-day" width="44" height="44"></canvas> <span>31<sup>°</sup></span></div>
                                <div class="p-2">
                                    <h3 class="m-b-0"><?php echo date('l, d-m-Y');?></h3><small>Kota Malang, Indonesia</small></div>
                            </div>
                            <div class="card-body">
                                <table class="table no-border lite-padding">
                                    <tbody>
                                        <tr>
                                            <td>Luas Wilayah</td>
                                            <td class="font-medium">145.28 km <sup>2</sup></td>
                                        </tr>
                                        <tr>
                                            <td>Titik tertinggi</td>
                                            <td class="font-medium">667 m</td>
                                        </tr>
                                        <tr>
                                            <td>Titik terendah</td>
                                            <td class="font-medium">440 m</td>
                                        </tr>
                                        <tr>
                                            <td>Total Kepadatan Penduduk</td>
                                            <td class="font-medium">6.200 km <sup>2</sup></td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Penduduk</td>
                                            <td class="font-medium">874890</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <ul class="list-unstyled row text-center city-weather-days">
                                    <li class="col">
                                        <canvas class="clear-day" width="30" height="30"></canvas><span>09:30</span>
                                        <h3>27<sup>°</sup></h3></li>
                                    <li class="col">
                                        <canvas class="partly-cloudy-day" width="30" height="30"></canvas><span>11:30</span>
                                        <h3>32<sup>°</sup></h3></li>
                                    <li class="col">
                                        <canvas class="wind" width="30" height="30"></canvas><span>13:30</span>
                                        <h3>31<sup>°</sup></h3></li>
                                    <li class="col">
                                        <canvas class="sleet" width="30" height="30"></canvas><span>15:30</span>
                                        <h3>27<sup>°</sup></h3></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Social media  -->
                        <a href="https://www.facebook.com/malangkota.go.id/" class="fa fa-facebook"></a>
                        <a href="https://twitter.com/PemkotMalang" class="fa fa-twitter"></a>
                        <a href="https://www.instagram.com/pemkotmalang/?hl=id" class="fa fa-instagram"></a>
                        <a href="https://www.youtube.com/user/mediacentermalang" class="fa fa-youtube"></a>
                        <a href="https://play.google.com/store/apps/developer?id=Dinas+Komunikasi+dan+Informatika+Kota+Malang&hl=in" class="fa fa-android"></a>
                    </div>

                    <!-- column -->
                </div>
                <!-- Row -->
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Dashboard Kota Malang by Kominfo </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts yach-->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>

    </body>
</html>



<!doctype html>
<html>

<head>
    <title>Line Chart</title>
    <script src="<?php print_r(base_url());?>assets/chartjs/Chart.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/chartjs/utils.js"></script>

    <!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>

<body>
    <div>
        <label>Pilih data mana yang akan di pilih</label>
        <select id="j_data" name="j_data">
            
        </select>

        <br><br>

        <label>Kategori</label>
        <select id="kategori" name="kategori">
            
        </select>

        <br><br>

        <label>Jenis Kategori</label>
        <select id="j_kategori" name="j_kategori">
            
        </select>

        <br><br>
    </div>
    <div style="width:100%;" id="total_div">
        <!-- <canvas id="canvas"></canvas> -->
    </div>

    <!-- <div>
        <div id="chart" style="width:100%; height: 500px;"></div>
    </div> -->
    <br>
    <br>
    <td align="right"></td>

    
    
    <!-- <?php print_r($str_tbl);?> -->
    

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_json_select = JSON.parse('<?php print_r($data_json_select);?>');

        // console.log(data_json);
        // console.log(data_json_select);

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};


        function load_first(){
            var param = $("#j_data").val();
            var param_kategori = $("#kategori").val();
            var param_j_kategori = $("#j_kategori").val();

            config = {};
            for (let item_canvas in array_chart_div) {
                var tmp_config = {
                    type: 'line',
                    data: {
                        labels: MONTHS,
                        datasets: [{
                            label: 'Penduduk Laki-Laki',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: data_json["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"]["blimbing"]["l"],
                            fill: false,
                        }, {
                            label: 'Penduduk Perempuan',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: data_json["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"]["blimbing"]["p"],
                        }, {
                            label: 'Penduduk Perempuan dan Laki-Laki',
                            fill: false,
                            backgroundColor: window.chartColors[2],
                            borderColor: window.chartColors[2],
                            data: data_json["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"]["blimbing"]["lp"],
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Pertumbuhan Penduduk Menurut Jenis '+title_chart[item_canvas]+' Pada Tahun <?php print_r($periode_th);?>'
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode, <?php print_r($periode_th);?>'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_"+array_chart_div[item_canvas]).getContext('2d');
            
                window.myLine = new Chart(ctx, config[tmp_config]);
                window.myLine.update();
            }
        }

        $(document).ready(function(){
            create_j_data();

            var param = $("#j_data").val();
            create_kategori(param);

            var param_kategori = $("#kategori").val();
            create_j_kategori(param, param_kategori);

            create_canvas();
            load_first();

            create_chart();
            // get_chart_new();
        });

        $("#kategori").change(function(){
            var param = $("#j_data").val();

            var param_kategori = $("#kategori").val();
            create_j_kategori(param, param_kategori);
            create_canvas();
            load_first();

            create_chart();
        });

        $("#j_data").change(function(){
            var param = $("#j_data").val();
            create_kategori(param);

            var param_kategori = $("#kategori").val();
            create_j_kategori(param, param_kategori);
            create_canvas();
            load_first();

            create_chart();
        });


        $("#j_kategori").change(function(){
            create_canvas();
            load_first();

            create_chart();
            // get_chart_new();
        });

        function create_canvas(){
            var param = $("#j_data").val();
            var param_kategori = $("#kategori").val();
            var param_j_kategori = $("#j_kategori").val();

            var total_div_str;

            var check_type_data = typeof data_json_select[param]["item"][param_kategori]["item"][param_j_kategori];

            // console.log(data_json_select[param]["item"][param_kategori]["item"][param_j_kategori]);

            var data = data_json_select[param]["item"][param_kategori]["item"][param_j_kategori];

            
            if(check_type_data == "object"){
                for (let item in data) {
                    var check_type_item = typeof data[item];

                    total_div_str = "";
                    array_chart_div = [];
                    if(check_type_item == "object"){
                        for (let sub_item in data[item]) {
                            total_div_str += "<canvas id=\"canvas_"+sub_item+"\"><p></p></canvas>";

                            array_chart_div.push(sub_item);
                            title_chart.push(data[item][sub_item]);
                        }
                    }
                    else{
                        total_div_str = "<canvas id=\"canvas_"+param_j_kategori+"\">"+param_j_kategori+"</canvas>"; 
                        array_chart_div.push(param_j_kategori);
                        // console.log(array_chart_div); 
                    }
                }
            }
            console.log(total_div_str);

            $("#total_div").html(total_div_str);
        }

        function create_chart(){
            var param = $("#j_data").val();
            var param_kategori = $("#kategori").val();
            var param_j_kategori = $("#j_kategori").val();

            var data = data_json[param][param_kategori][param_j_kategori];
            var data_label = data_json_select[param]["item"][param_kategori]["item"][param_j_kategori];
            // console.log(data_label);

            for (let item_canvas in array_chart_div) {
                var color_line = 0;
                var data_set = [];
                for (let item in data) {

                    var tmp_dataset = {
                        label: item,
                        backgroundColor: window.chartColors[color_line],
                        borderColor: window.chartColors[color_line],
                        data: data[item][array_chart_div[item_canvas]],
                        fill: false,
                    };

                    data_set[color_line] = tmp_dataset;

                    color_line++;
                }
                var ctx = document.getElementById("canvas_"+array_chart_div[item_canvas]).getContext('2d');

                config[item_canvas]["data"]["datasets"] = data_set;       
                window.myLine = new Chart(ctx, config[item_canvas]);
                window.myLine.update();
                // console.log(data_set);
            }
        }

        function create_j_data(){
            var str_option = "";
            for (let element_item in data_json_select) {
                str_option += "<option value=\""+element_item+"\">"+data_json_select[element_item]["keterangan"]+"</option>";
            }
            $("#j_data").html(str_option);
        }

        function create_kategori(param){
            var str_option = "";
            for (let element_item in data_json_select[param].item) {
                str_option += "<option value=\""+element_item+"\">"+data_json_select[param].item[element_item]["keterangan"]+"</option>";
            }
            $("#kategori").html(str_option);
        }

        function create_j_kategori(param, param_kategori){
            var data_select = data_json_select[param].item[param_kategori].item;
            var str_option = "";
            for (let element_item in data_select) {
                str_option += "<option value=\""+element_item+"\">"+element_item+"</option>";
            }
            $("#j_kategori").html(str_option);
        }

        
    </script>
</body>

</html>

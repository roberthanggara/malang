
<html>

<head>
    <title>Line Chart</title>
    <script src="<?php print_r(base_url());?>assets/chartjs/Chart.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/chartjs/utils.js"></script>

    <!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>
<?php
    $th_first = $this->uri->segment(4);
    $th_finish = $this->uri->segment(5);
?>

<body>
    
    <input type="number" name="th_first" id="th_first"> &nbsp;&nbsp;&nbsp;
    <input type="number" name="th_finish" id="th_finish">
    <br>
    <button type="button" id="next" name="next">Next</button>
    <br><br>
    <div>
        <label>Kecamatan</label>
        <select id="kecamatan" name="kecamatan">
            
        </select>
        

        <label>Jenis Data</label>
        <select id="jenis_data" name="jenis_data">
            
        </select>
        

        <label>Pilih data mana yang akan di pilih</label>
        <select id="j_data" name="j_data">
            
        </select>

        <br><br>

        <label>Kategori</label>
        <select id="kategori" name="kategori">
            
        </select>

        <br><br>

        <label>Jenis Kategori</label>
        <select id="j_kategori" name="j_kategori">
            
        </select>

        <br><br>
    </div>
    <div style="width:100%;" id="total_div">
        
    </div>

    <!-- <div>
        <div id="chart" style="width:100%; height: 500px;"></div>
    </div> -->
    <br>
    <br>
    <td align="right"></td>

    
    
    <!-- <?php print_r($str_tbl);?> -->
    

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_json_select = JSON.parse('<?php print_r($data_json_select);?>');
        var data_label = JSON.parse('<?php print_r($label);?>');

        var list_kecamatan = [
                                    {"id":"blimbing", "ket":"KEC. Blimbing"},
                                    {"id":"kedung_kandang", "ket":"KEC. Kedungkandang"},
                                    {"id":"klojen", "ket":"KEC. Klojen"},
                                    {"id":"lowokwaru", "ket":"KEC. Lowokwaru"},
                                    {"id":"sukun", "ket":"KEC. Sukun"}
                                ];

        console.log(data_json.agama["agama"].ISLAM.blimbing.ISLAM);
        console.log(data_json_select);

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        $(document).ready(function(){
            // console.log(data_label);
            // console.log(data_json_select);
            console.log(data_json);
            set_val_th();

            create_op_kec();
            create_jenis();

            create_canvas();

            get_data();
            


        });

        $("#next").click(function(){
            var th_first = $("#th_first").val();
            var th_finish = $("#th_finish").val();

            window.location.href = "<?php print_r(base_url());?>show_report/showdispenduknew/get_data/"+th_first+"/"+th_finish;
        });


        function create_op_kec(){
            var str_op_kec = "";
            for (let i in list_kecamatan) {
                str_op_kec += "<option value=\""+list_kecamatan[i].id+"\">"+list_kecamatan[i].ket+"</option>";
            }

            $("#kecamatan").html(str_op_kec);
        }


        function create_jenis(){
            var str_jenis_data = "";
            for (let i in data_json_select.agama.item.agama.item) {
                // console.log(i);
                str_jenis_data += "<option value=\""+i+"\">"+i+"</option>";
            }

            $("#jenis_data").html(str_jenis_data);
        }

        $("#kecamatan").change(function(){
            create_canvas();
            get_data();
        });

       

        function get_data(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();

            var agama_islam = data_json.agama["agama"].ISLAM[kecamatan].ISLAM;
            var agama_kristen = data_json.agama["agama"].KRISTEN[kecamatan].KRISTEN;
            var agama_katolik = data_json.agama["agama"].KATHOLIK[kecamatan].KATHOLIK;
            var agama_hindu = data_json.agama["agama"].HINDU[kecamatan].HINDU;
            var agama_budha = data_json.agama["agama"].BUDHA[kecamatan].BUDHA;
            var agama_konghucu = data_json.agama["agama"].KONGHUCHU[kecamatan].KONGHUCHU;
            var agama_kepercayaan = data_json.agama["agama"].PENGHAYAT_KEPERCAYAAN[kecamatan].KEPERCAYAAN;
            

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk Beragama ISLAM',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: agama_islam,
                            fill: false,
                        }, {
                            label: 'Penduduk Beragama PROTESTAN',
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: agama_kristen,
                            fill: false,
                        },{
                            label: 'Penduduk Beragama KATHOLIK',
                            backgroundColor: window.chartColors[2],
                            borderColor: window.chartColors[2],
                            data: agama_katolik,
                            fill: false,
                        },{
                            label: 'Penduduk Beragama BUDHA',
                            backgroundColor: window.chartColors[3],
                            borderColor: window.chartColors[3],
                            data: agama_budha,
                            fill: false,
                        },{
                            label: 'Penduduk Beragama HINDU',
                            backgroundColor: window.chartColors[4],
                            borderColor: window.chartColors[4],
                            data: agama_hindu,
                            fill: false,
                        },{
                            label: 'Penduduk Beragama KONGHUCHU',
                            backgroundColor: window.chartColors[5],
                            borderColor: window.chartColors[5],
                            data: agama_konghucu,
                            fill: false,
                        },{
                            label: 'Penduduk Beragama PENGHAYAT KEPERCAYAAN',
                            backgroundColor: window.chartColors[6],
                            borderColor: window.chartColors[6],
                            data: agama_kepercayaan,
                            fill: false,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Jumlah Penduduk Berdasarkan Agama Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_agama").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function get_data_pindah_datang(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            var lahir_l = data_json.lampid[jenis_data].PENDATANG_BULAN_INI[kecamatan].l;
            var lahir_p = data_json.lampid[jenis_data].PENDATANG_BULAN_INI[kecamatan].p;
            var lahir_lp = data_json.lampid[jenis_data].PENDATANG_BULAN_INI[kecamatan].lp;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk Laki-Laki',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: lahir_l,
                            fill: false,
                        }, {
                            label: 'Penduduk Perempuan',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: lahir_p,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Pertumbuhan Penduduk Menurut (DATANG) Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_pendatang").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function get_data_mati(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            var lahir_l = data_json.lampid[jenis_data].MATI_BULAN_INI[kecamatan].l;
            var lahir_p = data_json.lampid[jenis_data].MATI_BULAN_INI[kecamatan].p;
            var lahir_lp = data_json.lampid[jenis_data].MATI_BULAN_INI[kecamatan].lp;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk Laki-Laki',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: lahir_l,
                            fill: false,
                        }, {
                            label: 'Penduduk Perempuan',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: lahir_p,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Pertumbuhan Penduduk Menurut (Kematian) Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_mati").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function get_data_pindah(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            var lahir_l = data_json.lampid[jenis_data].PINDAH_BULAN_INI[kecamatan].l;
            var lahir_p = data_json.lampid[jenis_data].PINDAH_BULAN_INI[kecamatan].p;
            var lahir_lp = data_json.lampid[jenis_data].PINDAH_BULAN_INI[kecamatan].lp;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk Laki-Laki',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: lahir_l,
                            fill: false,
                        }, {
                            label: 'Penduduk Perempuan',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: lahir_p,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Pertumbuhan Penduduk Menurut (Pindah) Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_pindah").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function get_data_akhir_bln(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            var lahir_l = data_json.lampid[jenis_data].PENDUDUK_AKHIR_BULAN_INI[kecamatan].l;
            var lahir_p = data_json.lampid[jenis_data].PENDUDUK_AKHIR_BULAN_INI[kecamatan].p;
            var lahir_lp = data_json.lampid[jenis_data].PENDUDUK_AKHIR_BULAN_INI[kecamatan].lp;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk Laki-Laki',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: lahir_l,
                            fill: false,
                        }, {
                            label: 'Penduduk Perempuan',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: lahir_p,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Jumlah Penduduk Kota Malang Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_akhir_bulan").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function create_canvas(){
            var str_canvas = "<canvas id=\"canvas_agama\"></canvas>";

            $("#total_div").html(str_canvas);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            $("#th_first").val(th_first);
            $("#th_finish").val(th_finish);
        }

        
    </script>
</body>

</html>

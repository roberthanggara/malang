<!DOCTYPE html>
<?php
    $th_first = $this->uri->segment(4);
    // $th_finish = $this->uri->segment(5);
?>
<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">

      
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
              <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                       <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">874890</h3>
                                    <h6 class="card-subtitle">Pertumbuhan Penduduk Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-success"></i></h2>
                                    <h3 class="">431483</h3>
                                    <h6 class="card-subtitle">Jumlah Penduduk Jenis Kelamin Pria</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-purple"></i></h2>
                                    <h3 class="">443407</h3>
                                    <h6 class="card-subtitle">Jumlah Penduduk Jenis Kelamin Wanita</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class="">145.28 km<sup>2</sup> (56.09 sq mi)</h3>
                                    <h6 class="card-subtitle">Luas Wilayah</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-12 m-t-30">
                        <div class="card">
                            <div class="card-body collapse show">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="card-title">Data Kependudukan Kota Malang</h4>
                                        <p class="card-text">Menurut Zona Kecamatan di Kota Malang</p>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="number" class="form-control" name="th_first" id="th_first">
                                            </div>
                                            <div class="col-md-7">
                                                <select class="form-control custom-select" id="filter_jenis" name="filter_jenis">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" id="next" name="next" class="btn waves-effect waves-light btn-info">
                                                    Get data
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                            
                                <div class="mapdiv">

                                <svg   version="1.2" viewbox="0 0 1000 1745" xmlns="http://www.w3.org/2000/svg">
                                    <a class="mapdiv5" xlink:title="LOWOKWARU" xlink:href="https://www.google.lk/search?source=hp&ei=57w4WvPEKIOe0gTO47CADA&q=Kandy" href="#LOWOKWARU" onclick="get_data('lowokwaru')">
                                        <path d="M298.55 199.83l5.86-45.2 47.71 45.48 27.34 29 12.28-16.74 30.13 10.88-15.34 30.15 5 40.18-8.37 36.83-11.44 36.83h-17l-10.88-12.56-3.63 22.6-8.93 20.09L327 382.31s-10-44.36-10-38.92-5.86-12.14-5.86-12.14l-31-6.7v-36l12.3-41.55-6.17-21.21 1.4-42.69" fill="#fd3366" />
                                        
                                    </a>

                                    <a class="mapdiv1" xlink:title="SUKUN" xlink:href="https://www.google.lk/search?source=hp&ei=57w4WvPEKIOe0gTO47CADA&q=Matale" href="#SUKUN" onclick="get_data('sukun')">
                                    <path d="M363.84 532.15l-19.25-14.23-10.88-4.19v-7.53l-17.58-5.86v-12.56l-18.42-10.88v-8.37h8.37l12.56-10-5-7.53 5.86-11.72-8.37-15.07-13.39 25.95s-5.86 5-8.37 5-15.07 14.23-15.07 14.23l-7.53-10.88 5-10.88-5-14.23-20.93-16.74h-9.21l-7.53-5 4.19-9.21-4.19-10 8.37-9.21 7.53-21.02-4.21-14.22-5 14.23-.84 10-11.72-9.21-14.23-4.43h-11.74V348l5.86-12.56v-6.7L189.73 322l-7.53 9.21-15.9 2.51-9.21-5L147 322l14.23-18.42 16.74 9 1.67 3.52 10-3.52h5L204 317l10.88 5 15.07.84 7.53-10.21-7.53-9 3.35-5-39.34-16.74 7.53-10 13.39 5.86 10-5.86 8.37 8.37 5-8.37h5.86L250 266h5.44v-7.53L250 247l-9.21-9.47h-5l-5-6.72 3.35-7.53 4.19-12.56.84-6.7-11.77-11.72-8.37 5-10-.84-8.42 2.54-4.19-6.7 2.51-16.74h5.86l3.91-7.25 7-13.67 51.06 15.9 19.53 55.25 7.81.56 2.23 10-5.33 14.54 1.67 5-5 11.72 8.37 1.67-4.19 20.09 4.19 6.7 7.53 16.57-6.7 6 14.23-.84h11.72l13.39-5.19 2.51 18.58-5.86 6.7-5 5.44h7.53l1.67 15.21 6.7 7-5.86 12.56 4.19 10 9.21-4.19-2.51-11.72 5.86-4.19 4.19-9.49 5-15.21 10.88 8.79-4.19 6.42h12.17v95.7l9 48.55z" fill="#01e680" />
                           
                                    </a>

                                    <a class="mapdiv2" xlink:title="KLOJEN" href="#KLOJEN" onclick="get_data('klojen')">
                                       <path d="M639.52 312.67l-8.37 33.09-10 20.09-10.6 16.74L596 407.14l-3.35 2.79h-17.87v8.37s.56 15.63 0 14-29-6.14-29-6.14l2.79 21.76 2.79 18.42-5.6 16.66v9.49l14 12.84-2.79 5h-3.35l-3.91 12.84-19 12.84-11.16 22.32-14.55-2.75-13.39 11.16-8.37 14.51v18.42h-22l-13.11-10-13.39 14-2.79-3.91v-6.14l-5 .56-7.86 3.82-16.74-7.81-10.6-1.67-4.46-6.7-12.56.56v-12.29l-3.07-8.37 3.07-15.63-3.07-6.7-2.79-5-5-2.23v-19.55l5.58-9.49 5.3-5.58 7-12.28 1.12-10.6-8.09-10.6v-30.69l-3.07-11.16 3.07-10v-3.35l-7.53 1.67 7.53-24 5.3-20.09 6.14-17.86 5.58-16.74L436.95 353l24.27-109.94 23.07-5.56 17.39 9.5 9.49 5-4.46 15.07 10-2.79h6.14l7.81-8.37 14.51 10.6 8.93 1.12 8.93 2.79-3.35 14.51 8.93 1.12 10 3.35 7.25-6.14 8.93-1.12 3.35-2.23 3.91 5 12.28-2.23 6.7 2.23-2.23 11.16-1.61 8.93-.56 4.46 8.93.56z" fill="#fda601" />
                           
                                    </a>


                                    <a class="mapdiv3" xlink:title="KEDUNGKANDANG" xlink:href="" href="#KEDUNGKANDANG" onclick="get_data('kedung_kandang')">
                                        <path fill="#d526f5" d="M411.83 49.99l-5.58 11.72-8.93-2.79-5.58 19.53 10.05 24-10.05 62.5 5.03 31.81 7.25-.56-12.28 24.56 17.3 7.81-1.67 5.58-5.58 2.23 5.02 2.79-5.02 7.81v14.51l3.35 35.16-7.26 20.65-6.14 14.51 15.07 11.16 27.34 15.62 5.59-12.27 11.71-13.4-5.58-8.37 10.61-21.2v-22.88l4.46-2.79 6.14-12.84 6.7-1.67v-10.05l7.81-13.39 2.7-2.21 3.44-2.81 6.14 2.23v-13.39l5.02-3.35 2.23-8.37h5.02l4.47 2.23v-12.28l-2.79-8.37 2.79-8.37 3.9-8.93-5.58-7.25 10.61-11.16 2.79 5.02-2.79 5.02 5.58 5.02v-8.92l5.58-1.68v-4.46l-2.79-2.79-1.12-2.79-9.49-8.93-3.34-7.81 5.02-1.12v-4.47l-5.02-3.34 2.79-6.7-6.7-9.49-3.91-7.81-1.67-5.02-.56-13.4v-3.9l2.79 2.79 1.12-5.02-20.09-11.16-24-13.4-12.83-6.14 2.79-6.14-5.58-3.9-11.72-5.02.55-6.14h-8.92l-16.19 23.99z" />
                           
                                    </a>

                                     <a class="mapdiv4" xlink:title="BLIMBING" xlink:href="https://www.google.lk/search?source=hp&ei=57w4WvPEKIOe0gTO47CADA&q=Batticaloa" href="#BLIMBING" onclick="get_data('blimbing')">
                                      <path d="M116.07 117.8l8.07-2 23.73 6.2 4.19-4.19 5.86 4.19H178l15.9-4.19h19.25l10.9 4.19h6.7l5.86-4.19v-7l-3.91-7.25v-2.79l-3.35-3.91.56-5h6.7l2.79-10.6V74l-5.58-6.14-5-4.46-6.14-2.79-9.49 2.23 2.23-10.6-.56-5.58h-4.46l-5-5.58.56-4.46h6.7l.56 6.14h3.91l2.23 6.7h4.46l6.14 11.16 1.67-4.46 7.81.56v8.93l6.7-1.12 3.86-8.96 11.16 2.79 10 3.91 12.28 1.12 4.46-5.58-2.19-7.81 2.23-2.79 8.93 1.67 7.82 1.12 8.93-2.23L322 36.6l7.25 4.46 9.49-7.25 6.7-6.7v-5l5.56-3.92 9.49-5.58h10l7.81 5.58L389 29.9l8.37 8.93L404 45l7.81 5-3.35 6.7-2.23 5-3.91 1.12s-3.32 9.5-3.32 7.82v1.67l3.91 5.58L409 84l-6.7 10 5 3.91 4.46-2.23 3.91 3.35V103l6.14 2.79v5l-4.4 6.21-3.91 11.16-5-3.91-5.58 14-5.58 16.74-3.34 3.82 5.58 3.91 2.23 6.14v9.49h-3.35l2.79 5v10l2.75 2.85-3.91 11.72-3.91 10.6-3.32 9.48-5 9.49-8.37-8.37-10.6-3.35-1.12-8.37-7.25 1.12.56-7.25-8.93-11.16-6.15 2.23-6.14-2.23-5-8.37-6.14-1.67-3.91-6.7-10-4.46-6.7-4.46-2.79 12.84v12.84l15.07 9.49L332 218l-3.35 5.58 3.91 9.49-9.49 14-15.63-8.93-7.25-3.91-3.91 2.23-2.23-10-7.81-.56-6.7.56-.54-11.28-4.46-12.28-5.58 5 3.35-7.81-3.31-8.91v-3.35l-6.7 5.58-5.58-.56-2.79-5 3.35-6.14-1.67-7.25-4.46 4.46-5.61 1.08-1.12-7.81-5 1.12L231 175l-8.93-2.23-13.37-4.47-11.7-5.58-1.67 5.58-7.81-5.58-2.79 3.91-9.49-5-5.58-2.23s-7.25-2.79-7.25-4.46-5-6.14-5-6.14l-5-.56-7.25-1.12-6.7-9.49-3.91 3.91-8.93-7.81-6.7.56-7.25-4.46v-7.25z" fill="#16baf7" />
                          
                                    </a>

                                    <circle cx="302.6" cy="1147.4" id="0">
                                    </circle>
                                    <circle cx="616.8" cy="745.3" id="1">
                                    </circle>
                                    <circle cx="149.2" cy="439.8" id="2">
                                    </circle>
                                </svg>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->


                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><small></small></h4>
                                <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="surya" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg" style="margin-left:150px;">
                                        <div class="modal-content" style="width:170%;">
                                            <div class="modal-header">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Kecamatan Kedungkandang</h4>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select class="form-control custom-select" id="filter_bln" name="filter_bln">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control custom-select" id="filter_kategori" name="filter_kategori"></select>
                                                        </div>
                                                        <!-- <div class="col-md-3">
                                                            <select class="form-control custom-select" id="filter_kecamatan" name="filter_kecamatan">
                                                            </select>
                                                        </div> -->
                                                        <div class="col-md-2">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row" id="out_body">
                                                    
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <h4>Sumber : Dinas Kependudukan Kota Malang</h4>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9">
                        <div class="card">
                           
                        </div>
                    </div>
                    <!-- <div id="chart_LAHIR_BULAN_INI"></div> -->
                  
                </div>
                <!-- Row -->
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 Admin Press Admin by themedesigner.in </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

    <script src="<?php print_r(base_url())?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/themes/dark.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/themes/animated.js"></script>

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_label = JSON.parse('<?php print_r($label);?>');

        var id_global;

        var list_kecamatan = [
                                    {"id":"blimbing", "ket":"KEC. Blimbing"},
                                    {"id":"kedung_kandang", "ket":"KEC. Kedungkandang"},
                                    {"id":"klojen", "ket":"KEC. Klojen"},
                                    {"id":"lowokwaru", "ket":"KEC. Lowokwaru"},
                                    {"id":"sukun", "ket":"KEC. Sukun"}
                                ];

        var list_kategori = {
                "LAHIR_BULAN_INI":"Berdasarkan Kelahiran", 
                "MATI_BULAN_INI":"Berdasarkan Kematian", 
                "PENDATANG_BULAN_INI":"Berdasarkan Jumlah Pendatang", 
                "PENDUDUK_AKHIR_BULAN_INI":"Penduduk Akhir Bulan ini", 
                "PENDUDUK_AWAL_BULAN_INI":"Penduduk Awal Bulan ini", 
                "PINDAH_BULAN_INI":"Penduduk Akhir Bulan ini"
            };


        var list_jenis = {
                "wni":"Warga Negara Indonesia", 
                "wna":"Warga Negara Asing", 
                "all":"Jumlah Seluruhnya"
            };


        var list_main_jenis = {
            "lampid"        :"Lahir Mati Pindah Dan Datang",
            "agama"         :"Penduduk Berdasarkan Agama",
            "rekam_ktp"     :"Penduduk Berdasarkan Perekaman KTP",
            "kelompok_umur" :"Penduduk Berdasarkan Kelompok Umur",
            "rekam_kk"      :"Penduduk Berdasarkan Perekaman KK"
        };



        // console.log(data_json);
        // console.log(data_json_select);

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        function am4themes_myTheme(target) {
          if (target instanceof am4core.ColorSet) {
            target.list = [
              am4core.color("#EC7063"),
              am4core.color("#AF7AC5"),
              am4core.color("#5DADE2"),
              am4core.color("#48C9B0"),
              am4core.color("#F4D03F"),
              am4core.color("#E67E22"),
              am4core.color("#A6ACAF"),
              am4core.color("#1A5276"),
              am4core.color("#142c6d")
            ];
          }
        }

        $(document).ready(function(){
            // console.log(data_label);
            // console.log(data_json_select);
            console.log(data_json);
            set_val_th();

            create_jenis_list();
            create_month_list();
            create_kategori_list();
            
            create_canvas();

            // get_data();
            

        });


        $("#next").click(function(){
            var th_first = $("#th_first").val();
            var filter_jenis = $("#filter_jenis").val();
            
            if(filter_jenis == "lampid"){
                window.location.href = "<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/"+th_first;
            }else if(filter_jenis == "agama"){
                window.location.href = "<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data_agama/"+th_first;
            }else if(filter_jenis == "rekam_ktp"){
                window.location.href = "<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data_rekap/"+th_first;
            }else if(filter_jenis == "kelompok_umur"){
                window.location.href = "<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data_umur/"+th_first;
            }else if(filter_jenis == "rekam_kk"){
                window.location.href = "<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/"+th_first;
            }

            
        });

        function create_jenis_list(){
            var str_option = "";
            for(let i in list_main_jenis){
                str_option += "<option value=\""+i+"\">"+list_main_jenis[i]+"</option>";
            }

            $("#filter_jenis").html(str_option);
        }



        function create_kategori_list(){
            var str_option = "";
            for(let i in list_jenis){
                str_option += "<option value=\""+i+"\">"+list_jenis[i]+"</option>";
            }

            $("#filter_kategori").html(str_option);
        }

        function create_month_list(){
            var str_option = "";
            for(let i in data_json.lampid.all){
                var th = i.substr(0,4);
                var periode = parseInt(i.substr(4,2))-1;
                str_option += "<option value=\""+i+"\">"+MONTHS[periode]+"</option>";
            }

            $("#filter_bln").html(str_option);
        }



        $("#filter_kategori").change(function(){
            // create_canvas();
            get_data(id_global);
           
        });

        $("#filter_bln").change(function(){
            // create_canvas();
            get_data(id_global);
           
        });

        function get_data(id){
            var filter_jenis        = $("#filter_jenis").val();
            var filter_kategori     = $("#filter_kategori").val();
            var filter_bln          = $("#filter_bln").val();
            var filter_kecamatan    = id;

            id_global = id;

            // console.log(filter_jenis);
            // console.log(filter_kategori);
            // console.log(filter_bln);
            // console.log(filter_kecamatan);

            create_canvas();

            for(let i in list_kategori){
                create_chart("chart_"+i, data_json[filter_jenis][filter_kategori][filter_bln][i][filter_kecamatan]);
            }
            

            // console.log(data_json[filter_jenis][filter_kategori][filter_bln]["PENDUDUK_AWAL_BULAN_INI"][filter_kecamatan]);

            $("#myLargeModalLabel").html(list_kecamatan[id]);
            $("#surya").modal('show');
        }


        function create_chart(div_chart, data_main){
            am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            am4core.useTheme(am4themes_myTheme);
            am4core.useTheme(am4themes_dark);
            // Themes end

            // Create chart instance
            var chart = am4core.create(div_chart, am4charts.PieChart);

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "value";
            pieSeries.dataFields.category = "keterangan";

            // Let's cut a hole in our Pie chart the size of 30% the radius
            chart.innerRadius = am4core.percent(30);

            // Put a thick white border around each Slice
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;
            pieSeries.slices.template
              // change the cursor on hover to make it apparent the object can be interacted with
              .cursorOverStyle = [
                {
                  "property": "cursor",
                  "value": "pointer"
                }
              ];

            pieSeries.alignLabels = false;
            pieSeries.labels.template.bent = true;
            pieSeries.labels.template.radius = 3;
            pieSeries.labels.template.padding(0,0,0,0);

            pieSeries.ticks.template.disabled = true;

            // Create a base filter effect (as if it's not there) for the hover to return to
            var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
            shadow.opacity = 0;

            // Create hover state
            var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

            // Slightly shift the shadow and make it more prominent on hover
            var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
            hoverShadow.opacity = 0.7;
            hoverShadow.blur = 5;

            // Add a legend
            chart.legend = new am4charts.Legend();

            chart.data = data_main;

            }); // end am4core.ready()
        }


        function create_canvas(){
            var str_option = "";
            for(let i in list_kategori){
                str_option +=   "<div class=\"col-md-4\">"+
                                    "<div class=\"col-md-12\"></br></div>"+
                                    "<div class=\"col-md-12\"><h4 class=\"modal-title\">"+list_kategori[i]+"</h4></div>"+
                                    "<div class=\"col-md-12\">"+
                                        "<div id=\"chart_"+i+"\" style=\"width: 100%; height: 350px;\"></div>"+
                                    "</div>"+
                                "</div>";
            }

            console.log(str_option);

            $("#out_body").html(str_option);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";

            $("#th_first").val(th_first);
        }
    </script>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

     <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>


   


    </body>
</html>


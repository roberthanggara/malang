<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php print_r($page);?></title>

	<script src="<?php print_r(base_url());?>assets/chartjs/Chart.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/chartjs/utils.js"></script>

	<style>
		canvas {
			-moz-user-select: none;
			-webkit-user-select: none;
			-ms-user-select: none;
		}
	</style>
</head>
<body>

<div id="container">
	<label>Kategori</label>
    <select id="kategori" name="kategori">
        
    </select>
    <br><br>

	<div id="chart_group" style="width:100%;">
		
	</div>
	<?php print_r(form_open_multipart('show_report/showdispenduk/get_data'));?>
		<h1><?php print_r($page);?></h1>
		<!-- <label>Periode Bulan: </label>
		<select name="bln_periode" id="bln_periode">
			<?php
				if($month){
					foreach ($month as $key => $value) {
						if($key >= 1){
							print_r("<option value=\"".$key."\">".$value."</option>");
						}
						
					}
				}
			?>
		</select> -->

		<label>Periode Tahun: </label><select name="th_periode" id="th_periode">
									<?php
										if($data_periode_th){
											foreach ($data_periode_th as $key => $value) {
												print_r("<option value=\"".$value->th_periode."\">".$value->th_periode."</option>");
											}
										}
									?>
								</select>
		<br><br>
		
		<input type="submit" name="do_upload" id="do_upload" value="Lihat Detail">
	</form>

	<br><br><br>
	
</div>

<script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>
<script>
	var json_data = JSON.parse('<?php print_r($data_main);?>');
	console.log(json_data);
	var color = Chart.helpers.color;
	
	$(document).ready(function(){
        create_j_data();
        create_canvas();

        create_config_graph();

        // create_graph();
    });

	function create_j_data(){
		var data_main = json_data.head;
        var str_option = "";
        for (let item in data_main) {
            str_option += "<option value=\""+item+"\">"+data_main[item]+"</option>";
        }
        $("#kategori").html(str_option);
    }

    $("#kategori").change(function(){
        var param = $("#kategori").val();
        // console.log(param);
        create_canvas();

        create_config_graph();
    });


    function create_config_graph(){
		var param = $("#kategori").val();
		var data_main = json_data.item[param];

		var data_head = json_data.head_sub;

		var data_set = [];

		// console.log(data_main);
		var no = 0;
		for (let item_head in data_head) {
			var str_fill = false;
			if(json_data.type_chart[item_head] == "bar"){
				str_fill = true;
			}
			var tmp_data_set = {
									type: json_data.type_chart[item_head],
									label: json_data.head_sub[item_head],
									backgroundColor: window.chartColors[no],
									borderColor: window.chartColors[no],
									fill: str_fill,
									data: json_data.item[param][item_head],
								};
			data_set.push(tmp_data_set);
            // console.log(item_head);
            no++;
        }

        create_graph(data_set);

        // console.log(data_set);

    }

    function create_canvas(){
    	var str_html_canvas = "<canvas id=\"canvas\"></canvas>";
    	$("#chart_group").html(str_html_canvas);
    }


    function create_graph(param){
    	// var param = $("#kategori").val();
    	console.log(param);
    	var config = {
			type: 'bar',
			data: {
				labels: json_data.label,
				datasets: param
			},
			options: {
				title: {
					text: 'Chart.js Combo Time Scale'
				},
				scales: {
					xAxes: [{
	                    display: true,
	                    scaleLabel: {
	                        display: true,
	                        labelString: 'Periode'
	                    }
	                }],
				},
			}
		};

		// window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);

		// };

		// console.log(config.data);
    }

	
	
</script>

</body>
</html>
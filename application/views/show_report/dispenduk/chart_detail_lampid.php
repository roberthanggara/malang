
<html>

<head>
    <title>Line Chart</title>
    <script src="<?php print_r(base_url());?>assets/chartjs/Chart.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/chartjs/utils.js"></script>

    <!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>
<?php
    $th_first = $this->uri->segment(4);
    $th_finish = $this->uri->segment(5);
?>

<body>
    
    <input type="number" name="th_first" id="th_first"> &nbsp;&nbsp;&nbsp;
    <input type="number" name="th_finish" id="th_finish">
    <br>
    <button type="button" id="next" name="next">Next</button>
    <br><br>
    <div>
        <label>Kecamatan</label>
        <select id="kecamatan" name="kecamatan">
            
        </select>
        

        <label>Jenis Data</label>
        <select id="jenis_data" name="jenis_data">
            
        </select>
        

        <label>Pilih data mana yang akan di pilih</label>
        <select id="j_data" name="j_data">
            
        </select>

        <br><br>

        <label>Kategori</label>
        <select id="kategori" name="kategori">
            
        </select>

        <br><br>

        <label>Jenis Kategori</label>
        <select id="j_kategori" name="j_kategori">
            
        </select>

        <br><br>
    </div>
    <div style="width:100%;" id="total_div">
        
    </div>

    <!-- <div>
        <div id="chart" style="width:100%; height: 500px;"></div>
    </div> -->
    <br>
    <br>
    <td align="right"></td>

    
    
    <!-- <?php print_r($str_tbl);?> -->
    

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_label = JSON.parse('<?php print_r($label);?>');

        var list_kecamatan = [
                                    {"id":"blimbing", "ket":"KEC. Blimbing"},
                                    {"id":"kedung_kandang", "ket":"KEC. Kedungkandang"},
                                    {"id":"klojen", "ket":"KEC. Klojen"},
                                    {"id":"lowokwaru", "ket":"KEC. Lowokwaru"},
                                    {"id":"sukun", "ket":"KEC. Sukun"}
                                ];

        var list_kategori = {
                "LAHIR_BULAN_INI":"Berdasarkan Kelahiran", 
                "MATI_BULAN_INI":"Berdasarkan Kematian", 
                "PENDATANG_BULAN_INI":"Berdasarkan Jumlah Pendatang", 
                "PENDUDUK_AKHIR_BULAN_INI":"Penduduk Akhir Bulan ini", 
                "PENDUDUK_AWAL_BULAN_INI":"Penduduk Awal Bulan ini", 
                "PINDAH_BULAN_INI":"Penduduk Akhir Bulan ini"
            };


        var list_jenis = {
                "wni":"Warga Negara Indonesia", 
                "wna":"Warga Negara Asing", 
                "all":"Jumlah Seluruhnya"
            };

        console.log(data_json);
        // console.log(data_json_select);

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        $(document).ready(function(){
            // console.log(data_label);
            // console.log(data_json_select);
            console.log(data_json);
            set_val_th();

            create_op_kec();
            create_jenis();

            create_canvas();

            get_data();
            

        });

        $("#next").click(function(){
            var th_first = $("#th_first").val();
            var th_finish = $("#th_finish").val();

            window.location.href = "<?php print_r(base_url());?>show_report/showdispenduknew/get_data/"+th_first+"/"+th_finish;
        });


        function create_op_kec(){
            var str_op_kec = "";
            for (let i in list_kecamatan) {
                str_op_kec += "<option value=\""+list_kecamatan[i].id+"\">"+list_kecamatan[i].ket+"</option>";
            }

            $("#kecamatan").html(str_op_kec);
        }


        function create_jenis(){
            var str_jenis_data = "";
            for (let i in data_json_select.lampid.item) {
                // console.log(i);
                str_jenis_data += "<option value=\""+i+"\">"+data_json_select.lampid.item[i].keterangan+"</option>";
            }

            $("#jenis_data").html(str_jenis_data);
        }

        $("#kecamatan").change(function(){
            create_canvas();
            get_data();
           
        });

        $("#jenis_data").change(function(){
            create_canvas();
            get_data();
           
        });

        function get_data(){
            var th_first = "<?php print_r($th_first);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

           
        }


        function create_canvas(){
            var str_canvas = "<canvas id=\"canvas_lahir\"></canvas>"
                            +"<canvas id=\"canvas_pendatang\"></canvas>"
                            +"<canvas id=\"canvas_mati\"></canvas>"
                            +"<canvas id=\"canvas_pindah\"></canvas>"
                            +"<canvas id=\"canvas_akhir_bulan\"></canvas>";

            $("#total_div").html(str_canvas);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            $("#th_first").val(th_first);
            $("#th_finish").val(th_finish);
        }

        
    </script>
</body>

</html>

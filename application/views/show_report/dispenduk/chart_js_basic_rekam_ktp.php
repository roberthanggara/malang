
<html>

<head>
    <title>Line Chart</title>
    <script src="<?php print_r(base_url());?>assets/chartjs/Chart.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/chartjs/utils.js"></script>

    <!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>
<?php
    $th_first = $this->uri->segment(4);
    $th_finish = $this->uri->segment(5);
?>

<body>
    
    <input type="number" name="th_first" id="th_first"> &nbsp;&nbsp;&nbsp;
    <input type="number" name="th_finish" id="th_finish">
    <br>
    <button type="button" id="next" name="next">Next</button>
    <br><br>
    <div>
        <label>Kecamatan</label>
        <select id="kecamatan" name="kecamatan">
            
        </select>
        

        <label>Jenis Data</label>
        <select id="jenis_data" name="jenis_data">
            
        </select>
        

        <label>Pilih data mana yang akan di pilih</label>
        <select id="j_data" name="j_data">
            
        </select>

        <br><br>

        <label>Kategori</label>
        <select id="kategori" name="kategori">
            
        </select>

        <br><br>

        <label>Jenis Kategori</label>
        <select id="j_kategori" name="j_kategori">
            
        </select>

        <br><br>
    </div>
    <div style="width:100%;" id="total_div">
        
    </div>

    <!-- <div>
        <div id="chart" style="width:100%; height: 500px;"></div>
    </div> -->
    <br>
    <br>
    <td align="right"></td>

    
    
    <!-- <?php print_r($str_tbl);?> -->
    

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_json_select = JSON.parse('<?php print_r($data_json_select);?>');
        var data_label = JSON.parse('<?php print_r($label);?>');

        var list_kecamatan = [
                                {"id":"blimbing", "ket":"KEC. Blimbing"},
                                {"id":"kedung_kandang", "ket":"KEC. Kedungkandang"},
                                {"id":"klojen", "ket":"KEC. Klojen"},
                                {"id":"lowokwaru", "ket":"KEC. Lowokwaru"},
                                {"id":"sukun", "ket":"KEC. Sukun"}
                            ];

        // console.log(data_json);
        // console.log(data_json_select);

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        $(document).ready(function(){
            // console.log(data_label);
            // console.log(data_json_select);
            console.log(data_json);
            set_val_th();

            create_op_kec();
            // create_jenis();

            create_canvas();

            get_data();
            get_data_akta();
        });

        $("#next").click(function(){
            var th_first = $("#th_first").val();
            var th_finish = $("#th_finish").val();

            window.location.href = "<?php print_r(base_url());?>show_report/showdispenduknew/get_data/"+th_first+"/"+th_finish;
        });


        function create_op_kec(){
            var str_op_kec = "";
            for (let i in list_kecamatan) {
                str_op_kec += "<option value=\""+list_kecamatan[i].id+"\">"+list_kecamatan[i].ket+"</option>";
            }

            $("#kecamatan").html(str_op_kec);
        }


        $("#kecamatan").change(function(){
            create_canvas();
            get_data();
            get_data_akta();
        });

        $("#jenis_data").change(function(){
            create_canvas();
            get_data();
            get_data_akta();
        });

        function get_data(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            var data_wajib_memiliki_ktp = data_json.rekam_ktp["rekam_ktp"].wajib_memiliki_KTP[kecamatan].lp;
            var data_sudah_rekam_KTP_el = data_json.rekam_ktp["rekam_ktp"].sudah_rekam_KTP_el[kecamatan].sudah_rekam_KTP_el;
            var data_belum_rekam_KTP_el = data_json.rekam_ktp["rekam_ktp"].belum_rekam_KTP_el[kecamatan].belum_rekam_KTP_el;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk WAJIB memiliki KTP',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: data_wajib_memiliki_ktp,
                            fill: false,
                        }, {
                            label: 'Penduduk SUDAH rekam KTP',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: data_sudah_rekam_KTP_el,
                        }, {
                            label: 'Penduduk BELUM rekam KTP',
                            fill: false,
                            backgroundColor: window.chartColors[2],
                            borderColor: window.chartColors[2],
                            data: data_belum_rekam_KTP_el,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Data Perekaman KTP Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_rekam_ktp").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        function get_data_akta(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            var kecamatan = $("#kecamatan").val();
            var jenis_data = $("#jenis_data").val();

            // console.log(data_json.rekam_ktp["rekam_ktp"].wajib_memiliki_KTP[kecamatan]);

            var wajib_akta = data_json.rekam_ktp["rekam_ktp"].wajib_memiliki_akta_kelahiran[kecamatan].wajib_memiliki_akta_kelahiran;
            var sudah_akta = data_json.rekam_ktp["rekam_ktp"].memiliki_akta_kelahiran[kecamatan].memiliki_akta_kelahiran;

            
            var tmp_config = {
                    type: 'line',
                    data: {
                        labels: data_label,
                        datasets: [{
                            label: 'Penduduk WAJIB memiliki AKTA KELAHIRAN',
                            backgroundColor: window.chartColors[0],
                            borderColor: window.chartColors[0],
                            data: wajib_akta,
                            fill: false,
                        }, {
                            label: 'Penduduk SUDAH memiliki AKTA KELAHIRAN',
                            fill: false,
                            backgroundColor: window.chartColors[1],
                            borderColor: window.chartColors[1],
                            data: sudah_akta,
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Data Perekaman AKTA KELAHIRAN Tahun '+th_first+' - '+th_finish
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Periode '+th_first+' - '+th_finish
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nilai'
                                }
                            }]
                        }
                    }
                };
                // config[item_canvas] = tmp_config;

                var ctx = document.getElementById("canvas_rekam_akta").getContext('2d');
            
                window.myLine = new Chart(ctx, tmp_config);
                window.myLine.update();
        }

        

        function create_canvas(){
            var str_canvas = "<canvas id=\"canvas_rekam_ktp\"></canvas>"
                            +"<canvas id=\"canvas_rekam_akta\"></canvas>"
                            // +"<canvas id=\"canvas_sudah_rekam_KTP_el\"></canvas>"
                            // +"<canvas id=\"canvas_belum_rekam_KTP_el\"></canvas>"
                            // +"<canvas id=\"canvas_wajib_memiliki_akta_kelahiran\"></canvas>"
                            +"<canvas id=\"canvas_memiliki_akta_kelahiran\"></canvas>";

            $("#total_div").html(str_canvas);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";
            var th_finish = "<?php print_r($th_finish);?>";

            $("#th_first").val(th_first);
            $("#th_finish").val(th_finish);
        }

        
    </script>
</body>

</html>

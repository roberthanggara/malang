<!DOCTYPE html>

<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
  
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- MENU SIDEBAR -->
              <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Data Pelanggan PDAM</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">22741</h3>
                                    <h6 class="card-subtitle">Pelanggan PDAM Kec. Klojen<br> Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 95%; height: 6px;" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">32887</h3>
                                     <h6 class="card-subtitle">Pelanggan PDAM Kec. Blimbing<br> Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 94%; height: 6px;" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">31307</h3>
                                    <h6 class="card-subtitle">Pelanggan PDAM Kec. Sukun<br> Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 95%; height: 6px;" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">35198</h3>
                                   <h6 class="card-subtitle">Pelanggan PDAM Kec. Lowokwaru<br> Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 98%; height: 6px;" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                      <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-account text-info"></i></h2>
                                    <h3 class="">38595</h3>
                                   <h6 class="card-subtitle">Pelanggan PDAM Kec. Kedungkandang<br> Kota Malang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 97%; height: 6px;" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row -->
  <div class="col-12" >
  <div class="card">
  <br>
  <center>
  <h3 class="card-title">Data Grafik - Jumlah Pelanggan PDAM Kota Malang</h3>
  </center>
  <div id="chartdiv"  style="width: 100%; height: 400px;" ></div>
 
  </div>

  <br>
  <center>
  <!-- Tabel disini -->

  <div class="col-lg-8">
                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Tabel - Jumlah Pelanggan PDAM Kota Malang</h4>
                                <h6 class="card-subtitle">Sumber <code>http://www.pdamkotamalang.com/</code></h6>
                                <div class="table-responsive">
                                    <table class="table color-table muted-table">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Distrik</th>
                                                <th>Jumlah Pelanggan</th>    
                                            </tr>
                                        </thead>
                                        <tbody id="out_tbl" style="color:#ffffff">
                                                                                       
                                              <?php
                                                $no = 1;
                                                foreach ($district as $key => $value) {
                                                  

                                                  foreach ($detail[$key] as $keya => $valuea) {
                                                    print_r("<tr>
                                                            <td>".$no."</td>
                                                            <td>".$valuea["name"]."</td>");
                                                    print_r(" <td align=\"2\">".number_format($valuea["value"], 0, ',', '.')." Pelanggan</td>
                                                          </tr>");

                                                    $no++;
                                                  }
                                                  print_r("<tr>
                                                            <td colspan=\"2\"><b>".$value["kecamatan"]."</b></td>");
                                                  print_r(" <td align=\"right\"><b>".number_format($value["val_kec"], 0, ',', '.')." Pelanggan</b></td>
                                                          </tr>");
                                                  
                                                  // print_r($value);
                                                }
                                              ?>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


  </center>
  <!-- Chart code -->
  <script>

  function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#142c6d")
        ];
      }
    }

    var data_chart = JSON.parse('<?php print_r($chart);?>');
    console.log(data_chart);
    am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_myTheme);
    am4core.useTheme(am4themes_dark);
    am4core.useTheme(am4themes_animated);
    // Themes end

    var container = am4core.create("chartdiv", am4core.Container);
    container.width = am4core.percent(100);
    container.height = am4core.percent(100);
    container.layout = "horizontal";


    var chart = container.createChild(am4charts.PieChart);

    // Add data
    chart.data = data_chart;

    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "val_kec";
    pieSeries.dataFields.category = "kecamatan";
    pieSeries.slices.template.states.getKey("active").properties.shiftRadius = 0;
    //pieSeries.labels.template.text = "{category}\n{value.percent.formatNumber('#.#')}%";

    pieSeries.slices.template.events.on("hit", function(event) {
      selectSlice(event.target.dataItem);
    })

    var chart2 = container.createChild(am4charts.PieChart);
    chart2.width = am4core.percent(30);
    chart2.radius = am4core.percent(80);

    // Add and configure Series
    var pieSeries2 = chart2.series.push(new am4charts.PieSeries());
    pieSeries2.dataFields.value = "value";
    pieSeries2.dataFields.category = "name";
    pieSeries2.slices.template.states.getKey("active").properties.shiftRadius = 0;
    //pieSeries2.labels.template.radius = am4core.percent(50);
    //pieSeries2.labels.template.inside = true;
    //pieSeries2.labels.template.fill = am4core.color("#ffffff");
    pieSeries2.labels.template.disabled = true;
    pieSeries2.ticks.template.disabled = true;
    pieSeries2.alignLabels = false;
    pieSeries2.events.on("positionchanged", updateLines);

    var interfaceColors = new am4core.InterfaceColorSet();

    var line1 = container.createChild(am4core.Line);
    line1.strokeDasharray = "2,2";
    line1.strokeOpacity = 0.5;
    line1.stroke = interfaceColors.getFor("alternativeBackground");
    line1.isMeasured = false;

    var line2 = container.createChild(am4core.Line);
    line2.strokeDasharray = "2,2";
    line2.strokeOpacity = 0.5;
    line2.stroke = interfaceColors.getFor("alternativeBackground");
    line2.isMeasured = false;

    var selectedSlice;

    function selectSlice(dataItem) {

      selectedSlice = dataItem.slice;

      var fill = selectedSlice.fill;

      var count = dataItem.dataContext.subData.length;
      pieSeries2.colors.list = [];
      for (var i = 0; i < count; i++) {
        pieSeries2.colors.list.push(fill.brighten(i * 2 / count));
      }

      chart2.data = dataItem.dataContext.subData;
      pieSeries2.appear();

      var middleAngle = selectedSlice.middleAngle;
      var firstAngle = pieSeries.slices.getIndex(0).startAngle;
      var animation = pieSeries.animate([{ property: "startAngle", to: firstAngle - middleAngle }, { property: "endAngle", to: firstAngle - middleAngle + 360 }], 600, am4core.ease.sinOut);
      animation.events.on("animationprogress", updateLines);

      selectedSlice.events.on("transformed", updateLines);

    //  var animation = chart2.animate({property:"dx", from:-container.pixelWidth / 2, to:0}, 2000, am4core.ease.elasticOut)
    //  animation.events.on("animationprogress", updateLines)
    }


    function updateLines() {
      if (selectedSlice) {
        var p11 = { x: selectedSlice.radius * am4core.math.cos(selectedSlice.startAngle), y: selectedSlice.radius * am4core.math.sin(selectedSlice.startAngle) };
        var p12 = { x: selectedSlice.radius * am4core.math.cos(selectedSlice.startAngle + selectedSlice.arc), y: selectedSlice.radius * am4core.math.sin(selectedSlice.startAngle + selectedSlice.arc) };

        p11 = am4core.utils.spritePointToSvg(p11, selectedSlice);
        p12 = am4core.utils.spritePointToSvg(p12, selectedSlice);

        var p21 = { x: 0, y: -pieSeries2.pixelRadius };
        var p22 = { x: 0, y: pieSeries2.pixelRadius };

        p21 = am4core.utils.spritePointToSvg(p21, pieSeries2);
        p22 = am4core.utils.spritePointToSvg(p22, pieSeries2);

        line1.x1 = p11.x;
        line1.x2 = p21.x;
        line1.y1 = p11.y;
        line1.y2 = p21.y;

        line2.x1 = p12.x;
        line2.x2 = p22.x;
        line2.y1 = p12.y;
        line2.y2 = p22.y;
      }
    }

    chart.events.on("datavalidated", function() {
      setTimeout(function() {
        selectSlice(pieSeries.dataItems.getIndex(0));
      }, 1000);
    });


    }); // end am4core.ready()
  </script>

<br>
                
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019  Dashboard Kota Malang by Kominfo  </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

     <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>


     <script type="text/javascript">
         function hastage(id){
            console.log(id);

            $("#hastage").modal('show');
         }
     </script>


    </body>
</html>


<!-- Styles -->
<style>
body { background-color: #30303d; color: #fff; }
#chartdiv {
  width: 100%;
  height: 500px;
}

</style>

<!-- Resources -->
<script src="<?php print_r(base_url())?>assets/amcharts4/core.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/charts.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/themes/dark.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/themes/animated.js"></script>

<!-- Chart code -->
<script>
  function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d")
        ];
      }
    }

  am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_dark);
  am4core.useTheme(am4themes_animated);
  am4core.useTheme(am4themes_myTheme);
  // Themes end

   // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.XYChart);

  // Add data
  chart.data = [{
    "year": 2005,
    "income": 2300000,
    "expenses": 3300000
  },{
    "year": 2006,
    "income": 2300000,
    "expenses": 4300000
  },{
    "year": 2007,
    "income": 2300000,
    "expenses": 1300000
  }];

  // Create axes
  var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "year";
  categoryAxis.numberFormatter.numberFormat = "#";
  categoryAxis.renderer.inversed = true;
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.cellStartLocation = 0.1;
  categoryAxis.renderer.cellEndLocation = 0.9;

  var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
  valueAxis.numberFormatter.numberFormat = "#,###";
  valueAxis.renderer.opposite = true;
  valueAxis.min = 0;

  // Create series
  function createSeries(field, name) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueX = field;
    series.dataFields.categoryY = "year";
    series.name = name;
    series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
    series.columns.template.height = am4core.percent(100);
    series.sequencedInterpolation = true;

    var valueLabel = series.bullets.push(new am4charts.LabelBullet());
    valueLabel.label.text = "Rp. [bold]{valueX}[/]";
    valueLabel.label.horizontalCenter = "left";
    valueLabel.label.dx = 10;
    valueLabel.label.hideOversized = false;
    valueLabel.label.truncate = false;

    var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
    categoryLabel.label.text = "{name}";
    categoryLabel.label.horizontalCenter = "right";
    categoryLabel.label.dx = -10;
    categoryLabel.label.fill = am4core.color("#fff");
    categoryLabel.label.hideOversized = false;
    categoryLabel.label.truncate = false;
  }

  createSeries("income", "Income");
  createSeries("expenses", "Expenses");

  }); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>
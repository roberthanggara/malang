<!DOCTYPE html>

<html> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
    
    <!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

     <script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- MENU SIDEBAR -->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
 <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Detail Kepegawaian</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
          
                <!-- Row -->


<div class="box-header with-border">
 <center><h1 class="headline text-yellow"> DATA GRAFIK KEPEGAWAIAN DI KOTA MALANG </h1></center><br>
    <p align="right">
        <a href="<?php print_r(base_url());?>index.php/Halaman_utama">Dashboard</a> |
        <a href="<?php print_r(base_url());?>Halaman_sub_utama">Home</a>
    </p>
    <br>
      <div class="card">
   
<h3><?php echo "Grafik Agregat Jumlah Aparatur Sipil Negara Pemerintah Kota Malang"; ?></h3>
    <hr>
    <p><?= $str_select;?></p>

<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<div class="col-12" id="chart_opening">
    <div class="box-body">
        <table width="100%">
            <tr>
                <td colspan="2">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Semua Golongan</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_gol_all" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="50%">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Golongan I</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_gol_I" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Golongan II</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_gol_II" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="50%">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Golongan III</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_gol_III" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Golongan IV</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_gol_IV" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%">
            <tr>
                <td colspan="2">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Berdasarkan Jenis Kelamin</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_jk" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%">
            <tr>
                <td colspan="2">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Berdasarkan Agama</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_agama" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%">
            <tr>
                <td colspan="2">
                    <table width="100%" border="1">
                        <tr>
                            <td align="center"><b>Berdasarkan Status Kawin</b></td>
                        </tr>
                        <tr>
                            <td><div id="chartdiv_sts" style="height: 350px; width: 100%;"></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
    </div>

<!-- TABELL DISINI -->
<center>
<div class="col-6">
    <h3><?php echo "Grafik Agregat Jumlah Aparatur Sipil Negara Pemerintah Kota Malang"; ?></h3>
    <hr>
    <p><?= $str_select;?></p>
    <table class="table table-striped table-bordered" id="chart-table">
                    <tr>
                        <td colspan='4' align="left" id="nama_dinas"><b>Dinas Olahraga dan Kebudayaan</b></td>
                        <td align="center" id="t_karyawan" width="20%"><b>0</b></td>
                    </tr>
                    <tr class='gol_main_1'>
                        <td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_gl" value="gl"></td>
                        <td colspan='3' align="center"><label for="filter_gl"><i><b>Berdasarkan Golongan</b></i></label></td>
                    </tr>
                    <tr class='gol_jenis_main_1'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Golongan I</td>
                        <td align="center" id="jml_golongan_1">20</td>
                    </tr>
                    <tr class='gol_data_1'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan I-A</td>
                        <td align="center" id="jml_golongan_1a">40</td>
                    </tr>
                    <tr class='gol_data_1'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan I-B</td>
                        <td align="center" id="jml_golongan_1b">40</td>
                    </tr>
                    <tr class='gol_data_1'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan I-C</td>
                        <td align="center" id="jml_golongan_1c">40</td>
                    </tr>
                    <tr class='gol_data_1'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan I-D</td>
                        <td align="center" id="jml_golongan_1d">40</td>
                    </tr>



                    
                    <tr class='gol_jenis_main_2'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Golongan II</td>
                        <td align="center" id="jml_golongan_2">20</td>
                    </tr>
                    <tr class='gol_data_2'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan II-A</td>
                        <td align="center" id="jml_golongan_2a">40</td>
                    </tr>
                    <tr class='gol_data_2'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan II-B</td>
                        <td align="center" id="jml_golongan_2b">40</td>
                    </tr>
                    <tr class='gol_data_2'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan II-C</td>
                        <td align="center" id="jml_golongan_2c">40</td>
                    </tr>
                    <tr class='gol_data_2'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan II-D</td>
                        <td align="center" id="jml_golongan_2d">40</td>
                    </tr>



                    
                    <tr class='gol_jenis_main_3'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Golongan III</td>
                        <td align="center" id="jml_golongan_3">20</td>
                    </tr>
                    <tr class='gol_data_3'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan III-A</td>
                        <td align="center" id="jml_golongan_3a">40</td>
                    </tr>
                    <tr class='gol_data_3'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan III-B</td>
                        <td align="center" id="jml_golongan_3b">40</td>
                    </tr>
                    <tr class='gol_data_3'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan III-C</td>
                        <td align="center" id="jml_golongan_3c">40</td>
                    </tr>
                    <tr class='gol_data_3'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan III-D</td>
                        <td align="center" id="jml_golongan_3d">40</td>
                    </tr>



                
                    <tr class='gol_jenis_main_4'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Golongan IV</td>
                        <td align="center" id="jml_golongan_4">20</td>
                    </tr>
                    <tr class='gol_data_4'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan IV-A</td>
                        <td align="center" id="jml_golongan_4a">40</td>
                    </tr>
                    <tr class='gol_data_4'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan IV-B</td>
                        <td align="center" id="jml_golongan_4b">40</td>
                    </tr>
                    <tr class='gol_data_4'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan IV-C</td>
                        <td align="center" id="jml_golongan_4c">40</td>
                    </tr>
                    <tr class='gol_data_4'>
                        <td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
                        <td>Golongan IV-D</td>
                        <td align="center" id="jml_golongan_4d">40</td>
                    </tr>

                    

                    <tr class='gender_main'>
                        <td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_jk" value="jk"></td>
                        <td colspan='3' align="center" ><label for="filter_jk"><i><b>Berdasarkan Gender</b></i></label></td>
                    </tr>
                    <tr class='gender_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Laki-Laki</td>
                        <td align="center" id="jml_gender_l">20</td>
                    </tr>
                    <tr class='gender_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Perempuan</td>
                        <td align="center" id="jml_gender_p">20</td>
                    </tr>



                    <tr class='agama_main'>
                        <td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_ag" value="ag"></td>
                        <td colspan='3' align="center" ><label for="filter_ag"><i><b>Berdasarkan Agama</b></i></label></td>
                    </tr>
                    <tr class='agama_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Islam</td>
                        <td align="center" id="jml_ag_is">20</td>
                    </tr>
                    <tr class='agama_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Kristen</td>
                        <td align="center" id="jml_ag_kr">20</td>
                    </tr>
                    <tr class='agama_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Katholik</td>
                        <td align="center" id="jml_ag_ka">20</td>
                    </tr>
                    <tr class='agama_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Hindu</td>
                        <td align="center" id="jml_ag_hi">20</td>
                    </tr>
                    <tr class='agama_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Budha</td>
                        <td align="center" id="jml_ag_bu">20</td>
                    </tr>



                    <tr class='status_main'>
                        <td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_sts" value="sts"></td>
                        <td colspan='3' align="center" ><label for="filter_sts"><i><b>Berdasarkan Status Perkawinan</b></i></label></td>
                    </tr>
                    <tr class='status_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Kawin</td>
                        <td align="center" id="jml_sts_k">20</td>
                    </tr>
                    <tr class='status_jenis_main'>
                        <td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
                        <td colspan='2'>Belum Kawin</td>
                        <td align="center" id="jml_sts_bk">20</td>
                    </tr>
    </table>
    </center>
    <hr>
    <p class="small pull-right">
        <a href="<?php echo base_url('') ?>">NCC</a> |
        <a href="<?php echo base_url('katalog') ?>">Katalog</a>
    </p>
</div>
<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
<script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        $(document).ready(function(){
            $("#jenis_instansi").val("1");

            var val_data = $("#jenis_instansi").val();
            get_data(val_data);
        });

        $("#show_for_detail").click(function(){
            window.location.href = "<?php print_r(base_url()."Bpkadapi/show_detail");?>";
        });

        $("#jenis_instansi").change(function(e){
            var val_data = $("#jenis_instansi").val();
            get_data(val_data);
            
        });

        function get_data(i){
            console.log(data_main[i]);
            $("#jml_golongan_1").html(data_main[i].sum_golongan[0].value);
            $("#jml_golongan_2").html(data_main[i].sum_golongan[1].value);
            $("#jml_golongan_3").html(data_main[i].sum_golongan[2].value);
            $("#jml_golongan_4").html(data_main[i].sum_golongan[3].value);


            $("#jml_golongan_1a").html(data_main[i].jumlah_golongan_I[0].value);
            $("#jml_golongan_1b").html(data_main[i].jumlah_golongan_I[1].value);
            $("#jml_golongan_1c").html(data_main[i].jumlah_golongan_I[2].value);
            $("#jml_golongan_1d").html(data_main[i].jumlah_golongan_I[3].value);
            
            $("#jml_golongan_2a").html(data_main[i].jumlah_golongan_II[0].value);
            $("#jml_golongan_2b").html(data_main[i].jumlah_golongan_II[1].value);
            $("#jml_golongan_2c").html(data_main[i].jumlah_golongan_II[2].value);
            $("#jml_golongan_2d").html(data_main[i].jumlah_golongan_II[3].value);

            $("#jml_golongan_3a").html(data_main[i].jumlah_golongan_III[0].value);
            $("#jml_golongan_3b").html(data_main[i].jumlah_golongan_III[1].value);
            $("#jml_golongan_3c").html(data_main[i].jumlah_golongan_III[2].value);
            $("#jml_golongan_3d").html(data_main[i].jumlah_golongan_III[3].value);

            $("#jml_golongan_4a").html(data_main[i].jumlah_golongan_IV[0].value);
            $("#jml_golongan_4b").html(data_main[i].jumlah_golongan_IV[1].value);
            $("#jml_golongan_4c").html(data_main[i].jumlah_golongan_IV[2].value);
            $("#jml_golongan_4d").html(data_main[i].jumlah_golongan_IV[3].value);


            $("#jml_gender_l").html(data_main[i].gender[0].value);
            $("#jml_gender_p").html(data_main[i].gender[1].value);


            $("#jml_ag_is").html(data_main[i].agama[0].value);
            $("#jml_ag_kr").html(data_main[i].agama[1].value);
            $("#jml_ag_ka").html(data_main[i].agama[2].value);
            $("#jml_ag_hi").html(data_main[i].agama[3].value);
            $("#jml_ag_bu").html(data_main[i].agama[4].value);


            $("#jml_sts_k").html(data_main[i].status_kawin[0].value);
            $("#jml_sts_bk").html(data_main[i].status_kawin[1].value);


            $("#t_karyawan").html(data_main[i].main[0].value+" (Karyawan)");
            $("#nama_dinas").html("<b>"+data_main[i].main[0].keterangan+"</b>");


            set_graph("chartdiv_gol_all", data_main[i].sum_golongan, am4core.useTheme(am4themes_material));

            set_graph("chartdiv_gol_I", data_main[i].jumlah_golongan_I, am4core.useTheme(am4themes_material));
            set_graph("chartdiv_gol_II", data_main[i].jumlah_golongan_II, am4core.useTheme(am4themes_material));
            set_graph("chartdiv_gol_III", data_main[i].jumlah_golongan_III, am4core.useTheme(am4themes_dataviz));
            set_graph("chartdiv_gol_IV", data_main[i].jumlah_golongan_IV, am4core.useTheme(am4themes_kelly));


            set_graph("chartdiv_jk", data_main[i].gender,am4core.useTheme(am4themes_material));

            set_graph("chartdiv_agama", data_main[i].agama,am4core.useTheme(am4themes_material));

            set_graph("chartdiv_sts", data_main[i].status_kawin);
        }

        function am4themes_myTheme(target) {
          if (target instanceof am4core.ColorSet) {
            target.list = [
              // warna biru
              am4core.color("#00BFFF"),
                
              // warna orange
              am4core.color("#FF8C00"), 

              // warna hijau
              am4core.color("#32CD32")
            ];
          }
        }

        function set_graph(id_chart_div, data_main,){
            am4core.ready(function() {

            am4core.useTheme(am4themes_dark);
            am4core.useTheme(am4themes_animated);
            am4core.useTheme(am4themes_myTheme);

            var chart = am4core.create(id_chart_div, am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.legend = new am4charts.Legend();

            chart.data = data_main;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "value";
            series.dataFields.category = "keterangan";

            });
        }

        $("input[name='filter']").change(function(){
            var array_filter_active = [];
            $.each($("input[name='filter']:checked"), function(){            
                array_filter_active.push($(this).val());
            });

            console.log(array_filter_active);
        });


    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
  <div class="row">
                   
                  
                </div>
                <!-- Row -->
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019  Dashboard Kota Malang by Kominfo </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.init.js") ?>"></script>



    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>



    <script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dark.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kell.js"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
<script>
    
    </body>
</html>


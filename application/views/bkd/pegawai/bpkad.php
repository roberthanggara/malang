<!DOCTYPE html>

<html> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
    
    <!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

     <script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- MENU SIDEBAR -->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                         <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Maps</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
          
                <!-- Row -->


<div class="box-header with-border">
 <center><h1 class="headline text-yellow"> DATA GRAFIK KEPEGAWAIAN DI KOTA MALANG </h1></center><br>
    
    <input type="radio" name="radio_choose" id="radio_choose" class="category_choose" value="1">
      <label for="radio_choose">filter berdasarkan golongan</label>
    <input type="radio" name="radio_choose" id="radio_choose1" class="category_choose" value="2">
      <label for="radio_choose1">filter berdasarkan jenis kelamin</label>
    <input type="radio" name="radio_choose" id="radio_choose2" class="category_choose" value="3">
      <label for="radio_choose2">filter berdasarkan agama</label>
    <input type="radio" name="radio_choose" id="radio_choose3" class="category_choose" value="4">
      <label for="radio_choose3">filter berdasarkan status</label>
    <input type="radio" name="radio_choose" id="radio_choose4" class="category_choose" value="5">
      <label for="radio_choose4">filter berdasarkan jumlah karyawan</label>

    <button id="show_for_detail">Tampilkan Detail Masing-Masing OPD</button>
    <div class="card">
    <div id="chartdiv" style="height: 3000px; width: 100%;"></div> 
    </div>



    <script>
      var data_main = JSON.parse('<?php print_r($data_json);?>');
      $(document).ready(function(){
        set_gologan(data_main.golongan);
      });

      $("#show_for_detail").click(function(){
        window.location.href = "<?php print_r(base_url()."bkd/Bpkadapi/show_detail");?>";
      });

      $('input[name=radio_choose]').change(function(){
        var param_radio = $('input[name=radio_choose]:checked').val();
        // console.log(param_radio)
        if(param_radio == 1){
          set_gologan(data_main.golongan);
          console.log(data_main.golongan);
        }else if(param_radio == 2){
          set_jenis_kelamin(data_main.jenis_kelamin);
          console.log(data_main.jenis_kelamin);
        }else if(param_radio == 3){
          set_agama(data_main.agama);
          console.log(data_main.agama);
        }else if(param_radio == 4){
          set_status(data_main.status);
          console.log(data_main.status);
        }else if(param_radio == 5){
          set_jml_total(data_main.jumlah_total);
          console.log(data_main.jumlah_total);
        }

      });

       function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          // warna biru
          am4core.color("#00BFFF"),
            
          // warna orange
          am4core.color("#FF8C00"), 

          // warna hijau
          am4core.color("#32CD32"),

          // warna merah
          am4core.color("#FF0000")
        ];
      }
    }

      function set_gologan(data_array){
        // Themes begin
            am4core.useTheme(am4themes_animated);
         am4core.useTheme(am4themes_dark);
         am4core.useTheme(am4themes_myTheme);
    
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          // labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
          labelBullet.label.horizontalCenter = "right";
         

        }

        createSeries("jumlah_golongan_I"  , "Jumlah Golongan I");
        createSeries("jumlah_golongan_II" , "Jumlah Golongan II");
        createSeries("jumlah_golongan_III", "Jumlah Golongan III");
        createSeries("jumlah_golongan_IV" , "Jumlah Golongan IV"); 
      }

      function set_jenis_kelamin(data_array){
        // Themes begin
         am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          // labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
          labelBullet.label.horizontalCenter = "right";
        }

        createSeries("laki"  , "Laki-laki");
        createSeries("perempuan" , "Perempuan");
      }

      function set_agama(data_array){
        // Themes begin
        am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          // labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
          labelBullet.label.horizontalCenter = "right";
        }

        createSeries("agama_islam"  , "Agama Islam");
        createSeries("agama_kristen" , "Agama Kristen");
        createSeries("agama_katholik", "Agama Katholik");
        createSeries("agama_hindu" , "Agama Hindu");
        createSeries("agama_budha" , "Agama Budha"); 
      }

      function set_status(data_array){
        // Themes begin
        am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);

        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          // labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
          labelBullet.label.horizontalCenter = "right";
        }

        createSeries("status_kawin"  , "Status Kawin");
        createSeries("status_belum_kawin" , "Status Belum Kawin");
      }

      function set_jml_total(data_array){
        // Themes begin
        am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 5;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          // labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
          labelBullet.label.horizontalCenter = "right";
        }

        createSeries("jml"  , "Jumlah Total");
        
      }
    </script>
  <div class="row">
                   
                  
                </div>
                <!-- Row -->
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019  Dashboard Kota Malang by Kominfo </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.init.js") ?>"></script>



    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>


    
    </body>
</html>


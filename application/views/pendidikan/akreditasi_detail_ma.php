<!DOCTYPE html>

<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
    
    <!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
               <!-- MENU SIDEBAR -->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                          <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>

                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
    
                <!-- Row -->
  
 

<div class="col-md-12" id="chart_opening">
  <CENTER><h1 id="header_page_1"></h1></CENTER>


  <br><br>

        <!-- Row -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG</h4>
                                    </div>
                                    <div class="pad">
                                   <!-- Grafik Chart Disini yACH -->
                                    <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                                 <tr>
                                <td>
                                 <div id="chartdiv_akred" style="width: 100%; height: 600px;"></div>
                                </td>
                            </tr>
                        </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>


                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                 <div class="d-flex no-block">
                                    <h4 class="card-title">GRAFIK KURIKULUM/SERTIFIKASI</h4>  
                                </div>
                                <div class="pad">
                        <!-- Grafik Chart Disini yACH -->
                            <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                            <tr>
                                <td>
                                  <div id="chartdiv_iso" style="width: 100%; height: 600px;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <!-- END ROW -->
                 <!-- Row -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG BERDASARKAN ZONA</h4>
                                    </div>
                                    <div class="pad">
                                   <!-- Grafik Chart Disini yACH -->
                                    <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                                 <tr>
                                <td>
                                 <div id="chartdiv_akred_z" style="width: 100%; height: 600px;"></div>
                                </td>
                            </tr>
                        </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>


                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                 <div class="d-flex no-block">
                                    <h4 class="card-title">GRAFIK KURIKULUM/SERTIFIKASI BERDASARKAN ZONA</h4>  
                                </div>
                                <div class="pad">
                        <!-- Grafik Chart Disini yACH -->
                                     <table class="table table-striped table-bordered" id="chart-table" style="color:#ffffff">
                                     <tr>
                                     <td>
                                  <div id="chartdiv_iso_z" style="width: 100%; height: 600px;"></div>
                                </td>
                            </tr>
                        </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                    <div class="card">
                            <div class="card-body">
                                <center><h3 id="header_page_2"></h3></center>
                                    <hr>
                                        <!-- <p><?= $str_select;?></p> -->
                                         <table class="table color-table muted-table" id="chart-table" style="color:#ffffff">
                                 <thead id="t_head">
                            </thead>
                          <tbody id="t_body">  
                          </tbody>
                            
                        </table>
                      <hr>
                      <p class="small pull-right">
                        <a href="<?php print_r(base_url());?>halaman_utama">Dashboard</a> | 
                        <a href="<?php print_r(base_url());?>halaman_sub_utama">Home</a>
                      </p>
                    </div>
                    </div>
                    </div>

                </div>
                 <!-- END ROW -->

            <!-- /.box-body -->
    </center>
</div>
 <h6 class="card-subtitle">Sumber <code>Backbone Dinas Pendidikan</code></h6>
<footer class="footer"> © 2019  Dashboard Kota Malang by Kominfo  </footer>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.init.js") ?>"></script>



    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dark.js"></script>



    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        $(document).ready(function(){
          set_all_data();
        });

        // $("#stsek").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });


        // $("#tahundata").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });


        // $("#jenjang").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });
         function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#EC7063")
        ];
      }
    }



        function set_request(){
          var data_main =  new FormData();
          data_main.append('stsek', $("#stsek").val());
          data_main.append('tahundata', $("#tahundata").val());
          data_main.append('jenjang', $("#jenjang").val());
                                                
          $.ajax({
              url: "<?php echo base_url()."pendidikan_new/pendidikan/index_ma_req";?>",
              dataType: 'html',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: data_main,                         
              type: 'post',
              success: function(res){
                  // console.log(res);
                  change_data(res);
                  // set_val_update_strata(res, param);
              }
          });
        }

        function change_data(res){
          data_main_new = JSON.parse(res);
          data_main = data_main_new.msg_detail.item;
          console.log(data_main_new.msg_detail.item);
        }

        function set_all_data(){
          $("#header_page_1").html("Grafik "+data_main.title_header+" \n "+data_main.title_main);

          var title_header = data_main.title;

          var header_table = "<tr class='PAUD_jenis_main_1' align=\"center\">"+
            "<h3><td colspan='12'><b>"+data_main.title_header+" ("+data_main.title_main+")"+
            "</b></td></h3></tr>";

          header_table += "<tr>"+
              "<td width='5%' align='center' rowspan='3'><b>No.</b></td>"+
              "<td rowspan='3' align='center'><b>KECAMATAN</b></td>"+
              "<td rowspan='3' align='center'><b>Jumlah<br>Sekolah</b></td>";

          for (let element in data_main.item) {  
            header_table += "<td colspan='4' align='center'><b>"+data_main.item[element]["nama"]+"</b></td>";
          }
          header_table += "</tr>";

          header_table += "<tr>";
          for (let element in data_main.title) {  
            for (let element_sub in data_main.title[element]) {
              header_table += "<td rowspan='2' align='center'><b>"+data_main.title[element][element_sub]+"</b></td>";
            }
          }
          header_table += "</tr>";

          $("#t_head").html(header_table);

          var data_sum_kec = [];
          var data_all = [];

          var array_sum_item = [];
          var array_sum_sklh = [];

          var array_table = [];
          var no_x = 0; 
          for (let element_item in data_main.item) {
            var no_element_sub = 0;
            
            array_sum_item[element_item] = [];

            var sum_sekolah = 0;

            var tmp_all = [];
            for (let element_sub in data_main.item[element_item]["item"]) {
              var body_table = "";

              // array_sum_item[element_item][element_sub] = [];
              
              if(no_x == 0){
                body_table += "<td align='center'>"+(no_element_sub+1)+"</td><td>"+data_main.item[element_item]["item"][element_sub].nama_kecamatan+"</td><td align='center'>"+data_main.item[element_item]["item"][element_sub].jml_sek+"</td>";

                sum_sekolah += data_main.item[element_item]["item"][element_sub].jml_sek;

                var tmp_sum_kec = {
                  "nama_kecamatan":data_main.item[element_item]["item"][element_sub].nama_kecamatan,
                  "val":data_main.item[element_item]["item"][element_sub].jml_sek
                };

                data_sum_kec.push(tmp_sum_kec);
              }

              tmp_all[no_element_sub] = {};
              // tmp_all[no_x][no_element_sub] = {};
              
              tmp_all[no_element_sub]["nama_kecamatan"] = data_main.item[element_item]["item"][element_sub].nama_kecamatan;


              var no_element_sub_x = 0;
              for (let element_sub_x in data_main.item[element_item]["item"][element_sub]) {
                // console.log(data_main.item[element_item]["item"][element_sub][element_sub_x]);
                if(no_element_sub_x > 1){
                  body_table += "<td align='center'>"+data_main.item[element_item]["item"][element_sub][element_sub_x]+"</td>";

                  if((element_sub_x in array_sum_item[element_item])){
                    array_sum_item[element_item][element_sub_x] += data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }else{
                    array_sum_item[element_item][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }

                  tmp_all[no_element_sub][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  // console.log(tmp_all[no_element_sub][element_sub_x]);
                }

                no_element_sub_x++;
              }

              // console.log(tmp_all);

              data_all[no_x] = [];
              data_all[no_x].push(tmp_all);

              if((no_element_sub in array_table)){
                array_table[no_element_sub] += body_table;
              }else{
                array_table[no_element_sub] = body_table;
              }
              
              no_element_sub++;
            }

            if(no_x == 0){
              array_sum_sklh[no_x] = sum_sekolah;    
            }

            no_x++;
          }

          // console.log(array_sum_item);
          var item_graph_sum_all = {};

          var fix_table = "";
          for (let e in array_table) {
            fix_table += "<tr>"+array_table[e]+"</tr>";
          }

          fix_table += "<tr><td colspan='2' align='center'><b>Jumlah Keseluruhan</b></td><td align='center'><b>"+array_sum_sklh[0]+"</b></td>";

          var item_graph_all = {};

          for (let z in array_sum_item) {

            item_graph_all[z] = [];

            // tmp_item_graph_all = 
            for (let n in array_sum_item[z]) {
              var tmp_item_graph_all = {};
              fix_table += "<td align='center'><b>"+array_sum_item[z][n]+"</b></td>";

              if(z == "akred"){
                tmp_item_graph_all["category"] = "AKREDITASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }else if(z == "iso"){
                tmp_item_graph_all["category"] = "SERTIFIKASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }

              // console.log(tmp_item_graph_all);

              item_graph_all[z].push(tmp_item_graph_all);
            }            
          }

          fix_table += "</tr>";

          // console.log(data_all[0][0]);
          // console.log(array_sum_item);

          console.log(data_all);
          set_graph_acred_all(item_graph_all.akred);
          set_graph_iso_all(item_graph_all.iso);

          set_graph_acred(data_all[0][0]);
          set_graph_iso(data_all[1][0]);

          $("#t_body").html(fix_table);
        }

        function set_graph_acred_all(data_main){
          am4core.ready(function() {

          // Themes begin
           am4core.useTheme(am4themes_myTheme);
           am4core.useTheme(am4themes_dark);
           am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_akred", am4charts.PieChart3D);
          chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

          chart.legend = new am4charts.Legend();

          chart.data = data_main;

          var series = chart.series.push(new am4charts.PieSeries3D());
          series.dataFields.value = "val";
          series.dataFields.category = "category";

          });
        }

        function set_graph_iso_all(data_main){
          am4core.ready(function() {

          // Themes begin
           am4core.useTheme(am4themes_myTheme);
           am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_iso", am4charts.PieChart3D);
          chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

          chart.legend = new am4charts.Legend();

          chart.data = data_main;

          var series = chart.series.push(new am4charts.PieSeries3D());
          series.dataFields.value = "val";
          series.dataFields.category = "category";

          });
        }


        function set_graph_acred(data_main){
          am4core.ready(function() {

          // Themes begin
           am4core.useTheme(am4themes_myTheme);
           am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv_akred_z", am4charts.XYChart);


          // Add data
          chart.data = data_main;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "nama_kecamatan";
          categoryAxis.renderer.grid.template.location = 0;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.inside = true;
          valueAxis.renderer.labels.template.disabled = true;
          valueAxis.min = 0;

          // Create series
          function createSeries(field, name) {
            
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;
            
            // Make it stacked
            series.stacked = true;
            
            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            
            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            
            return series;
          }

          createSeries("akred_a", "AKREDITASI A");
          createSeries("akred_b", "AKREDITASI B");
          createSeries("akred_c", "AKREDITASI C");
          createSeries("akred_tt", "AKREDITASI TT");

          // Legend
          chart.legend = new am4charts.Legend();

          });
        }

        function set_graph_iso(data_main){
          am4core.ready(function() {

          // Themes begin
           am4core.useTheme(am4themes_myTheme);
           am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv_iso_z", am4charts.XYChart);


          // Add data
          chart.data = data_main;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "nama_kecamatan";
          categoryAxis.renderer.grid.template.location = 0;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.inside = true;
          valueAxis.renderer.labels.template.disabled = true;
          valueAxis.min = 0;

          // Create series
          function createSeries(field, name) {
            
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;
            
            // Make it stacked
            series.stacked = true;
            
            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            
            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            
            return series;
          }

          createSeries("iso_1", "SERTIFIKASI ISO 9001:2000");
          createSeries("iso_2", "SERTIFIKASI 9001:2008");
          createSeries("iso_3", "SERTIFIKASI Proses Sertifikasi");
          createSeries("iso_4", "SERTIFIKASI Belum Bersertifikat");

          // Legend
          chart.legend = new am4charts.Legend();

          });
        }

    </script>
    <?php if (isset($script_plus)) echo $script_plus ?>
    </body>
</html>


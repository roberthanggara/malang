<!DOCTYPE html>
<html>
<head>
	<title>pendidikan all</title>
	<style>
	body { background-color: #30303d; color: #fff; }
	#chartdiv {
	  width: 40%;
	  height: 300px;
	}

	</style>
</head>
<body>
	
	<?php
		if($option){
			print_r($option);
		}
	?>

	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_pd_pra"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_pd_pra" style="width: 100%; height: 600px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_pd_pra" border="1" style="border-collapse: collapse;">
					
				</table>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_dasar"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_dasar" style="width: 100%; height: 300px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_dasar" border="1" style="border-collapse: collapse;">
					
				</table>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_menengah_pertama"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_menengah_pertama" style="width: 100%; height: 300px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_menengah_pertama" border="1" style="border-collapse: collapse;">
					
				</table>
			</td>
		</tr>
	</table>
	
	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_menengah_atas"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_menengah_atas" style="width: 100%; height: 400px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_menengah_atas" border="1" style="border-collapse: collapse;">
					
				</table>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_khusus"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_khusus" style="width: 100%; height: 500px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_khusus" border="1" style="border-collapse: collapse;">
					
				</table>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="2" align="center"><b id="title_non_formal"></b></td>
		</tr>
		<tr valign="top">
			<td width="70%">
				<div id="chart_non_formal" style="width: 100%; height: 400px;"></div>
			</td>
			<td width="30%">
				<table width="100%" id="table_non_formal">
					
				</table>
			</td>
		</tr>
	</table>
	
	
	

<script src="<?php print_r(base_url())?>assets/amcharts4/core.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/charts.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/themes/dark.js"></script>
<script src="<?php print_r(base_url())?>assets/amcharts4/themes/animated.js"></script>


<script src="<?php print_r(base_url())?>assets/js/jquery-3.2.1.js"></script>


<script>
    var data = JSON.parse('<?php print_r($data_json);?>');
    console.log(data);

    $(document).ready(function(){
        set_data();
    });

    $("#select_kec").change(function(){
    	set_data();
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d")
        ];
      }
    }

    function set_data(){
        var kec = $("#select_kec").val();

        var data_pd_pra = data["pd_pra"]["item"][kec].item;
        	var keterangan_pd_pra = data["pd_pra"]["keterangan"];

        var data_dasar = data["dasar"]["item"][kec].item;
        	var keterangan_dasar = data["dasar"]["keterangan"];

        var data_menengah_pertama = data["menengah_pertama"]["item"][kec].item;
        	var keterangan_menengah_pertama = data["menengah_pertama"]["keterangan"];

        var data_menengah_atas = data["menengah_atas"]["item"][kec].item;
        	var keterangan_menengah_atas = data["menengah_atas"]["keterangan"];

        var data_khusus = data["khusus"]["item"][kec].item;
        	var keterangan_khusus = data["khusus"]["keterangan"];

        var data_non_formal = data["non_formal"]["item"][kec].item;
        	var keterangan_non_formal = data["non_formal"]["keterangan"];
        

        create_chart(data_pd_pra, "chart_pd_pra", keterangan_pd_pra);
        create_chart(data_dasar, "chart_dasar", keterangan_dasar);
        create_chart(data_menengah_pertama, "chart_menengah_pertama", keterangan_menengah_pertama);
        create_chart(data_menengah_atas, "chart_menengah_atas", keterangan_menengah_atas);
        create_chart(data_khusus, "chart_khusus", keterangan_khusus);
        create_chart(data_non_formal, "chart_non_formal", keterangan_non_formal);


        create_table(data_pd_pra, "table_pd_pra", "title_pd_pra", keterangan_pd_pra);
        create_table(data_dasar, "table_dasar", "title_dasar", keterangan_dasar);
        create_table(data_menengah_pertama, "table_menengah_pertama", "title_menengah_pertama", keterangan_menengah_pertama);
        create_table(data_menengah_atas, "table_menengah_atas", "title_menengah_atas", keterangan_menengah_atas);
        create_table(data_khusus, "table_khusus", "title_khusus", keterangan_khusus);
        create_table(data_non_formal, "table_non_formal", "title_non_formal", keterangan_non_formal);
    }

    function create_table(data, id_table, id_header, keterangan){

    	console.log(data);
        var str_table = "<thead>"+
							"<tr>"+
								"<th>Keterangan</th>"+
								"<th>Negeri</th>"+
								"<th>Swasta</th>"+
								"<th>Total</th>"+
							"</tr>"+
						"</thead>"+
						"<tbody>";

		var tn = 0;
		var ts = 0;
		var ta = 0;

        for(let i in data){
            str_table += "<tr>"+
                            "<td>"+data[i].nama+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].n))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].s))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].a))+"</td>"+
                        "</tr>";

            tn += data[i].n;
            ts += data[i].s;
            ta += data[i].a;
        }
        	str_table += "<tr>"+
                            "<td>Total</td>"+
                            "<td align=\"right\">"+tn+"</td>"+
                            "<td align=\"right\">"+ts+"</td>"+
                            "<td align=\"right\">"+ta+"</td>"+
                        "</tr>";

        str_table += 	"</tbody>";

        $("#"+id_table).html(str_table);
        $("#"+id_header).html(keterangan);
    }

    function create_chart(data_chart, id_chart, title){
  		// console.log(data_chart);
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        am4core.useTheme(am4themes_dark);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 200;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 200;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "nama";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueX}[/]";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            // categoryLabel.label.text = "{name}";
            categoryLabel.label.text = name;
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
            // console.log(categoryLabel);
        }

        createSeries("n", "Negeri");
		createSeries("s", "Swasta");

        }); // end am4core.ready()
    }

    function create_chart1(data_chart, id_chart, title){
        am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
		// Themes end

		// Create chart instance
		var chart = am4core.create(id_chart, am4charts.XYChart);

		// Add data
		chart.data = data_chart;

		chart.legend = new am4charts.Legend();
		chart.legend.position = "right";

		// Create axes
		var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "nama";
		categoryAxis.renderer.grid.template.opacity = 0;
		categoryAxis.numberFormatter.numberFormat = "#";

		let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 250;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 250;

		var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		valueAxis.renderer.grid.template.opacity = 0;
		valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
		valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
		valueAxis.renderer.ticks.template.length = 10;
		valueAxis.renderer.line.strokeOpacity = 0.5;
		valueAxis.renderer.baseGrid.disabled = true;
		valueAxis.renderer.minGridDistance = 40;

		// Create series
		function createSeries(field, name) {
		  var series = chart.series.push(new am4charts.ColumnSeries());
		  series.dataFields.valueX = field;
		  series.dataFields.categoryY = "nama";
		  series.stacked = true;
		  series.name = name;
		  
		  var labelBullet = series.bullets.push(new am4charts.LabelBullet());
		  labelBullet.locationX = 0.5;
		  labelBullet.label.text = "{valueX}";
		  labelBullet.label.fill = am4core.color("#fff");
		}

		createSeries("n", "Negeri");
		createSeries("s", "Swasta");

		}); // end am4core.ready()

    }
</script>
</body>
</html>
<!DOCTYPE html>

<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
       
    <link href="<?php echo base_url("adminpress/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/dark/css/colors/blue-dark.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist-init.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"); ?>" rel="stylesheet">
     <link href="<?php echo base_url("adminpress/css/colors/blue-dark.css"); ?>" rel="stylesheet" id="theme">
    
    
    <!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
  <!--   AMCHART JS  -->

    <script src="<?php print_r(base_url());?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/animated.js"></script>
    <script src="<?php print_r(base_url());?>assets/amcharts4/themes/dark.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
 
  <!--   maps button css -->
    <link href="<?php echo base_url("map/css/style.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("map/css/style1.css"); ?>" rel="stylesheet">

<style>
            #box1{
                width:500px;
                height:250px;
                background:green;
                border-radius: 5px;
            }
            #box2{
                width:500px;
                height:250px;
                background:#ef2626;
                border-radius: 8px;
            }
            
            .floating-box {
                display: inline-block;
                width: 500px;
                height: 200px;
                margin: 10px;
                border: 3px solid #73AD21;  
}

            .after-box {
                 border: 3px solid red; 
}

        </style>
<style>
        table {
            font-size: 14px;
            font-color:#ffffff;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>

    </head>
   <body class="fix-header fix-sidebar card-no-border">
         <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-icon.png"); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url("adminpress/assets/images/logo-light-icon.png");?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url("adminpress/assets/images/logo-text.png"); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url("adminpress/assets/images/logo-light-text.png"); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Diskominfo</h4>
                                                <p class="text-muted">kominfo@gmail.com</p></div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"><img src="<?php echo base_url("adminpress/assets/images/users/1.jpg"); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5>Pemerintah Kota Malang</h5>
                    </div>
                </div>
                <!-- End User profile text-->
               <!-- MENU SIDEBAR -->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sub_utama" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">Dashboard Data Pendidikan</a></li>
                                 <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rd">Rangkuman Data Sekolah Dasar</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_rmp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                  <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikannew/index_ma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>

                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Dispendukcapil</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li>
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>bkd/Bpkadapi/" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>welcome/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>  
                        </li>
                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/get_pengaduan_rekap">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_FountainTap">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/pdammain/index_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Pendapatan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li>
                        <!-- Menu SIPEX Belanja -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                         </ul>
                        </li>
                        <!-- Menu SIPEX Pembiayaan -->
                         <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                
                         </ul>
                        </li>
                        <!-- Menu Dinas dan Lembaga -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga" aria-expanded="false"><i class="fa fa-bank"></i><span class="hide-menu">Dinas dan Lembaga</span></a>  
                        </li>
                         <!-- Menu Sarana Prasarana-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Sarana dan Prasarana</span></a> 
                        </li>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
             <div class="container-fluid">
                
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
              <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Dinas dan Lembaga</h4>
                                <h4 class="card-subtitle">Pemerintah Kota Malang </h4>
                                <!-- Nav tabs -->
                                <div class="vtabs customvtab" >
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tap1" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down" >Badan dan Kantor</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Bagian</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap3" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Daftar Eksekutif</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap4" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Daftar Forkopimda</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap5" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Daftar Legislatif</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap6" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Dinas</span></a> </li>
                                         <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tap7" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Kecamatan dan Kelurahan</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tap1" role="tabpanel">
                                            <div class="p-20">
                        <!-------------------------------------------- BADAN DAN KANTOR 2.0-------------------------------------------->
                                    <div class="col-lg-8 offset-lg-1">
                                        <h4 class="card-title">Badan dan Kantor</h4>
                                        <h6 class="card-subtitle">Pemerintahan Kota Malang</h6>
                                       
                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                                          INSPEKTORAT
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body">
                                                      <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Drs. ABDUL MALIK, M.Pd.</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Inspektur Kota Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Gajah Mada No. 2A</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 364450, 321276</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td align="center"><strong> :</strong></td>
                                                        <td><a href="http://inspektorat.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://inspektorat.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>inspektorat@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2" aria-expanded="false" aria-controls="collapseTwo">
                                             BADAN PERENCANAAN, PENELITIAN DAN PENGEMBANGAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body">
                                                     <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong> DWI RAHAYU, SH, M.Hum.</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu No. 1</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 366922</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><a href="http://barenlitbang.malangkota.go.id/"><strong>http://barenlitbang.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>barenlitbang@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
                                              BADAN PELAYANAN PAJAK DAERAH
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Ir. ADE HERAWANTO, MT</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Badan Pelayanan Pajak Daerah (BP2D)</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong> Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Telp. (0341) 751532</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong> :</strong></td>
                                                        <td><a href="http://bppd.malangkota.go.id/"><strong>http://bppd.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bppd@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colaps 4 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse4" aria-expanded="false" aria-controls="collapseFour">
                                              BADAN KEPEGAWAIAN DAERAH
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Dra. ANITA SUKMAWATI<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Badan Kepegawaian Daerah</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 328829</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website </strong></td>
                                                        <td><strong> :</strong></td>
                                                        <td><a href="http://bkd.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://bkd.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bkd@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                             <!-- colaps 5 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse5" aria-expanded="false" aria-controls="collapseFive">
                                              BADAN KESATUAN BANGSA DAN POLITIK
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>ZULKIFLI AMRIZAL, S.Sos, M.Si<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Badan Kesatuan Bangsa dan Politik</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Jend. A. Yani 98 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 491180</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website </strong></td>
                                                        <td><strong> : </strong></td>
                                                        <td><a href="http://bakesbangpol.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://bakesbangpol.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bkb@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                             <!-- colaps 6 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse6" aria-expanded="false" aria-controls="collapseSix">
                                              BADAN PENGELOLA KEUANGAN DAN ASET DAERAH
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Drs. SUBKHAN<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Badan Pengelola Keuangan dan Aset Daerah </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 326025</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website </strong></td>
                                                        <td><strong> :</strong></td>
                                                        <td><a href="http://bpkad.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://bpkad.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bpkad.malangkota.go.id@gmail.com</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                           <!-- colaps 7 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion6" href="#collapse7" aria-expanded="false" aria-controls="collapseSeven">
                                              BADAN PENANGGULANGAN BENCANA DAERAH
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Drs. ALIE MULYANTO, MM</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Pelaksana Badan Penanggulangan Bencana Daerah (BPBD)</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong> Jl. Danau Ranau Raya 1A Sawojajar &#8211; Kota Malang<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong><span class="LrzXr zdqRlf kno-fv">(0341) 3021657</span></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Websita</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><a href="http://bpbd.malangkota.go.id/"><strong>http://bpbd.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bpbd@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colaps 8 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion8" href="#collapse9" aria-expanded="false" aria-controls="collapseEight">
                                              SATUAN POLISI PAMONG PRAJA
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse9" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong> Drs. PRIYADI, MM<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Satuan Polisi Pamong Praja</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Simpang Mojopahit No 1</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 353939</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><a href="http://satpolpp.malangkota.go.id/" target="_blank" rel="noopener noreferrer"><strong>http://satpolpp.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Email</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>satpol-pp@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <!-- -----------------------------TAB BAGIAN COLAPS 2.1  ------------------------------------------------- -->
                                        <div class="tab-pane  p-20" id="tap2" role="tabpanel">
                                    <div class="col-lg-8 offset-lg-1">
                                        <h4 class="card-title">Bagian</h4>
                                        <h6 class="card-subtitle">Daftar Bagian Sekretariat Daerah Kota Malang sebagai berikut :</h6>
                                       
                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                                        <!-- colap 11 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion21" href="#collapse11" aria-expanded="true" aria-controls="collapseOne">
                                             BAGIAN HUMAS
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse11" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>MUHAMMAD NUR WIDIANTO, S.Sos</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Humas</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Telp: 350782</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>(0341) 751532</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><a href="http://humas.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://humas.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bag-humas@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colap 21 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse21" aria-expanded="false" aria-controls="collapseTwo">
                                              BAGIAN KESEJAHTERAAN RAKYAT DAN KEMASYARAKATAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse21" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>&#8211;</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Kesejahteraan Rakyat dan Kemasyarakatan<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Simpang Majapahit 1A Malang, Telp. (0341) 364208</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong> Website</strong></td>
                                                        <td><strong> :</strong></td>
                                                        <td><strong> <a href="http://kesra.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kesra.malangkota.go.id</a></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>E-mail </strong></td>
                                                        <td width="12"><strong>:</strong></td>
                                                        <td width="262"><strong> bag-kesra@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colap 31 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse31" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN HUKUM
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse31" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table  cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>TABRANI, SH, M.Hum</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Hukum</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Telp : 320297</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><strong><a href="http://hukum.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://hukum.malangkota.go.id</a></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bag-hukum@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                             <!-- colap 41 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse41" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN PENGEMBANGAN PEREKONOMIAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse41" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>&#8211;</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Pengembangan Perekonomian<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 MalangTelp. (0341) 351155</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><a href="http://perekonomian.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://perekonomian.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bag-perekonomian@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- colap 51 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse51" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN UMUM
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse51" class="collapse" role="tabpanel" aria-labelledby="headingFive">
                                                    <div class="card-body">
                                                        <table  cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>&#8211;</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Umum</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Telp. (0341) 366065</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><a href="http://umum.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://umum.malangkota.go.id</a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bag-umum@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                              <!-- colap 61 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse61" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN ORGANISASI
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse61" class="collapse" role="tabpanel" aria-labelledby="headingSix">
                                                    <div class="card-body">
                                                      <table  cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>&#8211;</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Organisasi</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Tugu 1 Telp. (0341) 365657</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><a href="http://organisasi.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://organisasi.malangkota.go.id</a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>bag-organisasi@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colap 71 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse71" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN SUMBERDAYA ALAM DAN PENGEMBANGAN INFRASTRUKTUR
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse71" class="collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                    <div class="card-body">
                                                       <table  cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="1000"><strong>Drs. R. WIDJAJA SALEH PUTRA (Plt)</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Kepala Bagian Sumberdaya Alam dan Pengembangan Infrastruktur</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Jl. Tugu 1 Malang<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="59" height="17"><strong>Website</strong></td>
                                                            <td width="12">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="262"><a href="http://sda.malangkota.go.id/"><strong>http://sda.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>bag-sda@malangkota.go.id</strong></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- colap 81 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse81" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN LAYANAN PENGADAAN BARANG/JASA
                                            </a>
                                          </h5>
                                                </div>
                                               <div id="collapse81" class="collapse" role="tabpanel" aria-labelledby="headingEight">
                                                    <div class="card-body">
                                                       <table  cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="1000"><strong>Drs. R. WIDJAJA SALEH PUTRA</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Kepala Bagian Layanan Pengadaan Barang/Jasa</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>Jl. Simpang Mojopahit Telp. (0341) 324973</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="59" height="17"><strong>Website</strong></td>
                                                        <td width="12">
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td width="262"><a href="http://blp.malangkota.go.id/"><strong>http://blp.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td>
                                                        <div align="center"><strong>:</strong></div>
                                                        </td>
                                                        <td><strong>blp@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <!-- colap 91 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse91" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN PEMERINTAHAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse91" class="collapse" role="tabpanel" aria-labelledby="headingNine">
                                                    <div class="card-body">
                                                       <table  cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="1000"><strong>BOEDI UTOMO, SE, M.Si</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Kepala Bagian Pemerintahan</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Jl. Tugu 1 Telp. (0341) 320978</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="59" height="17"><strong>Website</strong></td>
                                                            <td width="12">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="262"><a href="http://pemerintahan.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://pemerintahan.malangkota.go.id</a></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>bag-pemerintahan@malangkota.go.id</strong></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                                <!-- colap 101 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion21" href="#collapse101" aria-expanded="false" aria-controls="collapseThree">
                                             BAGIAN KEUANGAN DAN PERLENGKAPAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse101" class="collapse" role="tabpanel" aria-labelledby="headingTen">
                                                    <div class="card-body">
                                                       <table  cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="1000"><strong>WORO TANTY POERWANDARI, SH</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Kepala Bagian Keuangan dan Perlengkapan<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>Jl. Simpang Majapahit 1 A Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="59" height="17"><strong>Website</strong></td>
                                                            <td width="12">
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td width="262"><a href="http://perlengkapan.malangkota.go.id/"><strong>http://perlengkapan.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td>
                                                            <div align="center"><strong>:</strong></div>
                                                            </td>
                                                            <td><strong>bag-perlengkapan@malangkota.go.id</strong></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>


                                      <div class="tab-pane p-20" id="tap3" role="tabpanel">
                                            <!-- breadcum -->
                                                        <div class="top-main-spacer"></div>
                                                        <div id="post" class="floatright col span_9 clr">
                                                            <div id="page-heading">
                                                                <h1>Daftar Eksekutif</h1>       
                                                            </div><!-- /page-heading -->
                                                            <article id="post" class="clearfix">
                                                                                    <div class="entry clearfix">    
                                                                    <p>Lembaga eksekutif di lingkungan Pemerintah Kota Malang meliputi Sekretariat Daerah, Bagian, Dinas, dan Badan.</p>

                                            <h3 style="color:#00BFFF"><b>KEPALA DAERAH</b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: center; width: 161px;">
                                                            <p align="center"><strong>NAMA</strong></p>
                                                            </td>
                                                            <td style="text-align: center; width: 198px;">
                                                            <p align="center"><b>ALAMAT</b></p>
                                                            </td>
                                                            <td style="text-align: center; width: 156px;">
                                                            <p align="center"><b>JABATAN</b></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left; width: 161px;"><strong>Drs. H. SUTIAJI<br />
                                                            </strong></td>
                                                            <td style="text-align: left; width: 198px;">Jl. Tugu 1, Telp. (0341) 362704, Jl. Ijen 2, Telp. (0341) 362785</td>
                                                            <td style="text-align: left; width: 156px;">WALI KOTA</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left; width: 161px;"><strong>Ir. H. SOFYAN EDI JARWOKO<br />
                                                            </strong></td>
                                                            <td style="text-align: left; width: 198px;">Jl. Tugu 1, Telp. (0341) 320220</td>
                                                            <td style="text-align: left; width: 156px;">WAKIL WALI KOTA</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h3 style="color:#00BFFF"><b>SEKRETARIAT DAERAH</b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                    <tbody>
                                                        <tr style="height: 56px;">
                                                            <td style="height: 56px; text-align: center;" width="199">
                                                            <p align="center"><b>NAMA</b></p>
                                                            </td>
                                                            <td style="height: 56px; text-align: center;" width="233">
                                                            <p align="center"><b>ALAMAT</b></p>
                                                            </td>
                                                            <td style="height: 56px; text-align: center;" width="186">
                                                            <p align="center"><b>JABATAN</b></p>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="height: 48px;" width="199"><b><strong>Drs. WASTO, SH, MH</strong></b></td>
                                                            <td style="height: 48px;" width="233">Jl. Tugu 1 Malang, Telp. (0341) 325644, 366065</td>
                                                            <td style="height: 48px;" width="186">Sekretaris Daerah Kota Malang</td>
                                                        </tr>
                                                        <tr style="height: 72px;">
                                                            <td style="height: 72px;" width="199"><strong>SRI WINARNI, SH</strong></td>
                                                            <td style="height: 72px;" width="233">Jl. Tugu 1 Malang, Telp. (0341) 369555</td>
                                                            <td style="height: 72px;" width="186">Asisten Pemerintahan dan Kesejahteraan Sosial (Asisten 1)</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="height: 48px;" width="199"><strong>Ir. DIAH AYU KUSUMA DEWI, MT</strong></td>
                                                            <td style="height: 48px;" width="233">Jl. Tugu 1 Malang,Telp. (0341) 328553</td>
                                                            <td style="height: 48px;" width="186">Asisten Perekonomian (Asisten 2)</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="height: 48px;" width="199"><strong>Drs. NUZUL NURCAHYONO<br />
                                                            </strong></td>
                                                            <td style="height: 48px;" width="233">Jl. Tugu No 1 Malang, Telp. (0341)</td>
                                                            <td style="height: 48px;" width="186">Asisten Administrasi Umum (Asisten 3)</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="height: 48px;" width="199"><b><strong>Drs. SUPRIYADI, M.Pd</strong><br />
                                                            </b></td>
                                                            <td style="height: 48px;" width="233">Jl. Tugu No. 1 Malang, Telp. (0341) 351600</td>
                                                            <td style="height: 48px;" width="186">Staf Ahli Bidang Ekonomi dan Keuangan</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="height: 48px;" width="199"><strong>Drs. AGOES EDY POETRANTO, MM</strong></td>
                                                            <td style="height: 48px;" width="233">Jl. Tugu No. 1 Malang, Telp. (0341) 351600</td>
                                                            <td style="height: 48px;" width="186">Staf Ahli Bidang Hukum, Pemerintahan dan Politik</td>
                                                        </tr>
                                                        <tr style="height: 120px;">
                                                            <td style="height: 120px;" width="199"><strong>Dr. dr. ASIH TRI RACHMI N., MM</strong></td>
                                                            <td style="height: 120px;" width="233">Jl. Tugu No. 1 Malang, Telp. (0341) 351600</td>
                                                            <td style="height: 120px;" width="186">Staf Ahli Pembangunan, Kesejahteraan Rakyat dan Sumberdaya Manusia</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h3 style="color:#00BFFF"><b>BAGIAN</b><b></b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                    <tbody>
                                                        <tr style="height: 24px;">
                                                            <td style="text-align: center; width: 218.75px; height: 24px;"><strong>NAMA</strong></td>
                                                            <td style="text-align: center; width: 351.55px; height: 24px;"><strong>ALAMAT</strong></td>
                                                            <td style="text-align: center; width: 265.367px; height: 24px;"><strong>JABATAN</strong></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72px;" rowspan="3"><strong>BOEDI UTOMO, SE, M.Si<br />
                                                            </strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Tugu 1 Malang, Telp. (0341) 320978</td>
                                                            <td style="width: 265.367px; height: 72px;" rowspan="3">Kepala Bagian Pemerintahan</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://pemerintahan.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://pemerintahan.malangkota.go.id/</a></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-pemerintahan@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72px;" rowspan="3"><strong>TABRANI, SH, M.Hum.</strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Tugu 1 Malang, Telp. (0341) 320297</td>
                                                            <td style="width: 265.367px; height: 72px;" rowspan="3">Kepala Bagian Hukum</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website:<a href="http://hukum.malangkota.go.id/" target="_blank" rel="noopener noreferrer"> http://hukum.malangkota.go.id/</a></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-hukum@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72.55px;" rowspan="3"><strong>&#8211;</strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Tugu 1 Malang, Telp. (0341) 365657</td>
                                                            <td style="width: 265.367px; height: 72.55px;" rowspan="3">Kepala Bagian Organisasi</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://organisasi.malangkota.go.id/">http://organisasi.malangkota.go.id/</a></td>
                                                            </tr>
                                                            <tr style="height: 24.55px;">
                                                            <td style="width: 351.55px; height: 24.55px;">Email: bag-organisasi@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="width: 218.75px; height: 96px;" rowspan="3"><strong>Drs. R. WIDJAJA SALEH PUTRA</strong></td>
                                                            <td style="width: 351.55px; height: 48px;">Jl. Simpang Mojopahit Malang, Telp. (0341) 324973</td>
                                                            <td style="width: 265.367px; height: 96px;" rowspan="3">Kepala Bagian Layanan Pengadaan Barang/Jasa</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://blp.malangkota.go.id/">http://blp.malangkota.go.id/</a></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: blp@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72px;" rowspan="3"><strong>&#8211;</strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Tugu 1 Malang, Telp. (0341) 351155</td>
                                                            <td style="width: 265.367px; height: 72px;" rowspan="3">Kepala Bagian Pengembangan Perekonomian</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://bapeko.malangkota.go.id/">http://bapeko.malangkota.go.id/</a></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-perekonomian@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72px;" rowspan="3"><strong>WORO TANTY POERWANDARI, SH</strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Simpang Majapahit 1 A Malang, Telp. (0341)</td>
                                                            <td style="width: 265.367px; height: 72px;" rowspan="3">Kepala Bagian Keuangan dan Perlengkapan</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://perlengkapan.malangkota.go.id/">http://perlengkapan.malangkota.go.id/</a></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-perlengkapan@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 48px;">
                                                            <td style="width: 218.75px; height: 96px;" rowspan="3"><strong>&#8211;<br /></strong></td>
                                                            <td style="width: 351.55px; height: 48px;">Jl. Simpang Majapahit 1A Malang, Telp. (0341) 364208</td>
                                                            <td style="width: 265.367px; height: 96px;" rowspan="3">Kepala Bagian Kesejahteraan Rakyat dan Kemasyarakatan</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://kesra.malangkota.go.id/">http://kesra.malangkota.go.id/</a></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-kesra@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 218.75px; height: 72px;" rowspan="3"><strong>MUHAMMAD NUR WIDIANTO, S.Sos</strong></td>
                                                            <td style="width: 351.55px; height: 24px;">Jl. Tugu 1 Malang, Telp (0341) 350782</td>
                                                            <td style="width: 265.367px; height: 72px;" rowspan="3">Kepala Bagian Humas</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://humas.malangkota.go.id/">http://humas.malangkota.go.id/</a></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-humas@malangkota.go.id</td>
                                                        </tr>
                                                        <tr style="height: 59px;">
                                                            <td style="width: 218.75px; height: 59px;"><strong>Drs. R. WIDJAJA SALEH PUTRA (Plt)</strong></td>
                                                            <td style="width: 351.55px; height: 59px;">Jl. Tugu 1 Malang</p>
                                                            <p>Website: <a href="http://sda.malangkota.go.id/">http://sda.malangkota.go.id/</a></p>
                                                            <p>Email: bag-sda@malangkota.go.id</td>
                                                            <td style="width: 265.367px; height: 59px;">Kepala Bagian Sumberdaya Alam dan Pengembangan Infrastruktur</td>
                                                        </tr>
                                                        <tr style="height: 29px;">
                                                            <td style="width: 218px; height: 77px;" rowspan="3"><strong>&#8211;</strong></td>
                                                            <td style="width: 351px; height: 29px;">Jl. Tugu 1 Malang, Telp. (0341) 366065</td>
                                                            <td style="width: 265.883px; height: 77px;" rowspan="3">Kepala Bagian Umum</td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Website: <a href="http://umum.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://umum.malangkota.go.id/</a></td>
                                                        </tr>
                                                        <tr style="height: 24px;">
                                                            <td style="width: 351.55px; height: 24px;">Email: bag-umum@malangkota.go.id</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <h3 style="color:#00BFFF"><b>SEKRETARIAT DEWAN PERWAKILAN RAKYAT DAERAH</b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                    <tbody>
                                                    <tr>
                                                    <td style="text-align: center;" width="233"><strong>NAMA</strong></td>
                                                    <td style="text-align: center;" width="357"><strong>ALAMAT</strong></td>
                                                    <td style="text-align: center;" width="283"><strong>JABATAN</strong></td>
                                                    </tr>
                                                    <tr>
                                                    <td width="233"><strong>Drs. MULYONO, M.Si<br />
                                                    </strong></td>
                                                    <td>Jl. Tugu 1A Malang, Telp. (0341) Website: http://dprd.malangkota.go.id/Email:sek-dprd@malangkota.go.id</td>
                                                    <td width="283">Sekretaris DPRD Kota Malang</td>
                                                    </tr>
                                                    <tr>
                                                    <td width="233"><strong>Drs. FARID WAHYUDI, MM</strong></td>
                                                    <td width="357">Jl. Tugu 1A Malang, Telp. (0341) Website: http://dprd.malangkota.go.id/Email:sek-dprd@malangkota.go.id</td>
                                                    <td width="283">Kepala Bagian Umum Sekretariat DPRD Kota Malang</td>
                                                    </tr>
                                                    <tr>
                                                    <td width="233"><strong>DICKY HARYANTO, SH, MM<br />
                                                    </strong></td>
                                                    <td width="357">Jl. Tugu 1A Malang, Telp. (0341) Website: http://dprd.malangkota.go.id/Email:sek-dprd@malangkota.go.id</td>
                                                    <td width="283">Kepala Bagian Hubungan Masyarakat dan Hubungan Antar Lembaga Sekretariat DPRD Kota Malang</td>
                                                    </tr>
                                                    </tbody>
                                            </table>
                                            </div>

                                            <h3 style="color:#00BFFF"><b>DINAS</b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                <tbody>
                                                    <tr style="height: 24px;">
                                                    <td style="text-align: center; height: 24px; width: 227.317px;"><strong>NAMA</strong></td>
                                                    <td style="text-align: center; height: 24px; width: 363.483px;"><strong>ALAMAT</strong></td>
                                                    <td style="text-align: center; height: 24px; width: 268.867px;"><strong>JABATAN</strong></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 227.317px;" rowspan="3"><strong>Dra. ZUBAIDAH, MM</strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Jl. Veteran 19 Malang, Telp. (0341) 551333</td>
                                                    <td style="height: 72px; width: 268.867px;" rowspan="3">Kepala Dinas Pendidikan (Disdik)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://diknas.malangkota.go.id/">http://diknas.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: disdik@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>dr. SUPRANOTO, M.Kes<br />
                                                    </strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Jl. Simpang Laksda Adisucipto 45 Malang, Telp.(0341) 406878, Fax.(0341) 406879</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Kesehatan (Dinkes)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dinkes.malangkota.go.id/">http://dinkes.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dinkes@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>dr. SUPRANOTO, M.Kes (Plt)</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Perkantoran Terpadu Gedung B Lt.3 Jl. Mayjen Sungkono Malang, Telp. (0341) 751534</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Tenaga Kerja (Disnaker)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://disnaker.malangkota.go.id/">http://disnaker.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: disnaker@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Dra. PENNY INDRIANI (Plt)</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Jl. Raya Sulfat 12 Malang 65123, Telp/Faks. (0341) 412266</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Sosial (Dinsos)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="https://dinsos.malangkota.go.id/" target="_blank" rel="noopener noreferrer">https://dinsos.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dinsos@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Dr. HANDI PRIYANTO, AP, M.SI<br />
                                                    </strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Jl. Raden Intan 1 Malang, Telp. (0341) 491140, 493826</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Perhubungan (Dishub)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="https://dishub.malangkota.go.id/">http://dishub.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dishub@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Dra. TRI WIDYANI P., M.Si<br />
                                                    </strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Perkantoran Terpadu Gedung A Lt. 4 Jl. Mayjen Sungkono Malang, Telp. (0341) 751550</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Komunikasi dan Informatika (Diskominfo)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://kominfo.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://kominfo.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: kominfo@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>IDA AYU MADE WAHYUNI, SH, M.Si</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Perkantoran Terpadu Gedung A Lt. 3 Jl. Mayjen Sungkono Malang, Telp. (0341) 751944</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Kebudayaan dan Pariwisata (Disbudpar)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website:<a href="http://budpar.malangkota.go.id/" target="_blank" rel="noopener noreferrer"> https://budpar.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: budpar@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 227.317px;" rowspan="3"><strong>Ir. HADI SANTOSO<br />
                                                    </strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Jl. Bingkil 1 Malang, Telp. (0341) 365226</td>
                                                    <td style="height: 72px; width: 268.867px;" rowspan="3">Kepala Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dpupr.malangkota.go.id/" target="_blank" rel="noopener noreferrer">https://dpupr.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dpupr@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 227.317px;" rowspan="3"><strong>Ir. HADI SANTOSO</strong><strong> (Plt)</strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Jl. Bingkil 1 Malang, Telp. (0341) 369377</td>
                                                    <td style="height: 72px; width: 268.867px;" rowspan="3">Kepala Dinas Perumahan dan Kawasan Permukiman (Disperkim)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dpkp.malangkota.go.id/">https://dpkp.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dpkp@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Drs. WAHYU SETIANTO, MM</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Jl. Simp. Terusan Danau Sentani 3 Sawojajar, Telp. (0341) 716546</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Perdagangan</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://perdagangan.malangkota.go.id/">http://perdagangan.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: din-perdagangan@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Drs. WAHYU SETIANTO, MM (Plt)<br />
                                                    </strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Perkantoran Terpadu Gedung A Lt.3 Jl. Mayjen Sungkono Malang, Telp/Faks. (0341) 751544</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Perindustrian (Disperin)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://disperin.malangkota.go.id/">http://disperin.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: disperin@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 227.317px;" rowspan="3"><strong>Dra. TRI WIDYANI P., M.Si (Plt)<br />
                                                    </strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Jl. R. Panji Suroso 18 Malang, Telp. (0341) 496264</td>
                                                    <td style="height: 72px; width: 268.867px;" rowspan="3">Kepala Dinas Koperasi dan Usaha Mikro</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dinkop.malangkota.go.id/">http://dinkop.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dinkop@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48.1333px;">
                                                    <td style="height: 96.1333px; width: 227.317px;" rowspan="3"><strong><span class="fontstyle0">Ir. SAPTO PRAPTO SANTOSO,<br />
                                                    M.Si</span></strong></td>
                                                    <td style="height: 48.1333px; width: 363.483px;">Jl. A. Yani Utara 202 Arjosari Malang, Telp. (0341) 491914</td>
                                                    <td style="height: 96.1333px; width: 268.867px;" rowspan="3">Kepala Dinas Pertanian dan Ketahanan Pangan</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website:<a href="http://pertanian.malangkota.go.id/"> http://pertanian.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: din-pertanian@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 227.317px;" rowspan="3"><strong>SUWARJANA, SE, MM</strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Jl. Ijen No. 30A Malang, Telp. (0341) 362005</td>
                                                    <td style="height: 72px; width: 268.867px;" rowspan="3">Kepala Dinas Perpustakaan Umum dan Arsip Daerah</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://perpustakaan.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://perpustakaan.malangkota.go.id</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: perpustakaan@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>IDA AYU MADE WAHYUNI, SH, M.Si (Plt)</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Jl. Tenes Malang, Telp. (0341) 324372 Fax. (0341) 324375</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Kepemudaan dan Olahraga (Dispora)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dispora.malangkota.go.id/">http://dispora.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dispora@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 34px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>Dra. RINAWATI, MM</strong></td>
                                                    <td style="height: 34px; width: 363.483px;">Jl. Mojopahit Malang, Telp. (0341) 331600</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Lingkungan Hidup (DLH)</td>
                                                    </tr>
                                                    <tr style="height: 31px;">
                                                    <td style="height: 31px; width: 363.483px;">Website: <a href="http://dlh.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://dlh.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 31px;">
                                                    <td style="height: 31px; width: 363.483px;">Email: dlh@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 27px;">
                                                    <td style="height: 67px; width: 227.317px;" rowspan="3"><strong>Dra. PENNY INDRIANI</strong></td>
                                                    <td style="height: 27px; width: 363.483px;">Jl. Ki Ageng Gribig Malang, Telp. (0341) 717744</td>
                                                    <td style="height: 67px; width: 268.867px;" rowspan="3">Kepala Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)</td>
                                                    </tr>
                                                    <tr style="height: 18px;">
                                                    <td style="height: 18px; width: 363.483px;">Website: <a href="http://dp3ap2kb.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://dp3ap2kb.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 22px;">
                                                    <td style="height: 22px; width: 363.483px;">Email: dp3ap2kb@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px; width: 227.317px;" rowspan="3"><strong>ERIK SETYO SANTOSO, ST, MT</strong></td>
                                                    <td style="height: 48px; width: 363.483px;">Perkantoran Terpadu Gedung A Lt.2 Jl. Mayjen Sungkono Malang</td>
                                                    <td style="height: 96px; width: 268.867px;" rowspan="3">Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dpmptsp.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://dpmptsp.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dmptsp@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px; width: 363.483px;" rowspan="3"><strong>Dra. ENY HARI SUTIARNY, MM</strong></td>
                                                    <td style="height: 24px; width: 363.483px;">Perkantoran Terpadu Gedung A Lt.2 Jl. Mayjen Sungkono Malang, Telp. (0341) 751535</td>
                                                    <td style="height: 72px; width: 363.483px;" rowspan="3">Kepala Dinas Kependudukan dan Pencatatan Sipil (Dispendukcapil)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Website: <a href="http://dispendukcapil.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://dispendukcapil.malangkota.go.id</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px; width: 363.483px;">Email: dispendukcapil@malangkota.go.id</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h3 style="color:#00BFFF"><b>INSPEKTORAT</b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                <tbody>
                                                    <tr>
                                                    <td width="199">
                                                    <p align="center"><b>NAMA</b></p>
                                                    </td>
                                                    <td width="235">
                                                    <p align="center"><b>ALAMAT</b></p>
                                                    </td>
                                                    <td width="185">
                                                    <p align="center"><b>JABATAN</b></p>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <td width="199"><b>Drs. ABDUL MALIK, M.Pd<br />
                                                    </b></td>
                                                    <td width="235">Jl. Gajah Mada 2 Malang, Telp. (0341) 364450, 321276</p>
                                                    <p>Website: http://inspektorat.malangkota.go.id/</p>
                                                    <p>Email: inspektorat@malangkota.go.id</p></td>
                                                    <td width="185">Inspektur</td>
                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>

                                            <h3 style="color:#00BFFF"><b>BADAN<br />
                                            </b></h3>
                                            <div class="table-responsive">
                                                <table class="table color-table muted-table">
                                                    <tbody>
                                                    <tr style="height: 24px;">
                                                    <td style="text-align: center; height: 24px;" width="233"><strong>NAMA</strong></td>
                                                    <td style="text-align: center; height: 24px;" width="357"><strong>ALAMAT</strong></td>
                                                    <td style="text-align: center; height: 24px;" width="283"><strong>JABATAN</strong></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px;" rowspan="3" width="233"><strong>DWI RAHAYU, SH, M.Hum.</strong></td>
                                                    <td style="height: 24px;" width="357">Jl. Tugu 1 Malang, Telp. (0341) 328771</td>
                                                    <td style="height: 72px;" rowspan="3" width="283">Kepala Badan Perencanaan, Penelitian dan Pengembangan</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://barenlitbang.malangkota.go.id/">http://barenlitbang.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: barenlitbang@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48.1333px;">
                                                    <td style="height: 96.1333px;" rowspan="3" width="233"><strong>Drs. PRIYADI, MM</strong></td>
                                                    <td style="height: 48.1333px;" width="357">Jl. Simpang Mojopahit 1 Malang, Telp. (0341) 353939</td>
                                                    <td style="height: 96.1333px;" rowspan="3" width="283">Kepala Satuan Polisi Pamong Praja (Satpol PP)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://satpolpp.malangkota.go.id/">http://satpolpp.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: satpol-pp@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px;" rowspan="3" width="233"><strong>Dra. ANITA SUKMAWATI<br />
                                                    </strong></td>
                                                    <td style="height: 24px;" width="357">Jl. Tugu 1 Malang, Telp. (0341) 328829</td>
                                                    <td style="height: 72px;" rowspan="3" width="283">Kepala Badan Kepegawaian Daerah (BKD)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://bkd.malangkota.go.id/">http://bkd.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: bkd@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px;" rowspan="3" width="233"><strong>ZULKIFLI AMRIZAL, S.Sos, M.Si</strong></td>
                                                    <td style="height: 24px;" width="357">Jl. Jend. A. Yani 98 Malang, Telp. (0341) 491180</td>
                                                    <td style="height: 72px;" rowspan="3" width="283">Kepala Badan Kesatuan Bangsa dan Politik (Bakesbangpol)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://bakesbangpol.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://bakesbangpol.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: bakesbangpol@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px;" rowspan="3" width="233"><strong>Drs. SUBKHAN<br />
                                                    </strong></td>
                                                    <td style="height: 24px;" width="357">Jl. Tugu 1 MalangTelp. (0341) 326025</td>
                                                    <td style="height: 72px;" rowspan="3" width="283">Kepala Badan Pengelola Keuangan dan Aset Daerah (BPKAD)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://bpkad.malangkota.go.id/" target="_blank" rel="noopener noreferrer">http://bpkad.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: bpkad@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 72px;" rowspan="3" width="233"><strong>Drs. ALIE MULYANTO, MM</strong></td>
                                                    <td style="height: 24px;" width="357">Jl. Danau Ranau Raya No. 1-A, Sawojajar &#8211; Malang, Telp. (0341) 3021657</td>
                                                    <td style="height: 72px;" rowspan="3" width="283">Kepala Pelaksana Badan Penanggulangan Bencana Daerah (BPBD)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="https://bpbd.malangkota.go.id/">https://bpbd.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: bpbd@malangkota.go.id</td>
                                                    </tr>
                                                    <tr style="height: 48px;">
                                                    <td style="height: 96px;" rowspan="3" width="233"><strong>Ir. ADE HERAWANTO, MT</strong></td>
                                                    <td style="height: 48px;" width="357">Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang, Telp. (0341) 751532</td>
                                                    <td style="height: 96px;" rowspan="3" width="283">Kepala Badan Pelayanan Pajak Daerah (BP2D)</td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Website: <a href="http://bppd.malangkota.go.id/">http://bppd.malangkota.go.id/</a></td>
                                                    </tr>
                                                    <tr style="height: 24px;">
                                                    <td style="height: 24px;" width="357">Email: bppd@malangkota.go.id</td>
                                                    </tr>
                                                    </tbody>
                                            </table>
                                            </div>

                                            <p><em><span style="font-size: 8pt;">Update: Mutasi 17/04/2015</span></em></p>
                                            <div style="clear:both;"></div> 
                                                                </div><!-- /entry -->    
                                                    </div>
                                        </div>


                                        <!-- MULAI TAB DINAS -->
                                        <div class="tab-pane  p-20" id="tap4" role="tabpanel">
                                             <!-- breadcum -->
                                    <div class="top-main-spacer"></div>
                                    <div id="post" class="floatright col span_9 clr">
                                        <div id="page-heading">
                                            <h1>Daftar Forkopimda</h1>      
                                        </div><!-- /page-heading -->
                                        <article id="post" class="clearfix">
                                            <div class="entry clearfix">    
                                                <div class="table-responsive">
                                                    <table class="table color-bordered-table info-bordered-table">
                                                        <thead>
                                                            <tr>
                                                            <td style="text-align: center;" colspan="3"><strong>Forkopimda Koordinator</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <th style="text-align: center;">Nama Pejabat</th>
                                                            <th style="text-align: center;">Alamat Kantor</th>
                                                            <th style="text-align: center;">Alamat Rumah</th>
                                                            </tr>
                                                            <tr>
                                                            <td>DANREM 083/Baladhika Jaya</p>
                                                            <p><strong>Kolonel Inf. ZAINUDDIN<br />
                                                            </strong></td>
                                                            <td>Jl. Bromo 17 MalangTelp. (0341) 366 114, 324 600</td>
                                                            <td>Jl Panglima Sudirman 25 E Malang<br />
                                                            Telp. (0341) 364 622</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KEPALA BAKORWIL 3 MALANG</p>
                                                            <p><strong>Drs. BENNY SAMPIR WANTO, M.Si<br />
                                                            </strong></td>
                                                            <td>Jl. Simpang Ijen 2 MalangTelp. (0341) 551 321</td>
                                                            <td>Jl. Simpang Ijen 2 Malang Telp. (0341) 551 321</td>
                                                            </tr>
                                                            <tr>
                                                            <th colspan="3"><strong>Forkopimda Plus</strong></th>
                                                            </tr>
                                                            <tr>
                                                            <td>PANGLIMA DIVISI INFANTERI 2/ KOSTRAD</p>
                                                            <div class="tribun-mark">
                                                            <p><strong>Brigjen TNI Tri Yuniarto, S.AP., M.Si., M.Tr.(Han)</strong></p>
                                                            </div>
                                                            </td>
                                                            <td>Jl. Raya Singosari Telp. (0341) 458 478</td>
                                                            <td>Jl. Panglima Sudirman E 27 Malang Telp. (0341) 327 966</td>
                                                            </tr>
                                                            <tr>
                                                            <td>DAN LANUD ABD SALEH</p>
                                                            <p><strong>Marsma TNI HESLY PAAT</strong></td>
                                                            <td>Jl. Lanud Abd. SalehTelp. (0341) 791 718</td>
                                                            <td>Jl. Lanud Abd. Saleh Telp. (0341) 791 718</td>
                                                            </tr>
                                                            <tr>
                                                            <td>DAN LANAL MALANG</p>
                                                            <p><strong>Kolonel Laut (P) HREESANG WISANGGENI, SE<br />
                                                            </strong></td>
                                                            <td>Jl. Yos Sudarso 14 MalangTelp. (0341) 326 882, 325 881, 325 456</td>
                                                            <td>Jl. Yos Sudarso Malang Telp. (0341) 354 282</td>
                                                            </tr>
                                                            <tr>
                                                            <th colspan="3"><strong>Forkopimda Kota Malang</strong></th>
                                                            </tr>
                                                            <tr>
                                                            <td>WALIKOTA MALANG</p>
                                                            <p><strong>Drs. H. SUTIAJI<br />
                                                            </strong></td>
                                                            <td>Jl. Tugu Nomor 1 Malang Telp. (0341) 362 704</td>
                                                            <td>Jl. Tugu Nomor 1 Malang Telp. (0341) 362 704</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KOMANDAN KODIM 0833 MALANG KOTA</p>
                                                            <p><strong>Letkol Inf TOMMY ANDERSON</strong></td>
                                                            <td>Jl. Kahuripan 6 Telp. (0341) 364 155 Fax. 362 284</td>
                                                            <td>Jl. Kahuripan E 34 Malang Telp. (0341) 325 007</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KAPOLRESTA MALANG KOTA</p>
                                                            <p><strong>AKBP Dr. LEONARDUS HARAPANTUA SIMARMATA PERMATA, S.Sos, SIK, MH<br />
                                                            </strong></td>
                                                            <td>Jl. Jaksa Agung Suprapto 19 Telp. (0341) 364 211</td>
                                                            <td>Jl. Ijen 40 Malang Telp. &#8211;</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KEPALA KEJAKSAAN NEGERI</p>
                                                            <h5 >ANDI DARMAWANGSA, SH, MH</h5>
                                                            </td>
                                                            <td>Jl. Simpang Panji Suroso No.5 Telp. (0341) 480 303, 480 304</td>
                                                            <td>Jl. Indragiri IV Malang Telp.</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KETUA PENGADILAN NEGERI</p>
                                                            <p><strong><span class="slide-text"><span class="show-text">NURULI MAHDILIS, SH., MH.</span></span></strong></td>
                                                            <td>Jl. Ahmad Yani Utara 198 Telp. (0341) 491 254, 495 171</td>
                                                            <td>Jl. Raden Intan 2 Malang Telp. (0341) 494 213</td>
                                                            </tr>
                                                            <tr>
                                                            <td>KETUA DPRD KOTA MALANG</p>
                                                            <p><strong>I MADE RIANDIANA KARTIKA<br /></strong></p>
                                                            </td>
                                                            <td>Jl. Tugu No 1A Malang Telp. 362 602, 325 617</td>
                                                            <td>l. Bantaran Gg. V No. 11-A Malang Telp. 08125216500</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <p>&nbsp;</p>
                                                <div style="clear:both;"></div> 
                                            </div><!-- /entry -->    
                                            </div>
                                        </div>


                                        <div class="tab-pane p-20" id="tap5" role="tabpanel" >
                                            <div class="top-main-spacer"></div>

                                            <div id="post" class="floatright col span_9 clr">
                                                <div id="page-heading">
                                                <center>
                                                    <h1>Daftar Legislatif</h1>      
                                                </div>
                                                <!-- /page-heading -->
                                               
                                                    <div class="entry clearfix">    
                                                        <h2><center><img src="<?php echo base_url("header_cover/assets/images/dprd.png"); ?>" alt="" width="130" height="126"  /></center></h2>
                                                        <h2 style="text-align: center;"><strong>SUSUNAN KEANGGOTAAN FRAKSI DPRD KOTA MALANG</strong><br />
                                                        <strong> MASA JABATAN 2019-2024</strong><br />
                                                        <strong> Jl. TUGU No 1 A &#8211; TELP : 0341.325617</strong></h2>
                                                        <ol>

                                                            <li><strong>Fraksi Partai Demokrasi Indonesia Perjuangan</strong><br />
                                                            <table class="table color-bordered-table info-bordered-table">
                                                            <tbody>
                                                            <tr>
                                                            <td style="width: 294.733px;">Ketua merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">EKO </span><span class="fontstyle0">HERDIYANTO</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Wakil Ketua merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">H. WANEDI</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Sekretaris merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">Drs. AGOES MARHAENTA, MH</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Wakil Sekretaris merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">NURUL </span><span class="fontstyle0">SETYOWATI, SE</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Bendahara merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">AMITHYA </span><span class="fontstyle0">RATNANGGANI </span><span class="fontstyle0">SIRRADUHITA, </span><span class="fontstyle0">SS</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Wakil Bendahara merangkap anggota</td>
                                                            <td style="width: 369.417px;"> <span class="fontstyle0">Hj. </span><span class="fontstyle0">LEA MAHDARINA, </span><span class="fontstyle0">ST</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 294.733px;">Anggota</td>
                                                            <td style="width: 369.417px;">
                                                            <ul>
                                                            <li>I MADE RIANDIANA KARTIKA, SE</li>
                                                            <li>IWAN MAHENDRA, S.Sos, M.AP</li>
                                                            <li>HARVAD KURNIAWAN R, SH</li>
                                                            <li>Dra. Rr. WIWIK SUKESI, M.Si</li>
                                                            <li>LULUK ZUHRIYAH</li>
                                                            <li>FERRY KURNIAWAN</li>
                                                            </ul>
                                                            </td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            <li><strong><strong><span class="fontstyle0">FRAKSI PARTAI </span><span class="fontstyle0">KEBANGKITAN </span><span class="fontstyle0">BANGSA<br />
                                                            </span></strong></strong></p>
                                                            <table class="table color-bordered-table info-bordered-table">
                                                            <tbody>
                                                            <tr>
                                                            <td style="width: 293px;"><span class="fontstyle0"> Penasehat merangkap anggota</span></td>
                                                            <td style="width: 368.9px;">
                                                            <ul>
                                                            <li><span class="fontstyle0">H. ABDURROCHMAN, </span><span class="fontstyle0">SH</span></li>
                                                            <li><span class="fontstyle0">ARIEF WAHYUDI, </span><span class="fontstyle0">SH</span></li>
                                                            </ul>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 293px;">Ketua merangkap anggota</td>
                                                            <td style="width: 368.9px;"> <span class="fontstyle0"> AHMAD FARIH SULAIMAN, S.Pd<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 293px;">Wakil Ketua merangkap anggota</td>
                                                            <td style="width: 368.9px;"> <span class="fontstyle0"> Drs. H. FATHOL ARIFIN, MH<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 293px;">Sekretaris merangkap anggota</td>
                                                            <td style="width: 368.9px;"> <span class="fontstyle0"> IKE KISNAWATI, SH<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 293px;">Wakil Sekretaris merangkap anggota</td>
                                                            <td style="width: 368.9px;"> <span class="fontstyle0"> HARTATIK, SE<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 293px;">Bendahara merangkap anggota</td>
                                                            <td style="width: 368.9px;"> <span class="fontstyle0"> ABDUL WAHID<br />
                                                            </span></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            <li><strong><strong> <span class="fontstyle0">FRAKSI PARTAI </span><span class="fontstyle0">KEADILAN SEJAHTERA<br />
                                                            </span></strong></strong></p>
                                                            <table class="table color-bordered-table info-bordered-table"><tbody>
                                                            <tr>
                                                            <td style="width: 300px;"><span class="fontstyle0">Ketua merangkap anggota</span></td>
                                                            <td style="width: 373.417px;"><span class="fontstyle0">TRIO </span><span class="fontstyle0">AGUS </span><span class="fontstyle0">PURWONO, S.TP</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 300px;">Wakil Ketua merangkap anggota</td>
                                                            <td style="width: 373.417px;"><span class="fontstyle0">H. ASMUALIK, </span><span class="fontstyle0">ST</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 300px;">Sekretaris merangkap anggota</td>
                                                            <td style="width: 373.417px;"><span class="fontstyle0">AHMAD FUAD RAHMAN, </span><span class="fontstyle0">SE</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 300px;">Anggota</td>
                                                            <td style="width: 373.417px;">
                                                            <ul>
                                                            <li> <span class="fontstyle0">H. BAYU </span><span class="fontstyle0">REKSO </span><span class="fontstyle0">AJI</span></li>
                                                            <li><span class="fontstyle0">H. ROKHMAD, </span><span class="fontstyle0">S.Sos</span></li>
                                                            <li><span class="fontstyle0">H. AKHDIYAT </span><span class="fontstyle0">SYABRIL </span><span class="fontstyle0">ULUM, </span><span class="fontstyle0">S.KOM, </span><span class="fontstyle0">MM</span></li>
                                                            </ul>
                                                            </td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            <li><strong><strong> <span class="fontstyle0">FRAKSI </span><span class="fontstyle0">GERINDRA<br />
                                                            </span></strong></strong></p>
                                                            <table class="table color-bordered-table info-bordered-table">
                                                            <tbody>
                                                            <tr style="height: 27.85px;">
                                                            <td style="width: 296px; height: 27.85px;"><span class="fontstyle0"> Penasehat merangkap anggota</span></td>
                                                            <td style="width: 371px; height: 27.85px;"><span class="fontstyle0">RIMZAH, </span><span class="fontstyle0">SIP</span></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 296px; height: 24px;">Ketua merangkap anggota</td>
                                                            <td style="width: 371px; height: 24px;"> <span class="fontstyle0"> Kol (Purn). Drs. DJOKO HIRTONO, SSTF, M.Si<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 296px; height: 24px;">Wakil Ketua merangkap anggota</td>
                                                            <td style="width: 371px; height: 24px;"> <span class="fontstyle0"> LELLY THRESIYAWATI<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 296px; height: 24px;">Sekretaris merangkap anggota</td>
                                                            <td style="width: 371px; height: 24px;"> <span class="fontstyle0"> RANDY GAUNG KUMARANING AL ISLAM<br />
                                                            </span></td>
                                                            </tr>
                                                            <tr style="height: 24px;">
                                                            <td style="width: 296px; height: 24px;">Bendahara merangkap anggota</td>
                                                            <td style="width: 371px; height: 24px;"> <span class="fontstyle0"> NURUL FARIDAWATI<br />
                                                            </span></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            <li><strong><strong> <span class="fontstyle0">FRAKSI </span><span class="fontstyle0">GOLKAR-NASDEM-PSI<br />
                                                            </span></strong></strong></p>
                                                            <table class="table color-bordered-table info-bordered-table">
                                                            <tbody>
                                                            <tr>
                                                            <td style="width: 302px;"><span class="fontstyle0">Ketua merangkap anggota</span></td>
                                                            <td style="width: 373.167px;"><span class="fontstyle0">HJ. </span><span class="fontstyle0">RETNO </span><span class="fontstyle0">SUMARAH, SE, MM</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 302px;">Wakil Ketua merangkap anggota</td>
                                                            <td style="width: 373.167px;"><span class="fontstyle0">Drs. </span><span class="fontstyle0">H. RAHMAN NURMALA, MM</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 302px;">Sekretaris merangkap anggota</td>
                                                            <td style="width: 373.167px;"><span class="fontstyle0">SURYADI, </span><span class="fontstyle0">S.Pd</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 302px;">Bendahara merangkap anggota</td>
                                                            <td style="width: 373.167px;"><span class="fontstyle0">EDDY WIDJANARKO, </span><span class="fontstyle0">S.AP</span></td>
                                                            </tr>
                                                            <tr>
                                                            <td style="width: 302px;">Anggota</td>
                                                            <td style="width: 373.167px;">
                                                            <ul>
                                                            <li> <span class="fontstyle0">GAGAH </span><span class="fontstyle0">SOERYO PAMOEKTI<br />
                                                            </span></li>
                                                            <li><span class="fontstyle0">DR. </span><span class="fontstyle0">JOSE </span><span class="fontstyle0">RIZAL </span><span class="fontstyle0">JOESOEF, SE, M.Si</span></li>
                                                            <li><span class="fontstyle0">Drs. </span><span class="fontstyle0">SUYADI, MM</span></li>
                                                            <li><span class="fontstyle0">MOH. ARIF BUDIARSO, </span><span class="fontstyle0">ST</span></li>
                                                            </ul>
                                                            </td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            <li><strong> <span class="fontstyle0">FRAKST </span><span class="fontstyle0">DAMAI </span><span class="fontstyle0">(DEMOKRAT-PAN-PERINDO)<br />
                                                            </span></strong></p>
                                                            <table class="table color-bordered-table info-bordered-table">
                                                            <tbody>
                                                            <tr>
                                                            <td><span class="fontstyle0">Ketua merangkap anggota</span></td>
                                                            <td><span class="fontstyle0"> H. IMRON </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td>Wakil Ketua merangkap anggota</td>
                                                            <td><span class="fontstyle0"> H. PUJIANTO, SE, M.Hum </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td>Sekretaris merangkap anggota</td>
                                                            <td><span class="fontstyle0"> H. LOOKH MAKHFUDZ, SS </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td>Bendahara merangkap anggota</td>
                                                            <td><span class="fontstyle0"> ALKASA SULIMA PRIYANTONO, SE </span></td>
                                                            </tr>
                                                            <tr>
                                                            <td>Anggota</td>
                                                            <td>
                                                            <ul>
                                                            <li><span class="fontstyle0">INDAH NURDIANA, </span><span class="fontstyle0">S.TP</span></li>
                                                            <li><span class="fontstyle0">WIWIK SULAIHA</span></li>
                                                            <li><span class="fontstyle0">H. EKO HADI </span><span class="fontstyle0">PURNOMO, SH</span></li>
                                                            </ul>
                                                            </td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            </li>
                                                            </ol>
                                                            <hr />
                                                            <p>Kantor DPRD Kota Malang terletak di Jl. Tugu No. 1A Telp. (0341) 325617 Malang 65119.</p>
                                                            <div style="clear:both;"></div> 
                                                        </div><!-- /entry -->  
                                                    </center>
                                                </div>
                                            </div>

                                            <!-- DINAS -->
                                        <div class="tab-pane  p-20" id="tap6" role="tabpanel"><!-- PANEL DINAS -->
                                    <div class="col-lg-12 offset-lg-12">
                                        <h4 class="card-title">DINAS</h4>
                                        <h6 class="card-subtitle">Pemerintah Kota Malang</h6>
                                        
                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                                           <!--  Colaps 1001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse1001" aria-expanded="true" aria-controls="collapseOne">
                                            DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SARU PINTU (DPMPTSP)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse1001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body">
                                                       <table>
                                                            <tbody>
                                                            <tr>
                                                            <td width="200"><strong>Pimpinan</strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><strong>ERIK SETYO SANTOSO, ST, MT</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="72"><strong>Jabatan</strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><strong>Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="72"><strong>Alamat </strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><strong>Perkantoran Terpadu Gedung A Lt. 2 Malang, </strong></p>
                                                            <p><strong>Jl. Mayjend. Sungkono Malang 65132</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="72"><strong>Telp.</strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><strong>(0341) 751942</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="72"><strong>Website</strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><a href="http://dpmptsp.malangkota.go.id/bppt"> <strong>http://dpmptsp.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            <tr>
                                                            <td width="72"><strong>E-mail </strong></td>
                                                            <td width="31"><strong>:</strong></td>
                                                            <td width="491"><strong>dmptsp@malangkota.go.id</strong></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 2001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2001" aria-expanded="false" aria-controls="collapseTwo">
                                             DINAS PERDAGANGAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2001" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><span style="font-size: 12pt;"><strong>Pimpinan</strong></span></td>
                                                        <td width="20"><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td width="397"><strong><span style="font-size: 12pt;">Drs. WAHYU SETIANTO, MM</span></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Jabatan</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>Kepala Dinas Perdagangan</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Alamat </strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>Jl. Simp. Terusan Danau Sentani 3 Malang</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Telp.</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>(0341) 716546</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Website</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>http://perdagangan.malangkota.go.id</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>E-mail </strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>perdagangan@malangkota.go.id</strong></span></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 3001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse3001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS KOMINFO
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse3001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table>
                                                        <tbody>
                                                        <tr>
                                                        <td width="200"><strong>Pimpinan</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><strong style="font-style: inherit;">Dra. TRI WIDYANI P, M.Si</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="75"><strong>Jabatan</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><strong style="font-style: inherit;">Kepala Dinas Komunikasi dan Informatika</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="75"><strong>Alamat </strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><strong style="font-style: inherit;">Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="75"><strong>Telp.</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><strong style="font-style: inherit;">(0341) 751550</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="75"><strong>Website</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><a style="font-weight: inherit; font-style: inherit;" href="http://kominfo.malangkota.go.id/"><strong style="font-style: inherit;">http://kominfo.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td width="75"><strong>E-mail </strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="31"><strong style="font-style: inherit;">:</strong></td>
                                                        <td style="font-weight: inherit; font-style: inherit;" width="491"><strong style="font-style: inherit;">kominfo@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 4001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse4001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS BUDAYA DAN PARIWISATA (DISBUDPAR)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse4001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><span style="font-size: 12pt;"><strong>Pimpinan</strong></span></td>
                                                        <td width="20"><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td width="700"><span style="font-size: 12pt;"><strong>IDA AYU MADE WAHYUNI, SH, M.Si</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Jabatan</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>Kepala Dinas Kebudayaan dan Pariwisata</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Alamat </strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142<br />
                                                        </strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>Telp.</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>(0341) 404515</strong></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><b>Website</b></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><a href="http://budpar.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://budpar.malangkota.go.id</strong></a></span></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><span style="font-size: 12pt;"><strong>E-mail </strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>:</strong></span></td>
                                                        <td><span style="font-size: 12pt;"><strong>budpar@malangkota.go.id</strong></span></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 5001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse5001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS TENAGA KERJA
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse5001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="12"><strong>:</strong></td>
                                                        <td width="700"><strong>dr. SUPRANOTO, M.Kes (Plt)<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Kepala Dinas Tenaga Kerja (Disnaker)<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 751534</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://disnaker.malangkota.go.id/"><strong>http://disnaker.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>disnaker@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 6001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse6001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PERINDUSTRIAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse6001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20"><strong>:</strong></td>
                                                        <td width="700"><strong>Drs. WAHYU SETIANTO, MM<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Plt. Kepala Dinas Perindustrian</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 751544</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"> <strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://disperin.malangkota.go.id/"><strong>http://disperin.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>disperin@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 7001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse7001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PENDIDIKAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse7001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20"><strong>:</strong></td>
                                                        <td width="700"><strong>Dra. ZUBAIDAH, MM</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Kepala Dinas Pendidikan</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Veteran 19 Malang 65145</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 551333</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://diknas.malangkota.go.id/" target="_blank" rel="noopener noreferrer"><strong>http://diknas.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>disdik@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                              <!--  Colaps 8001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse8001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS KESEHATAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse8001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="12"><strong>:</strong></td>
                                                        <td width="700"><strong>dr. SUPRANOTO, M.Kes</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Kepala Dinas Kesehatan</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Simp. Laksda Adisucipto 45 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 406878</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Faks.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 406879</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>dinkes@malangkota.go.id</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"> <strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://dinkes.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://dinkes.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                              <!--  Colaps 9001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse9001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PERHUBUNGAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse9001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="12"><strong>:</strong></td>
                                                        <td width="700"><strong>Dr. HANDI PRIYANTO, AP,<br />
                                                        M.Si</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Kepala Dinas Perhubungan</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Raden Intan 1 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 491140, 493826</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>dishub@malangkota.go.id</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://dishub.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://dishub.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                              <!--  Colaps 10001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse10001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PERTANIAN DAN KETAHANAN PANGAN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse10001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="12"><strong>:</strong></td>
                                                        <td width="700"> <strong><span class="fontstyle0">Ir. SAPTO PRAPTO SANTOSO,<br />
                                                        M.S</span></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Kepala Dinas Pertanian dan Ketahanan Pangan</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. A. Yani Utara 202 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 491914</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>din-pertanian@malangkota.go.id</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"> <strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://pertanian.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://pertanian.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 11001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse11001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PERUMAHAN DAN KAWASAN PERMUKIMAN (DISPERKIM)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse11001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20"><strong>:</strong></td>
                                                        <td width="700"><strong>Ir. HADI SANTOSO</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Plt. Kepala Dinas Perumahan dan Kawasan Permukiman</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Bingkil 1 Malang</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Telp.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>(0341) 369377</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>dpkp@malangkota.go.id</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://disperkim.malangkota.go.id/"><strong>http://disperkim.malangkota.go.id/</strong></a></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 12001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse12001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PEKERJAAN UMUM DAN PENATAAN RUANG (DPUPR)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse12001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="12"><strong>:</strong></td>
                                                            <td width="700"><strong>Ir. HADI SANTOSO</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong> Jl. Bingkil 1 Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>(0341) 365226</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dpupr@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><a href="http://dpupr.malangkota.go.id/"><strong>http://dpupr.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 13001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse13001" aria-expanded="false" aria-controls="collapseThree">
                                             DINAS SOSIAL
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse13001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="12"><strong>:</strong></td>
                                                            <td width="700"><strong>Dra. PENNY INDRIANI (Plt)<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Sosial;</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Sulfat No. 12 Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>(0341) 412266</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dinsos@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><a href="http://sosial.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://dinsos.malangkota.go.id</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 14001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse14001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS KOPERASI DAN USAHA MIKRO
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse14001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>Dra. TRI WIDYANI P., M.Si (Plt)<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Koperasi dan Usaha Mikro<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Panji Suroso 18 Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>(0341) 496264 </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dinkop@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong> Website</strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://dinkop.malangkota.go.id/"><strong>http://dinkop.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 15001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse15001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS KEPEMUDAAN DAN OLAHRAGA (DISPORA)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse15001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                         <table cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>IDA AYU MADE WAHYUNI, SH, M.Si (Plt)<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Kepemudaan dan Olahraga</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Tenes Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>(0341) 324372</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dispora@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website </strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://dispora.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://dispora.malangkota.go.id</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 16001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse16001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL (DISPENDUKCAPIL)
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse16001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                       <table cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>Dra. ENY HARI SUTIARNY, MM</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Kependudukan dan Pencatatan Sipil</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>(0341) 751535</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dispendukcapil@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website </strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://dispendukcapil.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://dispendukcapil.malangkota.go.id</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 17001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse17001" aria-expanded="false" aria-controls="collapseThree">
                                              DP3AP2KB
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse17001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>Dra. PENNY INDRIANI</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Ki Ageng Gribig Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong> Telp. (0341) 717744</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dp3ap2kb@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website </strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://dp3ap2kb.malangkota.go.id/"><strong>http://dp3ap2kb.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            <!--  Colaps 18001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse18001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS LINGKUNGAN HIDUP
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse18001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>Dra. RINAWATI, MM</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Lingkungan Hidup (DLH)</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Mojopahit Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Telp. (0341) 331600<br />
                                                            </strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>dlh@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website </strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://dlh.malangkota.go.id/"><strong>http://dlh.malangkota.go.id/</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                             <!--  Colaps 19001 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse19001" aria-expanded="false" aria-controls="collapseThree">
                                              DINAS PERPUSTAKAAN DAN ARSIP DAERAH
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse19001" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="800"><strong>SUWARJANA, SE, MM</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Kepala Dinas Perpustakaan Umum dan Arsip Daerah</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Ijen No. 30A Malang</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Telp.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Telp. (0341) 362005</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>perpustakaan@malangkota.go.id</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website </strong></td>
                                                            <td><strong>: </strong></td>
                                                            <td><a href="http://perpustakaan.malangkota.go.id/"><strong>http://digilib.malangkota.go.id</strong></a></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div><!-- /tutup colaps div -->
                                            

                                        </div>
                                    </div>
                                        </div><!-- /TUTUP PANEL DINAS -->


                                        <!-- MULAI TAB KEC -->
                                        <div class="tab-pane p-20" id="tap7" role="tabpanel">
                                             <div class="col-lg-12 offset-lg-1">
                                        <h4 class="card-title">Kecamatan dan Kelurahan di Pemerintah Kota Malang</h4>
                                        <h6 class="card-subtitle">Berikut merupakan Kecamatan dan Kelurahan di Linkup Pemerintah Kota Malang : </h6>
                                       
                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                        <!-- tab colaps 200.1 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2001" aria-expanded="true" aria-controls="collapseOne">
                                              KECAMATAN KLOJEN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body">
                                                        <div class="col-lg-12 offset-lg-1">
                                                    <h3 class="card-title">Kecamatan Klojen - Kota Malang</h3>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="20"><strong>Pimpinan</strong></td>
                                                        <td width="20"><strong>:</strong></td>
                                                        <td width="700"><strong>HERU MULYONO, SIP, MT</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Camat Klojen</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Surabaya 6 Telp. 0341-362468</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Faks.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>0341-362468</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong><a href="http://kecklojen.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kecklojen.malangkota.go.id</a></strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>kec-klojen@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                        <br>

                                                        <h2>Kelurahan-kelurahan di wilayah Kecamatan Klojen</h2>
                                                   
                                                    <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                                    <!-- tab colaps 2000.1 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse20001" aria-expanded="true" aria-controls="collapseOne">
                                                          Kelurahan Klojen
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-body">
                                                                  <table>
                                                                        <tbody>
                                                                        <tr>
                                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                        <td width="20"><strong>:</strong></td>
                                                                        <td width="600"><strong>NURHADI, S.Sos, M.AP</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Jabatan</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Lurah Klojen</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Alamat </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong> Jl Pattimura No. 51 Kode Pos 65111, Telp. 0341-325861</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Faks.</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>0341-325861</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Website</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><a href="http://kelklojen.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelklojen.malangkota.go.id</strong></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>E-mail </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>kel-klojen@malangkota.go.id</strong></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 2000.2 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingTwo">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20002" aria-expanded="false" aria-controls="collapseTwo">
                                                          Kelurahan Rampal Celaket
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="card-body">
                                                                   <table>
                                                                        <tbody>
                                                                        <tr>
                                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                        <td width="20"><strong>:</strong></td>
                                                                        <td width="600"> <strong>SABARDI, S.Ag, MM<br />
                                                                        </strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Jabatan</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Lurah Rampal Celaket</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Alamat </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Jl Kasembon 8 B Telp. 0341-353060</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Faks.</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>0341-353060</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Website</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><a href="http://kelrampalcelaket.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelrampalcelaket.malangkota.go.id</strong></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>E-mail </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>kel-rampalcelaket@malangkota.go.id</strong></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 2000.3 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20003" aria-expanded="false" aria-controls="collapseThree">
                                                          KELURAHAN KLOJEN
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>HENDRO TRI YULIANTO, SE</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Samaan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Kaliurang Barat 121 Telp.0341-352134</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-352134</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelsamaan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelsamaan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-samaan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 2000.4 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20004" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Kiduldalem
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                  <table>
                                                                   <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Johan Fuaddy, S.STP, M.Si<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kidul Dalem</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. MGR. S Pranoto 32A Telp. 0341-325764</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-325764</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkiduldalem.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkiduldalem.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kiduldalem@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 2000.5 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20005" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Sukoharjo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                        <tbody>
                                                                        <tr>
                                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                        <td width="20"><strong>:</strong></td>
                                                                        <td width="600"><strong>MUNADI, S.Sos, MM</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Jabatan</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Lurah Sukoharjo</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Alamat </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong> Jl Arismunandar No. 54 Telp.0341-327767</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Faks.</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>0341-327767</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Website</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><a href="http://kelsukoharjo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelsukoharjo.malangkota.go.id</strong></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>E-mail </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>kel-sukoharjo@malangkota.go.id</strong></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- tab colaps 20005 -->
                                                         <!-- tab colaps 2000.6-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20006" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Kasin
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20006" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                  <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>HERI SUGIANA, S.Pd</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kasin</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong> Jl Nusakambangan No. 1 Telp. 0341-325985</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-325985</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkasin.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkasin.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kasin@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 2000.7 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20007" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Oro-oro Dowo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20007" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Chrisman Sudarmodjo,SH</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Oro-Oro Dowo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong> Jl. Kunir No. 9A Telp. (0341) 343653<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>(0341) 343653</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keloro-orodowo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keloro-orodowo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-oroorodowo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 2000.8 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20008" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Bareng
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20008" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                        <tbody>
                                                                        <tr>
                                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                        <td width="20"><strong>:</strong></td>
                                                                        <td width="600"><strong>DWI CAHYONO, ST</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Jabatan</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Lurah Bareng</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Alamat </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Jl Bareng Tennes IV/158 Telp. 0341-353122</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Faks.</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>0341-353122</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Website</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><a href="http://kelbareng.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbareng.malangkota.go.id</strong></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>E-mail </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>kel-bareng@malangkota.go.id</strong></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 2000.9 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20009" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Gading Kasri
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse20009" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>&#8211;<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Gading Kasri</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Galunggung 5 Telp. 0341-326647</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-326647</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelgadingkasri.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelgadingkasri.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-gadingkasri@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 2000.10-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse200010" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Penanggungan
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse200010" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong> Yuyun Nanik Ekowati, SSTP., M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Penanggungan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Terusan Cikampek No. 147 Telp. 0341-583520</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-583520</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpenanggungan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpenanggungan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-penanggungan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 2000.11-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse200011" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Kauman
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse200011" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                  <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Boedi Soepriyono, B.Sc</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kauman</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. KH Hasyim Asyhari 21 Telp. 0341-362019</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-362019</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkauman.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkauman.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kauman@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         
                                                       
                                                    </div> <!-- colaps 20001 -->

                                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- tab colaps 200.2 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2002" aria-expanded="false" aria-controls="collapseTwo">
                                              KECAMATAN BLIMBING
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                 <div class="col-lg-12 offset-lg-1">
                                                    <h3 class="card-title">Kecamatan Blimbing - Kota Malang</h3>
                                                    <div class="card-body">

                                                <table>
                                                    <tbody>
                                                    <tr>
                                                    <td width="200" height="20"><strong>Pimpinan</strong></td>
                                                    <td width="20"><strong>:</strong></td>
                                                    <td width="700"><strong>Drs. MUARIB, M.Si</strong></td>
                                                    </tr>
                                                    <tr>
                                                    <td height="17"><strong>Jabatan</strong></td>
                                                    <td><strong>:</strong></td>
                                                    <td><strong>Camat Blimbing</strong></td>
                                                    </tr>
                                                    <tr>
                                                    <td height="17"><strong>Alamat </strong></td>
                                                    <td><strong>:</strong></td>
                                                    <td><strong>Jl. Raden Intan Kav. 14 Telp. 0341-491330</strong></td>
                                                    </tr>
                                                    <tr>
                                                    <td height="17"><strong>Faks.</strong></td>
                                                    <td><strong>:</strong></td>
                                                    <td><strong>0341-491330</strong></td>
                                                    </tr>
                                                    <tr>
                                                    <td height="17"><strong>Website</strong></td>
                                                    <td><strong>:</strong></td>
                                                    <td><a href="http://kecblimbing.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kecblimbing.malangkota.go.id</strong></a></td>
                                                    </tr>
                                                    <tr>
                                                    <td height="17"><strong>E-mail </strong></td>
                                                    <td><strong>:</strong></td>
                                                    <td><strong>kec-blimbing@malangkota.go.id</strong></td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <br>
                                                    <br><br>
                                                    <h4>Kelurahan-kelurahan di wilayah Kecamatan Blimbing</h4>

                                                         <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                                    <!-- tab colaps 3000.1 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse30001" aria-expanded="true" aria-controls="collapseOne">
                                                          Kelurahan Blimbing
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Roni Kuncoro, S.STP<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Blimbing</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. LA Sucipto No. 153 Telp. 0341-491601</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong> 0341-491601</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelblimbing.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelblimbing.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-blimbing@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 3000.2 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingTwo">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30002" aria-expanded="false" aria-controls="collapseTwo">
                                                          Kelurahan Balearjosari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="card-body">
                                                                    <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Ardi Nufianto, SE<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Balearjosari</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Raya Balearjosari No. 9 Telp. 0341-481119</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-481119</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelbalearjosari.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbalearjosari.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-balearjosari@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 3000.3 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30003" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Arjosari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>KUNARYO, A.Ks.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Arjosari</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Teluk Pelabuhan Ratu 378 Telp. 0341-481146</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-481146</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelarjosari.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelarjosari.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-arjosari@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 3000.4 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30004" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Purwodadi
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Drs. AGUS PURWANTO, M.Kes.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Purwodadi</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. A Yani Utara 148 Telp. 0341-547575</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-547575</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpurwodadi.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpurwodadi.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-purwodadi@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 3000.5 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30005" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Polowijen
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Suseno, SH, MM<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Polowijen</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. A. Yani Utara No. 2A Telp. 0341-482216</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-482216</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpolowijen.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpolowijen.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-polowijen@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- tab colaps 30005 -->
                                                         <!-- tab colaps 3000.6-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30006" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Pandanwangi
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30006" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Drs. REDY SUSANTO</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Pandanwangi</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Jl. Simpang Teluk Grajakan No. 6A Malang Telp. 0341-473852</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-473852</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpandanwangi.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpandanwangi.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-pandanwangi@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 3000.7 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30007" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Purwantoro
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30007" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Drs. MOCH. HADI, M.AP</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Purwantoro</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Tembaga 3 Telp. 0341-492727</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-492727</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpurwantoro.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpurwantoro.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-purwantoro@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 3000.8 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30008" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Bunulrejo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30008" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Dra. YUKE SISWANTI, M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Bunulrejo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Hamid Rusdi No. 91 Telp. 0341-368905</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-368905</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelbunulrejo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbunulrejo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-bunulrejo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 3000.9 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse30009" aria-expanded="false" aria-controls="collapseThree">
                                                          Keluarahan Kesatrian
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse30009" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Faried Su&#8217;aidi, ST<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kesatrian</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. PB Sudirman 18 Telp. 0341-356944</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-356944</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkesatrian.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkesatrian.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kesatrian@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 3000.10-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse300010" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Polean
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse300010" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>BENDOT JOKO NUGROHO, S.AP</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Polehan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Puntodewo 29 Telp. 0341-352053</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-352053</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpolehan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelpolehan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-polehan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 3000.11-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse300011" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Jodipan
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse300011" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>MEIDY HAZRAN, SH<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Jodipan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Jodipan Wetan 11 Telp. 0341-353067</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-353067</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keljodipan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keljodipan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-jodipan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- tab colaps 200.3 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2003" aria-expanded="false" aria-controls="collapseThree">
                                              KECAMATAN LOWOKWARU
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body">
                                                        <div class="col-lg-12 offset-lg-1">
                                                    <h3 class="card-title">Kecamatan Lowokwaru - Kota Malang</h3>

                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                        <td width="20"><strong>:</strong></td>
                                                        <td width="600"><strong>IMAM BADAR, SE, M.Si<br />
                                                        </strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Jabatan</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Camat Lowokwaru</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Alamat </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>Jl. Cengger Ayam I/12 Telp. 0341-493162</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Faks.</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>0341-493162</strong></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>Website</strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><a href="http://keclowokwaru.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keclowokwaru.malangkota.go.id</strong></a></td>
                                                        </tr>
                                                        <tr>
                                                        <td height="17"><strong>E-mail </strong></td>
                                                        <td><strong>:</strong></td>
                                                        <td><strong>kel-lowokwaru@malangkota.go.id</strong></td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                        <br><br>
                                                        <h4>Kelurahan-kelurahan di wilayah Kecamatan Lowokwaru</h4>

                                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                                    <!-- tab colaps 4000.1 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse40001" aria-expanded="true" aria-controls="collapseOne">
                                                          Kelurahan Tasikmadu
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;" width="200"><strong>Pimpinan</strong></td>
                                                                    <td style="height: 24px;" width="20"><strong>:</strong></td>
                                                                    <td style="height: 24px;" width="600"><strong> Drs. SUNARKA, M.AP</strong></td>
                                                                    </tr>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;"><strong>Jabatan</strong></td>
                                                                    <td style="height: 24px;"><strong>:</strong></td>
                                                                    <td style="height: 24px;"><strong>Lurah Tasikmadu</strong></td>
                                                                    </tr>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;"><strong>Alamat </strong></td>
                                                                    <td style="height: 24px;"><strong>:</strong></td>
                                                                    <td style="height: 24px;"><strong>Jl Atletik No. 125 Telp. 0341-486806</strong></td>
                                                                    </tr>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;"><strong>Faks.</strong></td>
                                                                    <td style="height: 24px;"><strong>:</strong></td>
                                                                    <td style="height: 24px;"><strong>0341-486806</strong></td>
                                                                    </tr>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;"><strong>Website</strong></td>
                                                                    <td style="height: 24px;"><strong>:</strong></td>
                                                                    <td style="height: 24px;"><a href="http://keltasikmadu.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keltasikmadu.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr style="height: 24px;">
                                                                    <td style="height: 24px;"><strong>E-mail </strong></td>
                                                                    <td style="height: 24px;"><strong>:</strong></td>
                                                                    <td style="height: 24px;"><strong>kel-tasikmadu@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 4000.2 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingTwo">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40002" aria-expanded="false" aria-controls="collapseTwo">
                                                          Kelurahan Tunggulwulung 
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="card-body">
                                                                    <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>BUDIONO, SE</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Tunggulwulung</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Arumba No. 6 Malang Telp. 0341-484160</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-484160</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keltunggulwulung.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keltunggulwulung.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-tunggulwulung@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 4000.3 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40003" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Merjosari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Drs. ABDULLAH</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Merjosari</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Mertojoyo No. 1 Telp. 0341-560525</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-560525</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelmerjosari.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelmerjosari.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-merjosari@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 4000.4 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40004" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Tlogomas
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                    <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>ANDI AISYAH MUHSIN, S.STP,<br />
                                                                    M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Tlogomas</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Raya Tlogomas 56 Telp. 0341-650649</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-650649</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keltlogomas.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keltlogomas.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-tlogomas@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 4000.5 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40005" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Dinoyo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                  <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>DWI HERMAWAN PURNOMO,<br />
                                                                    S.STP</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Dinoyo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl MT Haryono XIII Telp. 0341-551818</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-551818</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keldinoyo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keldinoyo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-dinoyo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- tab colaps 40005 -->
                                                         <!-- tab colaps 4000.6-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40006" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Sumbersari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40006" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                        <tbody>
                                                                        <tr>
                                                                        <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                        <td width="20"><strong>:</strong></td>
                                                                        <td width="600"><strong>Achiyat Hadi Supriyanto, S.Sos</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Jabatan</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Lurah Sumbersari</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Alamat </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>Jl Bend Sigura-gura 31 Telp. 0341-577940</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Faks.</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>0341-577940</strong></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>Website</strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><a href="http://kelsumbersari.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelsumbersari.malangkota.go.id</strong></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td height="17"><strong>E-mail </strong></td>
                                                                        <td><strong>:</strong></td>
                                                                        <td><strong>kel-sumbersari@malangkota.go.id</strong></td>
                                                                        </tr>
                                                                        </tbody>
                                                                        </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 4000.7 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40007" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Ketawanggede
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40007" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"> <strong>Drs. ACHMAD SANDHI, SH, M.AP</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Ketawanggede</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Kertosentono 103 Telp. 0341-572514</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-572514</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelketawanggede.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelketawanggede.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-ketawanggede@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 4000.8 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40008" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Jatimulyo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40008" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>PURNOMO, SE</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Jatimulyo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Simbar Menjangan 37 Telp. 0341-472111</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-472111</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keljatimulyo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keljatimulyo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-jatimulyo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 4000.9 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse40009" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Tunjungsekar
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse40009" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="12"><strong>:</strong></td>
                                                                    <td width="397"> <strong>SUBHAN EFFENDI, ST</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Tunjungsekar</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Ikan Piranha Atas No. 206 Telp. 0341-497111</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-497111</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong><a href="http://keltunjungsekar.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://keltunjungsekar.malangkota.go.id</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-tunjungsekar@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 4000.10-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse400010" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Mojolangu
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse400010" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"> <strong>BAMBANG MUJIONO</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Mojolangu</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Sudimoro 17 Telp. 0341-474320</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-474320</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong><a href="http://kelmojolangu.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kelmojolangu.malangkota.go.id</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-mojolangu@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 4000.11-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse400011" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Tulusrejo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse400011" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                <tbody>
                                                                <tr>
                                                                <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                <td width="20"><strong>:</strong></td>
                                                                <td width="600"><strong> Nina Sudiarty, S.STP, M.Si</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Jabatan</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>Lurah Tulusrejo</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Alamat </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>Jl. Bantaran Barat II Telp. 0341-474451</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Faks.</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>0341-474451</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Website</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong><a href="http://keltulusrejo.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://keltulusrejo.malangkota.go.id</a></strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>E-mail </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>kel-tulusrejo@malangkota.go.id</strong></td>
                                                                </tr>
                                                                </tbody>
                                                                </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 4000.12-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse400012" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Lowokwaru
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse400012" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong> Drs. SYAMSUL HUDA<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Lowokwaru</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Tretes 10 Telp. 0341-494387</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-494387</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong><a href="http://kellowokwaru.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kellowokwaru.malangkota.go.id</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-lowokwaru@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                      
                                                    </div> <!-- colaps 40001 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- tab colaps 200.4 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2004" aria-expanded="false" aria-controls="collapseThree">
                                              KECAMATAN SUKUN
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="col-lg-12 offset-lg-1">
                                                    <h3 class="card-title">Kecamatan Sukun - Kota Malang</h3>

                                                    <div class="card-body">
                                                    <table>
                                                            <tbody>
                                                            <tr>
                                                            <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                            <td width="20"><strong>:</strong></td>
                                                            <td width="600"><strong>I. K. Widi E. Wirawan, S.Sos, MM</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Jabatan</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Camat Sukun</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Alamat </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>Jl. Keben I &#8211; Telp. 0341-801268</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Faks.</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>0341-362468</strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>Website</strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong><a href="http://kecsukun.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kecsukun.malangkota.go.id</a></strong></td>
                                                            </tr>
                                                            <tr>
                                                            <td height="17"><strong>E-mail </strong></td>
                                                            <td><strong>:</strong></td>
                                                            <td><strong>kec-sukun@malangkota.go.id</strong></td>
                                                            </tr>
                                                            </tbody>
                                                            </table>
                                                            <br><h3>Kelurahan-kelurahan di wilayah Kecamatan Sukun</h3>
                                                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                                    <!-- tab colaps 5000.1 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse50001" aria-expanded="true" aria-controls="collapseOne">
                                                          Kelurahan Ciptomulyo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>BURHANUDDIN AL JUNDI,<br />
                                                                    S.Sos.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Ciptomulyo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Kolonel Sugiono VIII No.1 Telp. 0341-322175</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-322175</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelciptomulyo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelciptomulyo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-ciptomulyo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 5000.2 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingTwo">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50002" aria-expanded="false" aria-controls="collapseTwo">
                                                         Kelurahan Gadang
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="card-body">
                                                                    <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Hajar Iswantoro, S.Sos<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Gadang</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Kol Sugiono No. 140 Telp. 0341-802568</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-802568</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelgadang.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelgadang.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-gadang@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 5000.3 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50003" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Bandungrejosari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>ZAINUL AMALI, S.Sos., MSi</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Bandungrejosari</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. S. Supriyadi No. 30 Telp. 0341-801852</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-801852</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelbandungrejosari.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbandungrejosari.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-bandungrejosari@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 5000.4 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50004" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Sukun
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Drs. Mas Bambang Widjajanto</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Sukun</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Rajawali F-5 Telp. 0341-324595</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-324595</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelsukun.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelsukun.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-sukun@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 5000.5 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50005" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Tanjungrejo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Lalu Efendi Hidayat, ST<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Tanjungrejo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Terusan Mergan Raya No. 1 Telp. 0341-327395</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-327395</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keltanjungrejo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keltanjungrejo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-tanjungrejo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- tab colaps 50005 -->
                                                         <!-- tab colaps 5000.6-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50006" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Pisangcandi
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50006" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>Suswanto, S.Sos, M.Si<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Pisang Candi</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Angur No.1 Telp. (0341)-566193<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>(0341)-566193<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelpisangcandi.malangkota.go.id"><strong>http://kelpisangcandi.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-pisangcandi@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 5000.7 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50007" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Bandulan
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50007" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>DIAN SONYALIA CATUR RINA,<br />
                                                                    S.STP</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Bandulan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Raya Bandulan No. 103 Telp. 0341-571127</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-571127</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelbandulan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbandulan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-bandulan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 5000.8 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50008" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Karang Besuki
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50008" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>BAMBANG HERYYANTO, S.Sos, M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Karang Besuki</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Terusan Sigura-gura Blok D No. 174 Telp. (0341) 571230 Malang<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-571230</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkarangbesuki.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkarangbesuki.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-karangbesuki@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 5000.9 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse50009" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Mulyorejo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse50009" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>R. SYAHRIAL HAMID, S.Sos.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Mulyorejo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Budi Utomo No. 1 Telp. 0341-580170</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-580170</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelmulyorejo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelmulyorejo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-mulyorejo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 5000.10-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse500010" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Bakalan Krajan
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse500010" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>SUKO KURNIAWAN, S.Sos.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Bakalan Krajan</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Bakalan Krajan Telp. 0341-802557</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-802557</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelbakalankrajan.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbakalankrajan.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-bakalankrajan@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 5000.11-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse500011" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Kebonsari
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse500011" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong> NYOTO, S.STP.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kebonsari</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. S. Supriyadi No. 15 Telp. 0341-801925</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-801925</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong><a href="http://kelkebonsari.malangkota.go.id" target="_blank" rel="noopener noreferrer">http://kelkebonsari.malangkota.go.id</a></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kebonsari@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div> <!-- colaps 50001 -->
                                                    </div>
                                                </div>
                                            </div>
                                             <!-- tab colaps 200.5 -->
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2005" aria-expanded="false" aria-controls="collapseThree">
                                              KECAMATAN KEDUNGKANDANG
                                            </a>
                                          </h5>
                                                </div>
                                                <div id="collapse2005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                 <div class="col-lg-12 offset-lg-1">
                                                 <br>
                                                    <h3 class="card-title">Kecamatan Kedungkadang - Kota Malang</h3>
<table>
<tbody>
<tr>
<td width="200" height="17"><strong>Pimpinan</strong></td>
<td width="20"><strong>:</strong></td>
<td width="700"><strong>Drs. R. ACHMAD MABRUR</strong></td>
</tr>
<tr>
<td height="17"><strong>Jabatan</strong></td>
<td><strong>:</strong></td>
<td><strong>Camat Kedungkandang<br />
</strong></td>
</tr>
<tr>
<td height="17"><strong>Alamat </strong></td>
<td><strong>:</strong></td>
<td><strong>Jl. Mayjen Sungkono 59, Telp. 0341-752273</strong></td>
</tr>
<tr>
<td height="17"><strong>Faks.</strong></td>
<td><strong>:</strong></td>
<td><strong>0341-325814</strong></td>
</tr>
<tr>
<td height="17"><strong>Website</strong></td>
<td><strong>:</strong></td>
<td><a href="http://keckedungkandang.malangkota.go.id/" target="_blank" rel="noopener noreferrer"><strong>http://keckedungkandang.malangkota.go.id</strong></a></td>
</tr>
<tr>
<td height="17"><strong>E-mail </strong></td>
<td><strong>:</strong></td>
<td><strong>kec-kedungkandang@malangkota.go.id</strong></td>
</tr>
</tbody>
</table>

<br><h3>Kelurahan-kelurahan di wilayah Kecamatan Kedungkandang</h3>
                                                    <div class="card-body">
                                                       <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">

                                                    <!-- tab colaps 6000.1 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingOne">
                                                                <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse60001" aria-expanded="true" aria-controls="collapseOne">
                                                          Kelurahan Kotalama
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60001" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="card-body">
                                                                   <table>
                                                                <tbody>
                                                                <tr>
                                                                <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                <td width="20"><strong>:</strong></td>
                                                                <td width="600"><strong>JOKO SISWO BINTORO SETIO GUNAWAN, ST</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Jabatan</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>Lurah Kotalama</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Alamat </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>Jl. Kebalen Wetan No. 5A Telp. 0341-325814</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Faks.</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>0341-325814</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Website</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><a href="http://kelkotalama.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkotalama.malangkota.go.id</strong></a></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>E-mail </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>kel-kotalama@malangkota.go.id</strong></td>
                                                                </tr>
                                                                </tbody>
                                                                </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 6000.2 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingTwo">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60002" aria-expanded="false" aria-controls="collapseTwo">
                                                          Kelurahan Mergosono
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60002" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>KARLIONO, S.Sos, M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Mergosono</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Kol Sugiono V/372 Telp. 0341-335728</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-335728</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelmergosono.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelmergosono.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-mergosono@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 6000.3 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60003" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Bumiayu
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60003" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                <tbody>
                                                                <tr>
                                                                <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                <td width="20"><strong>:</strong></td>
                                                                <td width="600"><strong>SISWANTO HERU SUPARNADI, S.Sos, MM</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Jabatan</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>Lurah Bumiayu</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Alamat </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong><strong>Jl Kyai Parseh Jaya No. 35 Telp. 0341-7500101, 751034</strong></strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Faks.</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>0341-751034</strong></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>Website</strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><a href="http://kelbumiayu.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelbumiayu.malangkota.go.id</strong></a></td>
                                                                </tr>
                                                                <tr>
                                                                <td height="17"><strong>E-mail </strong></td>
                                                                <td><strong>:</strong></td>
                                                                <td><strong>kel-bumiayu@malangkota.go.id</strong></td>
                                                                </tr>
                                                                </tbody>
                                                                </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tab colaps 6000.4 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60004" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Wonokoyo
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60004" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>SUPARMAN, SH, M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Wonokoyo</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Kalisari No. 1 Telp. 0341-7500102</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-7500102</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelwonokoyo.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelwonokoyo.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-wonokoyo@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 6000.5 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60005" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Buring
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60005" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>&#8211;</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Buring</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl. Puncak Buring Indah No. 1  Telp 0341-751591</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-751591</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelburing.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelburing.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-buring@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- tab colaps 60005 -->
                                                         <!-- tab colaps 6000.6-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60006" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Kedungkandang
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60006" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>ACHMAD IMAM MUJI, S.Sos</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Kedungkandang</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Ki Ageng Gribig 12 Telp. 0341-352920</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-352920</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelkedungkandang.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelkedungkandang.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-kedungkandang@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 6000.7 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60007" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Lesanpuro
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60007" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>ADJI PRIJONO, SE, M.Pd</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Lesanpuro</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Ki Ageng Gribig No. 3 Telp. 0341-711564</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-711564</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kellesanpuro.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kellesanpuro.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-lesanpuro@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 6000.8 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60008" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Sawojajar
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60008" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>MOCH. FAKIHUDDIN, SH, M.Si</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Sawojajar</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Sawojajar 45 Telp. 0341-715953</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-715953</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelsawojajar.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelsawojajar.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-sawojajar@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 6000.9 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse60009" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Madyopuro
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse60009" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>SUKENDARI, SH, MM</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Madyopuro</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Madyopuro Telp. 0341-711538</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-711538</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelmadyopuro.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelmadyopuro.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-madyopuro@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 6000.10-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse600010" aria-expanded="false" aria-controls="collapseThree">
                                                         Kelurahan Cemorokandang
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse600010" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>AHMAD RIDWAN, SE</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Cemorokandang</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Cemorokandang No. 1 Telp. 0341-711539</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-711539</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelcemorokandang.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelcemorokandang.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-cemorokandang@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <!-- tab colaps 6000.11-->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse600011" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Arjowinangun
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse600011" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>DWI PATRIANTO, S.PSi.</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Arjowinangun</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Arjowinangun No. 1 Telp. 0341-7500103</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-7500103</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://kelarjowinangun.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://kelarjowinangun.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-arjowinangun@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <!-- tab colaps 6000.12 -->
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse600012" aria-expanded="false" aria-controls="collapseThree">
                                                          Kelurahan Tlogowaru
                                                        </a>
                                                      </h5>
                                                            </div>
                                                            <div id="collapse600012" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body">
                                                                   <table>
                                                                    <tbody>
                                                                    <tr>
                                                                    <td width="200" height="17"><strong>Pimpinan</strong></td>
                                                                    <td width="20"><strong>:</strong></td>
                                                                    <td width="600"><strong>AGOES TRI HARTADI, S.Sos, M.Si<br />
                                                                    </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Jabatan</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Lurah Tlogowaru</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Alamat </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>Jl Raya Tlogowaru No. 119 Telp. 0341-752808</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Faks.</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>0341-752808</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>Website</strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><a href="http://keltlogowaru.malangkota.go.id" target="_blank" rel="noopener noreferrer"><strong>http://keltlogowaru.malangkota.go.id</strong></a></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td height="17"><strong>E-mail </strong></td>
                                                                    <td><strong>:</strong></td>
                                                                    <td><strong>kel-tlogowaru@malangkota.go.id</strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- colaps 60001 -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                                    <!-- MULAI TAB KELURAHAN -->
                                                    <br><br>
                                                   
                                                  </div><!-- /TUTUP TAB KEC -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
<footer class="footer"> © 2019  Dashboard Kota Malang by Kominfo  </footer>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("adminpress/assets/plugins/jquery/jquery.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/popper.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/bootstrap/js/bootstrap.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/jquery.slimscroll.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/sidebarmenu.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/waves.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/dark/js/custom.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.min.js") ?>"></script>

    <script src="<?php echo base_url("adminpress/assets/plugins/peity/jquery.peity.init.js") ?>"></script>



    <script src="<?php echo base_url("adminpress/assets/plugins/sparkline/jquery.sparkline.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/raphael/raphael-min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/morrisjs/morris.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard1.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/dark/js/dashboard3.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/skycons/skycons.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-js/dist/chartist.min.js") ?>"></script>
    <script src="<?php echo base_url("adminpress/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js") ?>"></script>
     <script src="<?php echo base_url("adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js") ?>"></script>
     

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>

     <script>
    // This is for the sticky sidebar    
    $(".stickyside").stick_in_parent({
        offset_top: 100
    });
    $('.stickyside a').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 100
        }, 500);
        return false;
    });
    // This is auto select left sidebar
    // Cache selectors
    // Cache selectors
    var lastId,
        topMenu = $(".stickyside"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    // Bind click handler to menu items


    // Bind to scroll
    $(window).scroll(function() {
        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight - 250;

        // Get id of current scroll item
        var cur = scrollItems.map(function() {
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .removeClass("active")
                .filter("[href='#" + id + "']").addClass("active");
        }
    });
    </script>

    </body>
</html>


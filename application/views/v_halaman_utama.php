<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 

<html class="no-js"> <!--<![endif]-->
    <head>
        <title>Dashboard</title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- stylesheets -->
    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("template_front/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/font_icon/css/pe-icon-7-stroke.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/font_icon/css/helper.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/css/owl.carousel.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/css/owl.theme.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/css/animate.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("template_front/assets/css/style.css"); ?>" rel="stylesheet">

    <link href="<?php echo base_url("royalmaster/css/boostrap.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("royalmaster/css/style.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("royalmaster/vendors/linericon/style.css");?>" rel="stylesheet">
    <script src="<?php echo base_url("royalmaster/js/bootstrap.min.js") ?>"></script>

    <link href="<?php echo base_url("header_cover/assets/styles/normalize.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("header_cover/assets/styles/styles.css");?>" rel="stylesheet">

        <!-- google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500|Lato:300,400,700,900,300italic,400italic,700italic,900italic|Raleway:400,200,300,500,100|Titillium+Web:400,200,300italic,300,200italic' rel='stylesheet' type='text/css'>

        <script src="<?php echo base_url("template_front/assets/js/modernizr.js") ?>"></script>

    </head>
    <body>
      <!-- Video Markup -->
      <header>
    <section class="masthead">
        <video class="masthead-video" autoplay loop muted poster="assets/images/poster.jpg">
              <source src="<?php echo base_url("header_cover/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
            <source src="<?php echo base_url("header_cover/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
        </video>
        <div class="masthead-overlay"></div>
        <div class="bottom">
                <a data-scroll href="#navigation" class="scrollDown animated pulse" id="scrollToContent"><i class="pe-7s-angle-down-circle pe-va"></i></a>
            </div>
        <h1>DASHBOARD<span>-KOTA MALANG-</span></h1>
        <a class="masthead-video-credit" href="#" target="_blank">
            <span>Video: Profil Pemkot Malang</span>
            <span>by Diskominfo</span>
        </a>
    </section></header>
       
        <!-- Navigation area -->
        <section id="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="logo"><a data-scroll href="#body" class="logo-text">Dashboard </a></div>
                    </div>
                   

                </div>
            </div>            
        </section>
        
        <script>
function blinker() {
    $('.blinking').fadeOut(500);
    $('.blinking').fadeIn(500);
}
setInterval(blinker, 1000);
</script>
        <!-- Content Area -->

 
 <!-- Main Menu Dashboard-->

       <section id="testimonial" class="testimonial-area">
            <div class="container">
                <center><h2 class="blinking" style="font-size:50px;">Main Menu - Dashboard</h2></center><br>

              <!--   Menu disini -->
                <div class="row port cs-style-3">

                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
 			 <a target="_blank" href="https://suradi.malangkota.go.id/admin/login">
                            <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/2-administrasi.png"); ?>">
                            <div class="btn button_hover book_now_btn">Administrasi</div>
                        </div>
                     </div>
                    </div>


                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                         <a target="_blank" href="https://jdih.malangkota.go.id/hukum">
                            <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/3-legislasi.png"); ?>">
                            <div class="btn button_hover book_now_btn">Legislasi</div>
                        </div>
                     </div>
                    </div>


                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
  				<a target="_blank" href="http://ereport.malangkota.go.id/">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/4-pembangunan.png"); ?>">
                            <div class="btn button_hover book_now_btn">Pembangunan</div>
                        </div>
                     </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                          <a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/5-keuangan.png"); ?>">
                            <div class="btn button_hover book_now_btn">Keuangan</div>
                        </div>
                     </div>
                    </div>

                   
                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a target="_blank" href="<?php print_r(base_url());?>bkd/Bpkadapi/">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/6-kepegawaian.png"); ?>">
                            <div class="btn button_hover book_now_btn">Kepegawaian</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
			             <a target="_blank" href="<?php print_r(base_url());?>halaman_dinas_lembaga/dinasdanlembaga">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/7-dinas-lembaga.png"); ?>">
                            <div class="btn button_hover book_now_btn">Dinas dan Lembaga</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                             <a target="_blank" href="<?php print_r(base_url());?>welcome/kewilayahan">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/8-kewilayahan.png"); ?>">
                            <div class="btn button_hover book_now_btn">Kewilayahan</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                         <a target="_blank" href="https://ncctrial.malangkota.go.id/malang/spm/">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/9-kemasyarakatan.png"); ?>">
                            <div class="btn button_hover book_now_btn">Kemasyarakatan</div>
                        </div>
                     </div>
                    </div>


                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                         <a target="_blank" href="<?php print_r(base_url());?>Halaman_sarpras/sarpras">
                         
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/10-sarana-prasaran.png"); ?>">
                            <div class="btn button_hover book_now_btn">Sarana Prasarana</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a target="_blank" href="https://mediacenter.malangkota.go.id/">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/11-media-center.png"); ?>">
                            <div class="btn button_hover book_now_btn">Media Center</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a target="_blank" href="<?php print_r(base_url());?>halaman_pendidikan/get_data_local">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/12-pendidikan.png"); ?>">
                            <div class="btn button_hover book_now_btn">Pendidikan</div>

                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a target="_blank" href="<?php print_r(base_url());?>bp2d/Bppdapi/index_data_realisasi">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/13-bp2d.png"); ?>">
                            <div class="btn button_hover book_now_btn">PAJAK</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a target="_blank" href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/14-pdam.png"); ?>">
                            <div class="btn button_hover book_now_btn">PDAM</div>
                        </div>
                     </div>
                    </div>

                     <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                       <a target="_blank" href="<?php print_r(base_url());?>show_report/Showdispendukdetail/get_data/2019">
                        <div class="hotel_img">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/15-dispendukcapil.png"); ?>">
                            <div class="btn button_hover book_now_btn">Dispendukcapil</div>
                        </div>
                     </div>
                    </div>


                    <div class="col-md-4 col-sm-6 col-xs-12 item-space">
                     <div class="accomodation_item text-center">
                        <div class="hotel_img">
                        <a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">
                             <img class="img-responsive" src="<?php echo base_url("template_front/assets/images/5-keuangan.png"); ?>">
                            <div class="btn button_hover book_now_btn">SIPEX</div>
                        </div>
                     </div>
                    </div>
                </div>
            </div><!-- container -->
        </section><!-- /Menu -->

     
        <!-- Contact Area -->

        <section id="contact" class="mapWrap">
            <div id="googleMap" style="width:100%;"></div>
            <div id="contact-area">
                <div class="container">
                    <h2 class="block_title">Kontak Kami</h2>
                    <div class="row">
                        <div class="col-xs-12">
                        </div>
                        <div class="col-sm-6">
                            <div class="moreDetails">
                                <h2 class="con-title">Dinas Komunikasi dan Informatika Kota Malang</h2>
                                <p> Dinas Komunikasi dan Informatika melaksanakan tugas pokok penyusunan dan pelaksanaan kebijakan urusan pemerintahan daerah di bidang komunikasi dan informatika.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <ul class="address">
                                    <li><i class="pe-7s-map-marker"></i><span>Perkantoran Terpadu Pemerintah Kota Malang, Gedung A Lantai 4, Jl. Mayjen Sungkono, Arjowinangun, Kec. Kedungkandang, Kota Malang, Jawa Timur 65132<br>Indonesia</span></li>
                                    <li><i class="pe-7s-mail"></i><span>kominfokotamalang@gmail.com</span></li>
                                    <li><i class="pe-7s-phone"></i><span>(0341) 751550</span></li>
                                    <li><i class="pe-7s-global"></i><span>www.kominfo.malangkota.go.id</span></li>
                                </ul>
                        </div>
                    </div>
                </div><!-- container -->
            </div><!-- contact-area -->
            <div id="social">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="scoialinks">
                                <li class="normal-txt">Find me on</li>
                                <li class="social-icons"><a class="facebook" href="https://www.facebook.com/kominfo"></a></li>
                                <li class="social-icons"><a class="twitter" href="https://twitter.com/kominfomalang?lang=id"></a></li>
                                <li class="social-icons"><a class="google-plus" href="#"></a></li>
                                <li class="social-icons"><a class="wordpress" href="https://malangkota.go.id/beranda/"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- social -->
        </section><!-- contact -->

        <!-- Footer Area -->

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="copyright">© Copyright 2019 <a href="https://malangkota.go.id/beranda/" target="_blank">Diskominfo Kota Malang</a></p>
                    </div>
                    <div class="col-sm-6">
                        <p class="designed">Theme by <a href="#" target="_blank">Kominfo</a></p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Necessery scripts -->
    <script src="<?php echo base_url("template_front/assets/js/jquery-2.1.3.min.js") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/https://maps.googleapis.com/maps/api/js?v=3.exp") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/jquery.actual.min.js") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/smooth-scroll.js") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/owl.carousel.js") ?>"></script>
    <script src="<?php echo base_url("template_front/assets/js/script.js") ?>"></script>

    <script src="<?php echo base_url("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js") ?>"></script>

     <script src="<?php echo base_url("header_cover/covervid.js") ?>"></script>
   
    <script src="<?php echo base_url("header_cover/assets/scripts/scripts.js") ?>"></script>
   
   

    <!-- Call CoverVid -->
    <script type="text/javascript">
        // If using jQuery
            // $('.masthead-video').coverVid(1920, 1080);
        // If not using jQuery (Native Javascript)
            coverVid(document.querySelector('.masthead-video'), 640, 360);
    </script>

    </body>
</html>


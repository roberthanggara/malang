<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Showdispendukdetail extends CI_Controller {
	public $ssl_config = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );


	public function __construct(){
		parent::__construct();
		$this->load->model('main/mainmodel', 'mm');
		$this->load->model('dispenduk/main_dispenduk', 'md');

		$this->load->library("response_message");
	}

	public function index($th_first = 2015, $th_finish = 2019){
		$data["page"] = "data dispenduk";
		$data["data_periode_th"] = $this->md->get_all_th_periode();
		$data["month"] = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		$data["data_main"] = json_encode($this->get_all_periode($th_first, $th_finish));

		print_r(json_encode($this->get_all_periode($th_first, $th_finish)));

		// $this->load->view("show_report/dispenduk/home_new", $data);
	}

	public function get_data($th_first = 2015){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		$no = 0;
		// $array_data_new =

		$data["label"] = array();
		
		$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$th_first));

		$data_list = array();
		foreach ($data_all as $key => $value) {

			$json_file = $value->loc_file_json;
			$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file, false, stream_context_create($this->ssl_config)));
			$extract_data_json_lampid = $extract_data_json_main->lampid;

			$month_ok = $month[(int)substr($value->periode, 4, 2)];
			$data_list[$value->periode] = $month_ok;

			if($extract_data_json_lampid->status){
			#------wni------
				foreach ($extract_data_json_lampid->item->main_awal as $keya => $valuea) {
					// print_r($valuea);
					$array_data_new["lampid"]["wni"][$value->periode]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->p
																				)
																			);

					$array_data_new["lampid"]["wni"][$value->periode]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->LAHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->LAHIR_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wni"][$value->periode]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->MATI_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->MATI_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wni"][$value->periode]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDATANG_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDATANG_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wni"][$value->periode]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PINDAH_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PINDAH_BULAN_INI->p
																				)
																			);



					$array_data_new["lampid"]["wni"][$value->periode]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->p
																				)
																			);
				}

			#------wna------
				foreach ($extract_data_json_lampid->item->main_kurang as $keya => $valuea) {
					// print_r($valuea);
					$array_data_new["lampid"]["wna"][$value->periode]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->p
																				)
																			);

					$array_data_new["lampid"]["wna"][$value->periode]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->LAHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->LAHIR_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wna"][$value->periode]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->MATI_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->MATI_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wna"][$value->periode]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDATANG_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDATANG_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["wna"][$value->periode]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PINDAH_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PINDAH_BULAN_INI->p
																				)
																			);



					$array_data_new["lampid"]["wna"][$value->periode]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->p
																				)
																			);
				}

			#------all------
				foreach ($extract_data_json_lampid->item->main_akhir as $keya => $valuea) {
					// print_r($valuea);
					$array_data_new["lampid"]["all"][$value->periode]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AWAL_BULAN_INI->p
																				)
																			);

					$array_data_new["lampid"]["all"][$value->periode]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->LAHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->LAHIR_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["all"][$value->periode]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->MATI_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->MATI_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["all"][$value->periode]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDATANG_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDATANG_BULAN_INI->p
																				)
																			);


					$array_data_new["lampid"]["all"][$value->periode]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PINDAH_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PINDAH_BULAN_INI->p
																				)
																			);



					$array_data_new["lampid"]["all"][$value->periode]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))] = array(
																				array(
																					"keterangan"=>"Laki-laki",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->l
																				),
																				array(
																					"keterangan"=>"Perempuan",
																					"value"=>$valuea->PENDUDUK_AKHIR_BULAN_INI->p
																				)
																			);
				}
				
			}

			$no++;
		}
				

		$data["data_json"] = json_encode($array_data_new);
		// $data["periode_th"] = $th_first .. $th;
		// $data["data_json_select"] = json_encode($array_kategori_data);
		$data["label"] = json_encode($data["label"]);

		// print_r(json_encode($array_data_new));
		// print_r($data["label"]);
		// print_r($array_data_new);

		$this->load->view("show_report/dispenduk/chart_detail_lampid_new", $data);
	}

	public function get_data_rekap($th_first = 2015, $th_finish = 2019){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		// $th = $this->input->post("th_periode");
		
		$no = 0;
		// $array_data_new =

		$array_data_new = array(
								"rekap_kelurahan"=>array(
									"rekap_kelurahan"=>array(
										
									),
									"t_rekap_kelurahan"=>array(
										
									)
								)
							);

		
		$data["label"] = array();
		

		// if($th_first <= $th_finish){
			// for ($i = $th_first; $i <= $th_finish; $i++) {
				$value_th = $th_first;
				$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$value_th));
				foreach ($data_all as $key => $value) {
					// $json_file = $value_main->loc_file_json;
					$periode = $value->periode;

					array_push($data["label"], $month[(int)substr($periode, 4, 2)]." ".substr($periode, 0, 4));

					$json_file = $value->loc_file_json;
					$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file, false, stream_context_create($this->ssl_config)));
				

				#------rekap_kelurahan------
					$extract_data_json_rekap_kelurahan = $extract_data_json_main->rekap_kelurahan;

					if($extract_data_json_rekap_kelurahan->status){
					#------rekap_kelurahan------
						foreach ($extract_data_json_rekap_kelurahan->item->rekap_kelurahan as $keya => $valuea) {
							// print_r("<pre>");
							// print_r($valuea);

							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["JUMLAH_KEL"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KEL"][$no] = $valuea->JUMLAH_KEL;

							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["JUMLAH_KK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KK"][$no] = $valuea->JUMLAH_KK;

							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"][$value->periode] = array(
							// 	""=>
							// );

							if(!array_key_exists($value->periode, $array_data_new["rekap_kelurahan"]["rekap_kelurahan"])){
								$array_data_new["rekap_kelurahan"]["rekap_kelurahan"][$value->periode] = array();
							}

							$tmp_data = array(
								"keterangan"=>$valuea->KECAMATAN,
								"value"=>$valuea->JUMLAH_KK
							);

							array_push($array_data_new["rekap_kelurahan"]["rekap_kelurahan"][$value->periode], $tmp_data);


							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
							// $array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;


							
						}

					#------t_rekap_kelurahan------
						// foreach ($extract_data_json_rekap_kelurahan->item->t_rekap_kelurahan as $keya => $valuea) {
						// 	// print_r("<pre>");
						// 	// print_r($valuea);
						// 	$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["JUMLAH_KEL"][$no] = $valuea->JUMLAH_KEL;
						// 	$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["JUMLAH_KK"][$no] = $valuea->JUMLAH_KK;


						// 	$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
						// 	$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
						// 	$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;
						// }
					}
				#------rekap_kelurahan------

					$no++;
				}
			// }
		// }
				

		$data["data_json"] = json_encode($array_data_new);
		// $data["periode_th"] = $th_first .. $th;
		// $data["data_json_select"] = json_encode($array_kategori_data);
		$data["label"] = json_encode($data["label"]);

		// print_r(json_encode($array_data_new));
		// print_r($data["label"]);

		$this->load->view("show_report/dispenduk/chart_detail_rekap_kk", $data);
	}

	public function get_data_agama($th_first = 2015, $th_finish = 2019){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		// $th = $this->input->post("th_periode");
		
		$no = 0;
		// $array_data_new =

		$array_data_new = array("agama"=>array(
									"agama"=>array(
										// "ISLAM"=>array(),
										// "KRISTEN"=>array(),
										// "KATHOLIK"=>array(),
										// "HINDU"=>array(),
										// "BUDHA"=>array(),
										// "KONGHUCHU"=>array(),
										// "PENGHAYAT_KEPERCAYAAN"=>array()
									),
									"t_agama"=>array(
										"ISLAM"=>array(),
										"KRISTEN"=>array(),
										"KATHOLIK"=>array(),
										"HINDU"=>array(),
										"BUDHA"=>array(),
										"KONGHUCHU"=>array(),
										"PENGHAYAT_KEPERCAYAAN"=>array()
									)
								)
							);

		$array_kategori_data = array(
								"agama"=>array(
									"keterangan"=>"Agama",
									"item"=>array(
										"agama"=>array(
											"keterangan"=>"Rekapitulasi Agama Penduduk Berdasarkan Kecamatan",
											"item"=>array(
											)
										)
									)
								)
							);
		// print_r("<pre>");
		// print_r($array_data_new);
		$data["label"] = array();
		

		// if($th_first <= $th_finish){
			// for ($i = $th_first; $i <= $th_finish; $i++) {
				$value_th = $th_first;
				$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$value_th));
				foreach ($data_all as $key => $value) {
					// $json_file = $value_main->loc_file_json;
					$periode = $value->periode;

					array_push($data["label"], $month[(int)substr($periode, 4, 2)]." ".substr($periode, 0, 4));

					$json_file = $value->loc_file_json;
					$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file, false, stream_context_create($this->ssl_config)));
				

				#------agama------
					$extract_data_json_agama = $extract_data_json_main->agama;

					// print_r($extract_data_json_agama);

					if($extract_data_json_agama->item->agama){
					#------agama------
						foreach ($extract_data_json_agama->item->agama as $keya => $valuea) {
							// print_r("<pre>");
							// print_r($valuea);

							// $array_data_new["agama"]["agama"]["ISLAM"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["ISLAM"][$no] = $valuea->ISLAM;
							// $array_data_new["agama"]["agama"]["KRISTEN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KRISTEN"][$no] = $valuea->KRISTEN;
							// $array_data_new["agama"]["agama"]["KATHOLIK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KATHOLIK"][$no] = $valuea->KATHOLIK;
							// $array_data_new["agama"]["agama"]["HINDU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["HINDU"][$no] = $valuea->HINDU;
							// $array_data_new["agama"]["agama"]["BUDHA"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["BUDHA"][$no] = $valuea->BUDHA;
							// $array_data_new["agama"]["agama"]["KONGHUCHU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KONGHUCHU"][$no] = $valuea->KONGHUCHU;
							// $array_data_new["agama"]["agama"]["PENGHAYAT_KEPERCAYAAN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["PENGHAYAT_KEPERCAYAAN"][$no] = $valuea->PENGHAYAT_KEPERCAYAAN;

							$array_data_new["agama"]["agama"][$value->periode][strtolower(str_replace(" ","_",$valuea->KECAMATAN))] = array(
								array(
									"keterangan"=> "Islam",
									"value"=> $valuea->ISLAM
								),array(
									"keterangan"=> "Kristen",
									"value"=> $valuea->KRISTEN
								),array(
									"keterangan"=> "Katolik",
									"value"=> $valuea->KATHOLIK
								),array(
									"keterangan"=> "Hindu",
									"value"=> $valuea->HINDU
								),array(
									"keterangan"=> "Budha",
									"value"=> $valuea->BUDHA
								),array(
									"keterangan"=> "Konghucu",
									"value"=> $valuea->KONGHUCHU
								),array(
									"keterangan"=> "Penghayat Kepercayaan",
									"value"=> $valuea->PENGHAYAT_KEPERCAYAAN
								)

								// "keterangan"=> $valuea->KECAMATAN,
								// "ISLAM"=> $valuea->ISLAM,
								// "KRISTEN"=> $valuea->KRISTEN,
								// "KATHOLIK"=> $valuea->KATHOLIK,
								// "HINDU"=> $valuea->HINDU,
								// "BUDHA"=> $valuea->BUDHA,
								// "KONGHUCHU"=> $valuea->KONGHUCHU,
								// "PENGHAYAT_KEPERCAYAAN"=> $valuea->PENGHAYAT_KEPERCAYAAN,
							);


							// $array_kategori_data["agama"]["item"]["agama"]["item"]["ISLAM"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["ISLAM"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["KRISTEN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KRISTEN"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["KATHOLIK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KATHOLIK"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["HINDU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["HINDU"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["BUDHA"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["BUDHA"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["KONGHUCHU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KONGHUCHU"] = 0;
							// $array_kategori_data["agama"]["item"]["agama"]["item"]["PENGHAYAT_KEPERCAYAAN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["PENGHAYAT_KEPERCAYAAN"] = 0;
						}

					#------t_agama------
						// foreach ($extract_data_json_agama->item->t_agama as $keya => $valuea) {
						// 	// print_r("<pre>");
						// 	// print_r($valuea);
						// 	$array_data_new["agama"]["t_agama"]["ISLAM"][$no] 		= $valuea->ISLAM;
						// 	$array_data_new["agama"]["t_agama"]["KRISTEN"][$no] 	= $valuea->KRISTEN;
						// 	$array_data_new["agama"]["t_agama"]["KATHOLIK"][$no] 	= $valuea->KATHOLIK;
						// 	$array_data_new["agama"]["t_agama"]["HINDU"][$no] 		= $valuea->HINDU;
						// 	$array_data_new["agama"]["t_agama"]["BUDHA"][$no] 		= $valuea->BUDHA;
						// 	$array_data_new["agama"]["t_agama"]["KONGHUCHU"][$no] 	= $valuea->KONGHUCHU;
						// 	$array_data_new["agama"]["t_agama"]["PENGHAYAT_KEPERCAYAAN"][$no] = $valuea->PENGHAYAT_KEPERCAYAAN;
						// }
					}
				#------agama------

				

					$no++;
				}
			// }
		// }
				

		$data["data_json"] = json_encode($array_data_new);
		// $data["periode_th"] = $th_first .. $th;
		$data["data_json_select"] = json_encode($array_kategori_data);
		$data["label"] = json_encode($data["label"]);

		// print_r(json_encode($array_data_new));
		// print_r($data["label"]);

		$this->load->view("show_report/dispenduk/chart_detail_agama", $data);
	}

	public function get_data_ktp_kk($th_first = 2015, $th_finish = 2019){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		// $th = $this->input->post("th_periode");
		
		$no = 0;
		// $array_data_new =

		$array_data_new = array(
								
							);

		$array_kategori_data = array(
								"rekam_ktp"=>array(
									"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KEPEMILIKAN KTP DAN AKTA KELAHIRAN KOTA MALANG",
									"item"=>array(
										"rekam_ktp"=>array(
											"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KEPEMILIKAN KTP DAN AKTA KELAHIRAN KOTA MALANG",
											"item"=>array(
											)
										)
									)
									
								)
							);
		// print_r("<pre>");
		// print_r($array_data_new);
		$data["label"] = array();
		

		if($th_first <= $th_finish){
			for ($i = $th_first; $i <= $th_finish; $i++) {
				$value_th = $i;
				$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$value_th));
				foreach ($data_all as $key => $value) {
					// $json_file = $value_main->loc_file_json;
					$periode = $value->periode;

					array_push($data["label"], $month[(int)substr($periode, 4, 2)]." ".substr($periode, 0, 4));

					$json_file = $value->loc_file_json;
					$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file, false, stream_context_create($this->ssl_config)));
				

				#------rekam_ktp------
					$extract_data_json_rekam_ktp = $extract_data_json_main->rekam_ktp;

					if($extract_data_json_rekam_ktp->item->rekam_ktp){
					#------rekam_ktp------
						foreach ($extract_data_json_rekam_ktp->item->rekam_ktp as $keya => $valuea) {
							// print_r("<pre>");
							// print_r($valuea);

							$array_data_new["rekam_ktp"]["rekam_ktp"]["jumlah_penduduk"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["jumlah_penduduk"][$no] = $valuea->jumlah_penduduk;

							$array_data_new["rekam_ktp"]["rekam_ktp"]["sudah_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["sudah_rekam_KTP_el"][$no] = $valuea->sudah_rekam_KTP_el;

							$array_data_new["rekam_ktp"]["rekam_ktp"]["belum_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["belum_rekam_KTP_el"][$no] = $valuea->belum_rekam_KTP_el;
							
							$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["wajib_memiliki_akta_kelahiran"][$no] = $valuea->wajib_memiliki_akta_kelahiran;

							$array_data_new["rekam_ktp"]["rekam_ktp"]["memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["memiliki_akta_kelahiran"][$no] = $valuea->memiliki_akta_kelahiran;


							$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"][$no] = $valuea->wajib_memiliki_KTP->l;
							$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"][$no] = $valuea->wajib_memiliki_KTP->p;
							$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"][$no] = $valuea->wajib_memiliki_KTP->lp;

						}
					#------rekam_ktp------

					#------t_rekam_ktp------
						foreach ($extract_data_json_rekam_ktp->item->t_rekam_ktp as $keya => $valuea) {
							// print_r("<pre>");
							// print_r($valuea);

							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["jumlah_penduduk"]["jumlah_penduduk"][$no] = $valuea->jumlah_penduduk;

							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["sudah_rekam_KTP_el"]["sudah_rekam_KTP_el"][$no] = $valuea->sudah_rekam_KTP_el;

							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["belum_rekam_KTP_el"]["belum_rekam_KTP_el"][$no] = $valuea->belum_rekam_KTP_el;
							
							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_akta_kelahiran"]["wajib_memiliki_akta_kelahiran"][$no] = $valuea->wajib_memiliki_akta_kelahiran;

							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["memiliki_akta_kelahiran"]["memiliki_akta_kelahiran"][$no] = $valuea->memiliki_akta_kelahiran;


							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["l"][$no] = $valuea->wajib_memiliki_KTP->l;
							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["p"][$no] = $valuea->wajib_memiliki_KTP->p;
							$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["lp"][$no] = $valuea->wajib_memiliki_KTP->lp;
						}
					#------t_rekam_ktp------
					}
				#------rekam_ktp------

				

					$no++;
				}
			}
		}
				

		$data["data_json"] = json_encode($array_data_new);
		// $data["periode_th"] = $th_first .. $th;
		// $data["data_json_select"] = json_encode($array_kategori_data);
		// $data["label"] = json_encode($data["label"]);

		print_r(json_encode($array_data_new));
		// print_r($data["label"]);

		// $this->load->view("show_report/dispenduk/chart_js_basic_rekam_ktp", $data);
	}

	public function get_data_umur($th_first = 2015, $th_finish = 2019){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		// $th = $this->input->post("th_periode");
		
		$no = 0;
		// $array_data_new =

		$array_data_new = array(
								
							);

		$array_kategori_data = array(
								"kelompok_umur"=>array(
									"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KELOMPOK UMUR KOTA MALANG",
									"item"=>array(
										"kelompok_umur"=>array(
											"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KELOMPOK UMUR KOTA MALANG",
											"item"=>array(
											)
										)
									)
									
								)
							);


		$array_total = array();
		// print_r("<pre>");
		// print_r($array_data_new);
		$data["label"] = array();
		
		$kecamatan_array = array();
		// if($th_first <= $th_finish){
			// for ($i = $th_first; $i <= $th_finish; $i++) {
				$value_th = $th_first;
				$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$value_th));
				foreach ($data_all as $key => $value) {
					// $json_file = $value_main->loc_file_json;
					$periode = $value->periode;

					array_push($data["label"], $month[(int)substr($periode, 4, 2)]." ".substr($periode, 0, 4));

					$json_file = $value->loc_file_json;
					$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file, false, stream_context_create($this->ssl_config)));
				

					// print_r("<pre>");
					// print_r($extract_data_json_main);
				#------kelompok_umur------
					$extract_data_json_kelompok_umur = $extract_data_json_main->kelompok_umur;

					if($extract_data_json_kelompok_umur->item->value->kelompok_umur){
					#------kelompok_umur------
						foreach ($extract_data_json_kelompok_umur->item->value->kelompok_umur as $keya => $valuea) {
							// print_r("<pre>");
							// print_r($extract_data_json_kelompok_umur->item->header);
							// print_r($keya);

							$no_param = 0;

							$tmp_array_full = array();

							$str_kec = "";
							foreach ($extract_data_json_kelompok_umur->item->header as $key_head => $value_head) {
								if($key_head > 0 && $key_head < 15){
									$tmp_array = array(
										"keterangan"=>$value_head,
										"value"=>$valuea[$key_head]
									);
									array_push($tmp_array_full, $tmp_array);
									// $array_data_new["kelompok_umur"]["kelompok_umur"][str_replace(" ","_",$value_head)][strtolower(str_replace(" ","_",$valuea[0]))][str_replace(" ","_",$value_head)][$no] = $valuea[$key_head];

									// $array_kategori_data["kelompok_umur"]["item"]["kelompok_umur"]["item"][str_replace(" ","_",$value_head)][strtolower(str_replace(" ","_",$valuea[0]))][str_replace(" ","_",$value_head)] = $value_head;

									$no_param++;
								}else if($key_head == 0){
									// print_r(strtolower(str_replace(" ","_",$valuea[$key_head])));
									$kecamatan_array[strtolower(str_replace(" ","_",$valuea[$key_head]))] = "Kec. ".$valuea[$key_head];

									$str_kec = strtolower(str_replace(" ","_",$valuea[$key_head]));
								}
							}

							$array_data_new["kelompok_umur"]["kelompok_umur"][$value->periode][strtolower(str_replace(" ","_",$str_kec))] = $tmp_array_full;
							// print_r();
						}
					}
				#------kelompok_umur------

					$no++;
				}
			// }
		// }
				

		$data["data_json"] = json_encode($array_data_new);
		// $data["periode_th"] = $th_first .. $th;
		$data["data_json_select"] = json_encode($array_kategori_data);
		$data["label"] = json_encode($data["label"]);

		$data["data_option"] = json_encode($kecamatan_array);
		// print_r($kecamatan_array);

		// print_r(json_encode($array_data_new));
		// print_r($array_total);
		// print_r($array_data_new);
		// print_r($data["label"]);

		$this->load->view("show_report/dispenduk/chart_detail_umur", $data);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Showdispenduk extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('main/mainmodel', 'mm');
		$this->load->model('dispenduk/main_dispenduk', 'md');

		$this->load->library("response_message");
	}

	public function index(){
		$data["page"] = "data dispenduk";
		$data["data_periode_th"] = $this->md->get_all_th_periode();
		$data["month"] = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		$data["data_main"] = json_encode($this->get_all_periode());

		$this->load->view("show_report/dispenduk/home", $data);
	}

	public function get_all_periode(){
		$data_th = $this->md->get_all_th_periode();
		$month = ["","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des"];

		$data_new = array();
		$data_new["label"] = array();
		$no = 0;
		foreach ($data_th as $key_th => $value_th) {
			$data_main = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$value_th->th_periode));

			foreach ($data_main as $key_main => $value_main) {
				$json_file = $value_main->loc_file_json;
				$periode = $value_main->periode;

				array_push($data_new["label"], $month[(int)substr($periode, 4, 2)]." ".substr($periode, 0, 4));

				$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file));
				$data_lampid = $extract_data_json_main->lampid->item->t_main_akhir;

				foreach ($data_lampid as $key_lampid => $value_lampid) {
					$ex = 0;
					foreach ($value_lampid as $key => $value) {
						if($ex > 0){
							$data_new["item"][$key]["l"][$no] = $value->l;
							$data_new["item"][$key]["p"][$no] = $value->p;
							$data_new["item"][$key]["lp"][$no] = $value->lp;

							$data_new["head_sub"]["l"] = "Laki-laki";
							$data_new["head_sub"]["p"] = "Perempuan";
							$data_new["head_sub"]["lp"] = "Laki-laki dan Perempuan";

							$data_new["head"][$key] = str_replace("_", " ", $key);
						}
						$ex++;
					}
				}
				$no++;
			}
		}

		$data_new["type_chart"]["l"] = "line";
		$data_new["type_chart"]["p"] = "line";
		$data_new["type_chart"]["lp"] = "bar";
		return $data_new;
	}

	public function get_all(){
		$data_periode_th = $this->md->get_all_th_periode();
		if($data_periode_th){
			foreach ($data_periode_th as $keyth => $valueth) {
				// print_r($valueth);
				$data_main = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$valueth->th_periode));
				print_r($data_main);
			}
		}
	}

	public function get_data(){
		$month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		$th = $this->input->post("th_periode");
		$data_all = $this->md->get_data_periode_by_th(array("left(periode, 4) ="=>$th));

		$no = 0;
		// $array_data_new =

		$array_data_new = array(
								"lampid"=>array(
									"wni"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												),
									"t_wni"=> array("PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												),
									"wna"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												),
									"t_wna"=>array("PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												),
									"all"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												),
									"t_all"=>array("PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
												)
								),

								"rekap_kelurahan"=>array(
									"rekap_kelurahan"=>array(
										"JUMLAH_KEL"=>array(),
										"JUMLAH_KK"=>array(),
										"PENDUDUK_AKHIR_BULAN_INI"=>array()
									),
									"t_rekap_kelurahan"=>array(
										"JUMLAH_KEL"=>array(),
										"JUMLAH_KK"=>array(),
										"PENDUDUK_AKHIR_BULAN_INI"=>array()
									)
								),

								"agama"=>array(
									"agama"=>array(
										"ISLAM"=>array(),
										"KRISTEN"=>array(),
										"KATHOLIK"=>array(),
										"HINDU"=>array(),
										"BUDHA"=>array(),
										"KONGHUCHU"=>array(),
										"PENGHAYAT_KEPERCAYAAN"=>array()
									),
									"t_agama"=>array(
										"ISLAM"=>array(),
										"KRISTEN"=>array(),
										"KATHOLIK"=>array(),
										"HINDU"=>array(),
										"BUDHA"=>array(),
										"KONGHUCHU"=>array(),
										"PENGHAYAT_KEPERCAYAAN"=>array()
									)
								)
							);

		$array_kategori_data = array(
								"lampid"=>array(
									"keterangan"=>"Lahir, Mati, Pindah dan Datang",
									"item"=>array(
										"wni"=>array(
											"keterangan"=>"Warga Negara Indonesia Berdasarkan Kecamatan",
											"item"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
											)
										),
										"wna"=>array(
											"keterangan"=>"Warga Negara Asing Berdasarkan Kecamatan",
											"item"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
											)
										),
										"all"=>array(
											"keterangan"=>"Total Seluruh Warga Berdasarkan Kecamatan",
											"item"=>array(
												"PENDUDUK_AWAL_BULAN_INI"=>array(),
												"LAHIR_BULAN_INI"=>array(),
												"MATI_BULAN_INI"=>array(),
												"PENDATANG_BULAN_INI"=>array(),
												"PINDAH_BULAN_INI"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
											)
										)
									)
								),

								"rekap_kelurahan"=>array(
									"keterangan"=>"Rekapitulasi Kelurahan",
									"item"=>array(
										"rekap_kelurahan"=>array(
											"keterangan"=>"Rekapitulasi Kelurahan Berdasarkan Kecamatan",
											"item"=>array(
												"JUMLAH_KEL"=>array(),
												"JUMLAH_KK"=>array(),
												"PENDUDUK_AKHIR_BULAN_INI"=>array()
											)
										)
									)
									
								),

								"agama"=>array(
									"keterangan"=>"Agama",
									"item"=>array(
										"agama"=>array(
											"keterangan"=>"Rekapitulasi Agama Penduduk Berdasarkan Kecamatan",
											"item"=>array(
											)
										)
									)
									
								),

								"rekam_ktp"=>array(
									"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KEPEMILIKAN KTP DAN AKTA KELAHIRAN KOTA MALANG",
									"item"=>array(
										"rekam_ktp"=>array(
											"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KEPEMILIKAN KTP DAN AKTA KELAHIRAN KOTA MALANG",
											"item"=>array(
											)
										)
									)
									
								),

								"kelompok_umur"=>array(
									"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KELOMPOK UMUR KOTA MALANG",
									"item"=>array(
										"kelompok_umur"=>array(
											"keterangan"=>"REKAPITULASI JUMLAH PENDUDUK MENURUT KELOMPOK UMUR KOTA MALANG",
											"item"=>array(
											)
										)
									)
									
								)
							);
		// print_r("<pre>");
		// print_r($array_data_new);

		
		foreach ($data_all as $key => $value) {
			$json_file = $value->loc_file_json;
			$extract_data_json_main = json_decode(file_get_contents(base_url()."/assets/json_upload/". $json_file));
		#------lampid------
			$extract_data_json_lampid = $extract_data_json_main->lampid;

			if($extract_data_json_lampid->status){
			#------t_wni------
				foreach ($extract_data_json_lampid->item->t_main_awal as $keya => $valuea) {
					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AWAL_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AWAL_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AWAL_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wni"]["LAHIR_BULAN_INI"]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["LAHIR_BULAN_INI"]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["LAHIR_BULAN_INI"]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wni"]["MATI_BULAN_INI"]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["MATI_BULAN_INI"]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["MATI_BULAN_INI"]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wni"]["PENDATANG_BULAN_INI"]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["PENDATANG_BULAN_INI"]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["PENDATANG_BULAN_INI"]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wni"]["PINDAH_BULAN_INI"]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["PINDAH_BULAN_INI"]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["PINDAH_BULAN_INI"]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AKHIR_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AKHIR_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_wni"]["PENDUDUK_AKHIR_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;
				}

			#------t_wna------
				foreach ($extract_data_json_lampid->item->t_main_kurang as $keya => $valuea) {
					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AWAL_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AWAL_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AWAL_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wna"]["LAHIR_BULAN_INI"]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["LAHIR_BULAN_INI"]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["LAHIR_BULAN_INI"]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wna"]["MATI_BULAN_INI"]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["MATI_BULAN_INI"]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["MATI_BULAN_INI"]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wna"]["PENDATANG_BULAN_INI"]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["PENDATANG_BULAN_INI"]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["PENDATANG_BULAN_INI"]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wna"]["PINDAH_BULAN_INI"]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["PINDAH_BULAN_INI"]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["PINDAH_BULAN_INI"]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;


					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AKHIR_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AKHIR_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_wna"]["PENDUDUK_AKHIR_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;
				}

			#------t_all------
				foreach ($extract_data_json_lampid->item->t_main_akhir as $keya => $valuea) {
					$array_data_new["lampid"]["t_all"]["PENDUDUK_AWAL_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["PENDUDUK_AWAL_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["PENDUDUK_AWAL_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;


					$array_data_new["lampid"]["t_all"]["LAHIR_BULAN_INI"]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["LAHIR_BULAN_INI"]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["LAHIR_BULAN_INI"]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;


					$array_data_new["lampid"]["t_all"]["MATI_BULAN_INI"]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["MATI_BULAN_INI"]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["MATI_BULAN_INI"]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;


					$array_data_new["lampid"]["t_all"]["PENDATANG_BULAN_INI"]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["PENDATANG_BULAN_INI"]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["PENDATANG_BULAN_INI"]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;


					$array_data_new["lampid"]["t_all"]["PINDAH_BULAN_INI"]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["PINDAH_BULAN_INI"]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["PINDAH_BULAN_INI"]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;


					$array_data_new["lampid"]["t_all"]["PENDUDUK_AKHIR_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["t_all"]["PENDUDUK_AKHIR_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["t_all"]["PENDUDUK_AKHIR_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;
				}

			#------all_wni------
				foreach ($extract_data_json_lampid->item->main_awal as $keya => $valuea) {
					$array_data_new["lampid"]["wni"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";



					$array_data_new["lampid"]["wni"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";



					$array_data_new["lampid"]["wni"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";



					$array_data_new["lampid"]["wni"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";



					$array_data_new["lampid"]["wni"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";



					$array_data_new["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["wni"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;

					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wni"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";
				}

			#------all_wna------
				foreach ($extract_data_json_lampid->item->main_kurang as $keya => $valuea) {
					$array_data_new["lampid"]["wna"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;



					$array_data_new["lampid"]["wna"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;



					$array_data_new["lampid"]["wna"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;



					$array_data_new["lampid"]["wna"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;



					$array_data_new["lampid"]["wna"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;



					$array_data_new["lampid"]["wna"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["wna"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["wna"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;



					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["wna"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["wna"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["wna"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";
				}

			#------all_all------
				foreach ($extract_data_json_lampid->item->main_akhir as $keya => $valuea) {
					$array_data_new["lampid"]["all"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AWAL_BULAN_INI->lp;



					$array_data_new["lampid"]["all"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->LAHIR_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->LAHIR_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->LAHIR_BULAN_INI->lp;



					$array_data_new["lampid"]["all"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->MATI_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->MATI_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->MATI_BULAN_INI->lp;



					$array_data_new["lampid"]["all"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDATANG_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDATANG_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDATANG_BULAN_INI->lp;



					$array_data_new["lampid"]["all"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PINDAH_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PINDAH_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PINDAH_BULAN_INI->lp;



					$array_data_new["lampid"]["all"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["lampid"]["all"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["lampid"]["all"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;




					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AWAL_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["all"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["LAHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["all"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["MATI_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDATANG_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["all"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PINDAH_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";

					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["l"] = "Laki-laki";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["p"] = "Perempuan";
					$array_kategori_data["lampid"]["item"]["all"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->kecamatan))]["lp"] = "Laki-laki dan Perempuan";
				}
					
			}
		#------lampid------

		#------rekap_kelurahan------
			$extract_data_json_rekap_kelurahan = $extract_data_json_main->rekap_kelurahan;

			if($extract_data_json_rekap_kelurahan->status){
			#------rekap_kelurahan------
				foreach ($extract_data_json_rekap_kelurahan->item->rekap_kelurahan as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);

					$array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["JUMLAH_KEL"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KEL"][$no] = $valuea->JUMLAH_KEL;
					$array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["JUMLAH_KK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KK"][$no] = $valuea->JUMLAH_KK;


					$array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["rekap_kelurahan"]["rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;


					$array_kategori_data["rekap_kelurahan"]["item"]["rekap_kelurahan"]["item"]["JUMLAH_KEL"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KEL"] = 0;
					$array_kategori_data["rekap_kelurahan"]["item"]["rekap_kelurahan"]["item"]["JUMLAH_KK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["JUMLAH_KK"] = 0;

					$array_kategori_data["rekap_kelurahan"]["item"]["rekap_kelurahan"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"] = "Laki-laki";
					$array_kategori_data["rekap_kelurahan"]["item"]["rekap_kelurahan"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"] = "Perempuan";
					$array_kategori_data["rekap_kelurahan"]["item"]["rekap_kelurahan"]["item"]["PENDUDUK_AKHIR_BULAN_INI"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"] = "Laki-laki dan Perempuan";
				}

			#------t_rekap_kelurahan------
				foreach ($extract_data_json_rekap_kelurahan->item->t_rekap_kelurahan as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);
					$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["JUMLAH_KEL"][$no] = $valuea->JUMLAH_KEL;
					$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["JUMLAH_KK"][$no] = $valuea->JUMLAH_KK;


					$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["l"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->l;
					$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["p"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->p;
					$array_data_new["rekap_kelurahan"]["t_rekap_kelurahan"]["PENDUDUK_AKHIR_BULAN_INI"]["lp"][$no] = $valuea->PENDUDUK_AKHIR_BULAN_INI->lp;
				}
			}
		#------rekap_kelurahan------

		#------agama------
			$extract_data_json_agama = $extract_data_json_main->agama;

			// print_r($extract_data_json_agama);

			if($extract_data_json_agama->item->agama){
			#------agama------
				foreach ($extract_data_json_agama->item->agama as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);

					$array_data_new["agama"]["agama"]["ISLAM"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["ISLAM"][$no] = $valuea->ISLAM;
					$array_data_new["agama"]["agama"]["KRISTEN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KRISTEN"][$no] = $valuea->KRISTEN;
					$array_data_new["agama"]["agama"]["KATHOLIK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KATHOLIK"][$no] = $valuea->KATHOLIK;
					$array_data_new["agama"]["agama"]["HINDU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["HINDU"][$no] = $valuea->HINDU;
					$array_data_new["agama"]["agama"]["BUDHA"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["BUDHA"][$no] = $valuea->BUDHA;
					$array_data_new["agama"]["agama"]["KONGHUCHU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KONGHUCHU"][$no] = $valuea->KONGHUCHU;
					$array_data_new["agama"]["agama"]["PENGHAYAT_KEPERCAYAAN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["PENGHAYAT_KEPERCAYAAN"][$no] = $valuea->PENGHAYAT_KEPERCAYAAN;



					$array_kategori_data["agama"]["item"]["agama"]["item"]["ISLAM"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["ISLAM"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["KRISTEN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KRISTEN"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["KATHOLIK"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KATHOLIK"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["HINDU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["HINDU"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["BUDHA"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["BUDHA"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["KONGHUCHU"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["KONGHUCHU"] = 0;
					$array_kategori_data["agama"]["item"]["agama"]["item"]["PENGHAYAT_KEPERCAYAAN"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["PENGHAYAT_KEPERCAYAAN"] = 0;
				}

			#------t_agama------
				foreach ($extract_data_json_agama->item->t_agama as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);
					$array_data_new["agama"]["t_agama"]["ISLAM"][$no] 		= $valuea->ISLAM;
					$array_data_new["agama"]["t_agama"]["KRISTEN"][$no] 	= $valuea->KRISTEN;
					$array_data_new["agama"]["t_agama"]["KATHOLIK"][$no] 	= $valuea->KATHOLIK;
					$array_data_new["agama"]["t_agama"]["HINDU"][$no] 		= $valuea->HINDU;
					$array_data_new["agama"]["t_agama"]["BUDHA"][$no] 		= $valuea->BUDHA;
					$array_data_new["agama"]["t_agama"]["KONGHUCHU"][$no] 	= $valuea->KONGHUCHU;
					$array_data_new["agama"]["t_agama"]["PENGHAYAT_KEPERCAYAAN"][$no] = $valuea->PENGHAYAT_KEPERCAYAAN;
				}
			}
		#------agama------

		#------rekam_ktp------
			$extract_data_json_rekam_ktp = $extract_data_json_main->rekam_ktp;

			if($extract_data_json_rekam_ktp->item->rekam_ktp){
			#------rekam_ktp------
				foreach ($extract_data_json_rekam_ktp->item->rekam_ktp as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);

					$array_data_new["rekam_ktp"]["rekam_ktp"]["jumlah_penduduk"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["jumlah_penduduk"][$no] = $valuea->jumlah_penduduk;

					$array_data_new["rekam_ktp"]["rekam_ktp"]["sudah_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["sudah_rekam_KTP_el"][$no] = $valuea->sudah_rekam_KTP_el;

					$array_data_new["rekam_ktp"]["rekam_ktp"]["belum_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["belum_rekam_KTP_el"][$no] = $valuea->belum_rekam_KTP_el;
					
					$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["wajib_memiliki_akta_kelahiran"][$no] = $valuea->wajib_memiliki_akta_kelahiran;

					$array_data_new["rekam_ktp"]["rekam_ktp"]["memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["memiliki_akta_kelahiran"][$no] = $valuea->memiliki_akta_kelahiran;


					$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"][$no] = $valuea->wajib_memiliki_KTP->l;
					$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"][$no] = $valuea->wajib_memiliki_KTP->p;
					$array_data_new["rekam_ktp"]["rekam_ktp"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"][$no] = $valuea->wajib_memiliki_KTP->lp;


					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["jumlah_penduduk"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["jumlah_penduduk"] = 0;

					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["sudah_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["sudah_rekam_KTP_el"] = 0;

					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["belum_rekam_KTP_el"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["belum_rekam_KTP_el"] = 0;

					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["wajib_memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["wajib_memiliki_akta_kelahiran"] = 0;

					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["memiliki_akta_kelahiran"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["memiliki_akta_kelahiran"] = 0;


					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["l"] = "Laki-laki";
					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["p"] = "Perempuan";
					$array_kategori_data["rekam_ktp"]["item"]["rekam_ktp"]["item"]["wajib_memiliki_KTP"][strtolower(str_replace(" ","_",$valuea->KECAMATAN))]["lp"] = "Laki-laki dan Perempuan";
				}
			#------rekam_ktp------

			#------t_rekam_ktp------
				foreach ($extract_data_json_rekam_ktp->item->t_rekam_ktp as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);

					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["jumlah_penduduk"]["jumlah_penduduk"][$no] = $valuea->jumlah_penduduk;

					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["sudah_rekam_KTP_el"]["sudah_rekam_KTP_el"][$no] = $valuea->sudah_rekam_KTP_el;

					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["belum_rekam_KTP_el"]["belum_rekam_KTP_el"][$no] = $valuea->belum_rekam_KTP_el;
					
					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_akta_kelahiran"]["wajib_memiliki_akta_kelahiran"][$no] = $valuea->wajib_memiliki_akta_kelahiran;

					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["memiliki_akta_kelahiran"]["memiliki_akta_kelahiran"][$no] = $valuea->memiliki_akta_kelahiran;


					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["l"][$no] = $valuea->wajib_memiliki_KTP->l;
					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["p"][$no] = $valuea->wajib_memiliki_KTP->p;
					$array_data_new["rekam_ktp"]["t_rekam_ktp"]["wajib_memiliki_KTP"]["lp"][$no] = $valuea->wajib_memiliki_KTP->lp;
				}
			#------t_rekam_ktp------
			}
		#------rekam_ktp------

		#------kelompok_umur------
			$extract_data_json_kelompok_umur = $extract_data_json_main->kelompok_umur;

			if($extract_data_json_kelompok_umur->item->value->kelompok_umur){
			#------kelompok_umur------
				foreach ($extract_data_json_kelompok_umur->item->value->kelompok_umur as $keya => $valuea) {
					// print_r("<pre>");
					// print_r($valuea);
					foreach ($extract_data_json_kelompok_umur->item->header as $key_head => $value_head) {
						if($key_head > 0 && $key_head < 15){
							$array_data_new["kelompok_umur"]["kelompok_umur"][str_replace(" ","_",$value_head)][strtolower(str_replace(" ","_",$valuea[0]))][str_replace(" ","_",$value_head)][$no] = $valuea[$key_head];

							$array_kategori_data["kelompok_umur"]["item"]["kelompok_umur"]["item"][str_replace(" ","_",$value_head)][strtolower(str_replace(" ","_",$valuea[0]))][str_replace(" ","_",$value_head)] = $value_head;
						}
					}
				}
			}
		#------kelompok_umur------

			$no++;
		}
		

		$data["data_json"] = json_encode($array_data_new);
		$data["periode_th"] = $th;
		$data["data_json_select"] = json_encode($array_kategori_data);

		// print_r("<pre>");
		// print_r(json_encode($array_data_new));

		$this->load->view("show_report/dispenduk/chart_js_basic", $data);
	}

	
}

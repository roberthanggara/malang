<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_sklh extends CI_Controller{

    public function __construct(){
        parent::__construct();


        // $this->load->library('form_validation');
        // $this->load->library('Generate_json');
        // $this->load->library('Generate_id');
        // $this->load->library('auth');
        $this->load->library('Response_message');
        // $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index(){
        print_r("surya");
    }
    
   
    public function get_html($url){
        $content = file_get_contents($url);
        return $content;
    }

    public function get_jml_satuan_pendidikan($param){

        $url = "http://117.103.70.194:8080/api/api_sebaran.php";

        $bentuk = "(5,6,9,10)";

            switch ($param) {
                case 'paud':
                    $bentuk = "(1,2,3,4,34)";
                    break;

                case 'dasar':
                    $bentuk = "(5,6,9,10)";
                    // $bentuk = "(5,6)";
                    break;

                case 'menengah':
                    $bentuk = "(13,15,16)";
                    break;

                case 'non_formal':
                    $bentuk = "(24,26,27)";
                    break;

                case 'khusus':
                    $bentuk = "(29,7,8,14)";
                    break;

                
                default:
                    $bentuk = "(5,6,9,10)";
                    break;
            }


            $param = "67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318";
            $fields = array(
               'token' => $param,
               'bentuk' => $bentuk
            );

            $postvars = http_build_query($fields);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
            // return $url;
    }


    public function get_data(){
        $data = $this->get_jml_satuan_pendidikan('dasar');
        $data_json = json_decode($data);
        // print_r("<pre>");
        // print_r(json_decode($data));

        $no = 0;
        $array_data = array();
        $array_kec = array();

        $array_kel = array();
        foreach ($data_json as $key => $value) {
            $tmp_array = array(
                        "nama_sekolah"=> $value->nama_sekolah,
                        "nama_kec"=> $value->nama_kec,
                        "latlng"=> array($value->lintang, $value->bujur),
                        "nomor_telepon"=> $value->nomor_telepon,
                        "email"=> $value->email,
                        "website"=> $value->website,
                        "tanggal_sk_pendirian"=> $value->tanggal_sk_pendirian,
                        "alamat_jalan"=> $value->alamat_jalan,
                        "desa_kelurahan"=> $value->desa_kelurahan,
                        "status_sekolah"=> $value->status_sekolah
                    );

            if(!array_key_exists(strtolower(str_replace("Kec. ", "", $value->nama_kec)), $array_kel)){
                $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))] = array();
            }

            if(!array_key_exists(strtolower(str_replace(" ", "_", $value->desa_kelurahan)), $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))])){
                $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))][strtolower(str_replace(" ", "_", $value->desa_kelurahan))] = array();
            }
            array_push($array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))][strtolower(str_replace(" ", "_", $value->desa_kelurahan))], $tmp_array);

            $no++;
        }

        $data_send["title"] = "Detail Sekolah";
        $data_send["data_json"] = json_encode($array_kel);


        $this->load->view("pendidikan/pendidikan_sklh", $data_send);
    }



    public function get_data_x($param){
        $data = $this->get_jml_satuan_pendidikan($param);
        $data_json = json_decode($data);
        
        $no = 0;
        $array_data = array();
        $array_kec = array();

        $array_kel = array();
        foreach ($data_json as $key => $value) {
            $tmp_array = array(
                        "nama_sekolah"=> $value->nama_sekolah,
                        "nama_kec"=> $value->nama_kec,
                        "latlng"=> array($value->lintang, $value->bujur),
                        "nomor_telepon"=> $value->nomor_telepon,
                        "email"=> $value->email,
                        "website"=> $value->website,
                        "tanggal_sk_pendirian"=> $value->tanggal_sk_pendirian,
                        "alamat_jalan"=> $value->alamat_jalan,
                        "desa_kelurahan"=> $value->desa_kelurahan,
                        "status_sekolah"=> $value->status_sekolah
                    );

            if(!array_key_exists(strtolower(str_replace("Kec. ", "", $value->nama_kec)), $array_kel)){
                $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))] = array();
            }

            if(!array_key_exists(strtolower(str_replace(" ", "_", $value->desa_kelurahan)), $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))])){
                $array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))][strtolower(str_replace(" ", "_", $value->desa_kelurahan))] = array();
            }
            array_push($array_kel[strtolower(str_replace("Kec. ", "", $value->nama_kec))][strtolower(str_replace(" ", "_", $value->desa_kelurahan))], $tmp_array);

            $no++;
        }

        // $data_send["title"] = "Detail Sekolah";
        // $data_send["data_json"] = ;

        print_r(json_encode($array_kel));

        // $this->load->view("pendidikan/Pendidikan_sklh", $data_send);
    }


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller{

    public function __construct(){
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('Response_message');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index(){
        print_r("surya");
    }
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function get_html($url){
        $content = file_get_contents($url);
        return $content;
    }

    public function get_jml_satuan_pendidikan($param){

        // print_r("surya");
        // JUMLAH SATUAN PENDIDIKAN (SEKOLAH) : KOTA MALANG
        // $content = $this->get_html("http://117.103.70.194:8080/index.php");
        // print_r($content);

            switch ($param) {
                case 'paud':
                    $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                    break;

                case 'dasar':
                    $url = "http://117.103.70.194:8080/api/api_sp_02.php";
                    break;

                case 'menengah':
                    $url = "http://117.103.70.194:8080/api/api_sp_03.php";
                    break;

                case 'non_formal':
                    $url = "http://117.103.70.194:8080/api/api_sp_04.php";
                    break;

                case 'khusus':
                    $url = "http://117.103.70.194:8080/api/api_sp_05.php";
                    break;


                case 'pendidik':
                    $url = "http://117.103.70.194:8080/api/api_ptk_01.php";
                    break;

                case 'peserta_didik':
                    $url = "http://117.103.70.194:8080/api/api_pd_01.php";
                    break;


                case 'rangkuman_dasar':
                    $url = "http://117.103.70.194:8080/api/api_rd_02.php";
                    break;

                case 'rangkuman_smp':
                    $url = "http://117.103.70.194:8080/api/api_rd_03.php";
                    break;

                case 'rangkuman_sma':
                    $url = "http://117.103.70.194:8080/api/api_rd_04.php";
                    break;


                case 'rangkuman_ma_req':
                    $stsek = $this->input->post("stsek");
                    $tahundata = (string)$this->input->post("tahundata");
                    $jenjang = $this->input->post("jenjang");

                    $url = "http://117.103.70.194:8080/api/api_rd_04.php?stsek=".$stsek."&tahundata=".$tahundata."&jenjang=".$jenjang;
                    break;



                case 'new_case':
                    $url = "http://117.103.70.194:8080/api/api_rd_04.php?jenjang=5&stsek=2&tahundata=20182";
                    break;

                
                default:
                    $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                    break;
            }


            $param = "67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318";
            $fields = array(
               'token' => $param
            );

            $postvars = http_build_query($fields);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
            // return $url;
    }

    public function index_paud(){
        $data = $this->get_jml_satuan_pendidikan("paud");
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "tk"=>array("nama"=>"Taman Kanak Kanak", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "kb"=>array("nama"=>"Kelompok Bermain", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "tpa"=>array("nama"=>"Tempat Penitipan Anak",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sps"=>array("nama"=>"Satuan Paud Sejenis",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "ra"=>array("nama"=>"Raudatul Athfal",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">
                        <option value=\"tk\">Taman Kanak Kanak</option>
                        <option value=\"kb\">Kelompok Bermain</option>
                        <option value=\"tpa\">Tempat Penitipan Anak</option>
                        <option value=\"sps\">Satuan Paud Sejenis</option>
                        <option value=\"ra\">Raudatul Athfal</option>
                    </select>";

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->tkn,
                                    "swasta"=>$value->tks);

                array_push($data_new["item"]["tk"]["item"], $tmp_array);

                $all = $value->tkn + $value->tks;
                
                $data_new["item"]["tk"]["all"]["s"] += $value->tks;
                $data_new["item"]["tk"]["all"]["n"] += $value->tkn;
                $data_new["item"]["tk"]["all"]["a"] += $all;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->kbn,
                                    "swasta"=>$value->kbs);

                array_push($data_new["item"]["kb"]["item"], $tmp_array);

                $all = $value->kbn + $value->kbs;
                
                $data_new["item"]["kb"]["all"]["s"] += $value->kbs;
                $data_new["item"]["kb"]["all"]["n"] += $value->kbn;
                $data_new["item"]["kb"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->tpan,
                                    "swasta"=>$value->tpas);

                array_push($data_new["item"]["tpa"]["item"], $tmp_array);

                $all = $value->tpan + $value->tpas;
                
                $data_new["item"]["tpa"]["all"]["s"] += $value->tpas;
                $data_new["item"]["tpa"]["all"]["n"] += $value->tpan;
                $data_new["item"]["tpa"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->spsn,
                                    "swasta"=>$value->spss);

                array_push($data_new["item"]["sps"]["item"], $tmp_array);

                $all = $value->spsn + $value->spss;
                
                $data_new["item"]["sps"]["all"]["s"] += $value->spss;
                $data_new["item"]["sps"]["all"]["n"] += $value->spsn;
                $data_new["item"]["sps"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->ran,
                                    "swasta"=>$value->ras);

                array_push($data_new["item"]["ra"]["item"], $tmp_array);

                $all = $value->ran + $value->ras;
                
                $data_new["item"]["ra"]["all"]["s"] += $value->ras;
                $data_new["item"]["ra"]["all"]["n"] += $value->ran;
                $data_new["item"]["ra"]["all"]["a"] += $all;
            }
        }

        // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    }

    public function index_pendidikan_dasar(){
        $data = $this->get_jml_satuan_pendidikan("dasar");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "sd"=>array("nama"=>"SEKOLAH DASAR", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smp"=>array("nama"=>"SEKOLAH MENENGAH PERTAMA", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sma"=>array("nama"=>"SEKOLAH MENENGAH ATAS",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smk"=>array("nama"=>"SEKOLAH MENENGAH KEJURUAN",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "mi"=>array("nama"=>"MADRASAH IPTIDAIYAH",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "mts"=>array("nama"=>"MADRASAH TSANAWIYAH",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "ma"=>array("nama"=>"MADRASAH ALIAH",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        // foreach ($data_new["item"] as $key => $value) {
        //     $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        // }
        $option .= "<option value=\"sd\">SEKOLAH DASAR</option>";
        $option .= "<option value=\"smp\">SEKOLAH MENENGAH PERTAMA</option>";
        $option .= "<option value=\"mi\">MADRASAH IPTIDAIYAH</option>";
        $option .= "<option value=\"mts\">MADRASAH TSANAWIYAH</option>";

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sdn,
                                    "swasta"=>$value->sds);

                array_push($data_new["item"]["sd"]["item"], $tmp_array);

                $all = $value->sdn + $value->sds;
                
                $data_new["item"]["sd"]["all"]["s"] += $value->sds;
                $data_new["item"]["sd"]["all"]["n"] += $value->sdn;
                $data_new["item"]["sd"]["all"]["a"] += $all;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smpn,
                                    "swasta"=>$value->smps);

                array_push($data_new["item"]["smp"]["item"], $tmp_array);

                $all = $value->smpn + $value->smps;
                
                $data_new["item"]["smp"]["all"]["s"] += $value->smps;
                $data_new["item"]["smp"]["all"]["n"] += $value->smpn;
                $data_new["item"]["smp"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sman,
                                    "swasta"=>$value->smas);

                array_push($data_new["item"]["sma"]["item"], $tmp_array);

                $all = $value->sman + $value->smas;
                
                $data_new["item"]["sma"]["all"]["s"] += $value->smas;
                $data_new["item"]["sma"]["all"]["n"] += $value->sman;
                $data_new["item"]["sma"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smkn,
                                    "swasta"=>$value->smks);

                array_push($data_new["item"]["smk"]["item"], $tmp_array);

                $all = $value->smkn + $value->smks;
                
                $data_new["item"]["smk"]["all"]["s"] += $value->smks;
                $data_new["item"]["smk"]["all"]["n"] += $value->smkn;
                $data_new["item"]["smk"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->min,
                                    "swasta"=>$value->mis);

                array_push($data_new["item"]["mi"]["item"], $tmp_array);

                $all = $value->min + $value->mis;
                
                $data_new["item"]["mi"]["all"]["s"] += $value->mis;
                $data_new["item"]["mi"]["all"]["n"] += $value->min;
                $data_new["item"]["mi"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->mtsn,
                                    "swasta"=>$value->mtss);

                array_push($data_new["item"]["mts"]["item"], $tmp_array);

                $all = $value->mtsn + $value->mtss;
                
                $data_new["item"]["mts"]["all"]["s"] += $value->mtss;
                $data_new["item"]["mts"]["all"]["n"] += $value->mtsn;
                $data_new["item"]["mts"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->man,
                                    "swasta"=>$value->mas);

                array_push($data_new["item"]["ma"]["item"], $tmp_array);

                $all = $value->man + $value->mas;
                
                $data_new["item"]["ma"]["all"]["s"] += $value->man;
                $data_new["item"]["ma"]["all"]["n"] += $value->mas;
                $data_new["item"]["ma"]["all"]["a"] += $all;
            }
        }

        // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    }

    public function index_pendidikan_menengah(){
        $data = $this->get_jml_satuan_pendidikan("menengah");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "sma"=>array("nama"=>"SEKOLAH MENENGAH ATAS",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smk"=>array("nama"=>"SEKOLAH MENENGAH KEJURUAN",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "ma"=>array("nama"=>"MADRASAH ALIYAH",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {

                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sman,
                                    "swasta"=>$value->smas);

                array_push($data_new["item"]["sma"]["item"], $tmp_array);

                $all = $value->sman + $value->smas;
                
                $data_new["item"]["sma"]["all"]["s"] += $value->smas;
                $data_new["item"]["sma"]["all"]["n"] += $value->sman;
                $data_new["item"]["sma"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smkn,
                                    "swasta"=>$value->smks);

                array_push($data_new["item"]["smk"]["item"], $tmp_array);

                $all = $value->smkn + $value->smks;
                
                $data_new["item"]["smk"]["all"]["s"] += $value->smks;
                $data_new["item"]["smk"]["all"]["n"] += $value->smkn;
                $data_new["item"]["smk"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->man,
                                    "swasta"=>$value->mas);

                array_push($data_new["item"]["ma"]["item"], $tmp_array);

                $all = $value->man + $value->mas;
                
                $data_new["item"]["ma"]["all"]["s"] += $value->man;
                $data_new["item"]["ma"]["all"]["n"] += $value->mas;
                $data_new["item"]["ma"]["all"]["a"] += $all;
            }
        }

        // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    }

    public function index_pendidikan_non_formal(){
        $data = $this->get_jml_satuan_pendidikan("non_formal");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "kursus"=>array("nama"=>"KURSUS",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "tbm"=>array("nama"=>"TAMANA BACAAN MASYARAKAT",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "pkbm"=>array("nama"=>"PUSAT KEGIATAN BELAJAR MASYARAKAT",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {

                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->kurn,
                                    "swasta"=>$value->kurs);

                array_push($data_new["item"]["kursus"]["item"], $tmp_array);

                $all = $value->kurn + $value->kurs;
                
                $data_new["item"]["kursus"]["all"]["s"] += $value->kurs;
                $data_new["item"]["kursus"]["all"]["n"] += $value->kurn;
                $data_new["item"]["kursus"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->tbmn,
                                    "swasta"=>$value->tbms);

                array_push($data_new["item"]["tbm"]["item"], $tmp_array);

                $all = $value->tbmn + $value->tbms;
                
                $data_new["item"]["tbm"]["all"]["s"] += $value->tbmn;
                $data_new["item"]["tbm"]["all"]["n"] += $value->tbms;
                $data_new["item"]["tbm"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->pkbmn,
                                    "swasta"=>$value->pkbms);

                array_push($data_new["item"]["pkbm"]["item"], $tmp_array);

                $all = $value->pkbmn + $value->pkbms;
                
                $data_new["item"]["pkbm"]["all"]["s"] += $value->pkbmn;
                $data_new["item"]["pkbm"]["all"]["n"] += $value->pkbms;
                $data_new["item"]["pkbm"]["all"]["a"] += $all;
            }
        }

        // // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // // // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    }

    public function index_pendidikan_khusus(){
        $data = $this->get_jml_satuan_pendidikan("khusus");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "slb"=>array("nama"=>"SEKOLAH LUAR BIASA",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sdlb"=>array("nama"=>"SEKOLAH DASAR LUAR BIASA",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smplb"=>array("nama"=>"SEKOLAH MENEGAH PERTAMA LUAR BIASA",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smlb"=>array("nama"=>"SEKOLAH MENEGAH LUAR BIASA",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {

                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->slbn,
                                    "swasta"=>$value->slbs);

                array_push($data_new["item"]["slb"]["item"], $tmp_array);

                $all = $value->slbn + $value->slbs;
                
                $data_new["item"]["slb"]["all"]["s"] += $value->slbs;
                $data_new["item"]["slb"]["all"]["n"] += $value->slbn;
                $data_new["item"]["slb"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sdlbn,
                                    "swasta"=>$value->sdlbs);

                array_push($data_new["item"]["sdlb"]["item"], $tmp_array);

                $all = $value->sdlbn + $value->sdlbs;
                
                $data_new["item"]["sdlb"]["all"]["s"] += $value->sdlbs;
                $data_new["item"]["sdlb"]["all"]["n"] += $value->sdlbn;
                $data_new["item"]["sdlb"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smplbn,
                                    "swasta"=>$value->smplbs);

                array_push($data_new["item"]["smplb"]["item"], $tmp_array);

                $all = $value->smplbn + $value->smplbs;
                
                $data_new["item"]["smplb"]["all"]["s"] += $value->smplbs;
                $data_new["item"]["smplb"]["all"]["n"] += $value->smplbn;
                $data_new["item"]["smplb"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smlbn,
                                    "swasta"=>$value->smlbs);

                array_push($data_new["item"]["smlb"]["item"], $tmp_array);

                $all = $value->smlbn + $value->smlbs;
                
                $data_new["item"]["smlb"]["all"]["s"] += $value->smlbs;
                $data_new["item"]["smlb"]["all"]["n"] += $value->smlbn;
                $data_new["item"]["smlb"]["all"]["a"] += $all;
            }
        }

        // // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // // // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    }



    public function index_pendidik(){
        $data = $this->get_jml_satuan_pendidikan("pendidik");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "sd"=>array("nama"=>"SEKOLAH DASAR", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smp"=>array("nama"=>"SEKOLAH MENENGAH PERTAMA", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sma"=>array("nama"=>"SEKOLAH MENENGAH ATAS",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smk"=>array("nama"=>"SEKOLAH MENENGAH KEJURUAN",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }
        // $option .= "<option value=\"sd\">SEKOLAH DASAR</option>";
        // $option .= "<option value=\"smp\">SEKOLAH MENENGAH PERTAMA</option>";
        // $option .= "<option value=\"mi\">MADRASAH IPTIDAIYAH</option>";
        // $option .= "<option value=\"mts\">MADRASAH TSANAWIYAH</option>";

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;
            $data_new["title_header"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sdn,
                                    "swasta"=>$value->sds);

                array_push($data_new["item"]["sd"]["item"], $tmp_array);

                $all = $value->sdn + $value->sds;
                
                $data_new["item"]["sd"]["all"]["s"] += $value->sds;
                $data_new["item"]["sd"]["all"]["n"] += $value->sdn;
                $data_new["item"]["sd"]["all"]["a"] += $all;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smpn,
                                    "swasta"=>$value->smps);

                array_push($data_new["item"]["smp"]["item"], $tmp_array);

                $all = $value->smpn + $value->smps;
                
                $data_new["item"]["smp"]["all"]["s"] += $value->smps;
                $data_new["item"]["smp"]["all"]["n"] += $value->smpn;
                $data_new["item"]["smp"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sman,
                                    "swasta"=>$value->smas);

                array_push($data_new["item"]["sma"]["item"], $tmp_array);

                $all = $value->sman + $value->smas;
                
                $data_new["item"]["sma"]["all"]["s"] += $value->smas;
                $data_new["item"]["sma"]["all"]["n"] += $value->sman;
                $data_new["item"]["sma"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smkn,
                                    "swasta"=>$value->smks);

                array_push($data_new["item"]["smk"]["item"], $tmp_array);

                $all = $value->smkn + $value->smks;
                
                $data_new["item"]["smk"]["all"]["s"] += $value->smks;
                $data_new["item"]["smk"]["all"]["n"] += $value->smkn;
                $data_new["item"]["smk"]["all"]["a"] += $all;

            }
        }

        // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);       
    }

    public function index_peserta_didik(){
        $data = $this->get_jml_satuan_pendidikan("peserta_didik");
        // print_r($data);
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "sd"=>array("nama"=>"SEKOLAH DASAR", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smp"=>array("nama"=>"SEKOLAH MENENGAH PERTAMA", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sma"=>array("nama"=>"SEKOLAH MENENGAH ATAS",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "smk"=>array("nama"=>"SEKOLAH MENENGAH KEJURUAN",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }
        // $option .= "<option value=\"sd\">SEKOLAH DASAR</option>";
        // $option .= "<option value=\"smp\">SEKOLAH MENENGAH PERTAMA</option>";
        // $option .= "<option value=\"mi\">MADRASAH IPTIDAIYAH</option>";
        // $option .= "<option value=\"mts\">MADRASAH TSANAWIYAH</option>";

        $option .= "</select>";

        // print_r($option);

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;
            $data_new["title_header"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sdn,
                                    "swasta"=>$value->sds);

                array_push($data_new["item"]["sd"]["item"], $tmp_array);

                $all = $value->sdn + $value->sds;
                
                $data_new["item"]["sd"]["all"]["s"] += $value->sds;
                $data_new["item"]["sd"]["all"]["n"] += $value->sdn;
                $data_new["item"]["sd"]["all"]["a"] += $all;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smpn,
                                    "swasta"=>$value->smps);

                array_push($data_new["item"]["smp"]["item"], $tmp_array);

                $all = $value->smpn + $value->smps;
                
                $data_new["item"]["smp"]["all"]["s"] += $value->smps;
                $data_new["item"]["smp"]["all"]["n"] += $value->smpn;
                $data_new["item"]["smp"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->sman,
                                    "swasta"=>$value->smas);

                array_push($data_new["item"]["sma"]["item"], $tmp_array);

                $all = $value->sman + $value->smas;
                
                $data_new["item"]["sma"]["all"]["s"] += $value->smas;
                $data_new["item"]["sma"]["all"]["n"] += $value->sman;
                $data_new["item"]["sma"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->smkn,
                                    "swasta"=>$value->smks);

                array_push($data_new["item"]["smk"]["item"], $tmp_array);

                $all = $value->smkn + $value->smks;
                
                $data_new["item"]["smk"]["all"]["s"] += $value->smks;
                $data_new["item"]["smk"]["all"]["n"] += $value->smkn;
                $data_new["item"]["smk"]["all"]["a"] += $all;

            }
        }

        // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);       
    }


    public function index_rd(){
        $data = $this->get_jml_satuan_pendidikan("rangkuman_dasar");
        // print_r($data);
        $data_new = array(
                            "title_header"=>"",
                            "title"=>array(
                                        "akred"=>array("A","B","C","TT"),
                                        "kur"=>array("1994","2004","ktsp","2013")
                                    ),
                            "item" => array(
                                        "akred"=>array("nama"=>"AKREDITASI", 
                                            "item"=>array(), 
                                            "all"=>array(   "a"=>0,
                                                            "b"=>0,
                                                            "c"=>0,
                                                            "tt"=>0,)
                                        ),
                                        "kur"=>array("nama"=>"KURIKULUM YANG DIGUNAKAN",
                                            "item"=>array(),
                                            "all"=>array("1994"=>0,
                                                        "2004"=>0,
                                                        "ktsp"=>0,
                                                        "2013"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }
        $option .= "</select>";


        if($data){
            $data_array = json_decode($data);
            $data_new["title_main"] = $data_array->title;
            $data_new["title_header"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "akred_a"=>$value->akred_a,
                                    "akred_b"=>$value->akred_b,
                                    "akred_c"=>$value->akred_c,
                                    "akred_tt"=>$value->akred_tt);

                array_push($data_new["item"]["akred"]["item"], $tmp_array);

                $all = $value->akred_a + $value->akred_b + $value->akred_c + $value->akred_tt;
                
                $data_new["item"]["akred"]["all"]["a"] += $value->akred_a;
                $data_new["item"]["akred"]["all"]["b"] += $value->akred_b;
                $data_new["item"]["akred"]["all"]["c"] += $value->akred_c;
                $data_new["item"]["akred"]["all"]["tt"] += $value->akred_tt;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "kur_1994"=>$value->kur_1994,
                                    "kur_2004"=>$value->kur_2004,
                                    "kur_ktsp"=>$value->kur_ktsp,
                                    "kur_2013"=>$value->kur_2013);

                array_push($data_new["item"]["kur"]["item"], $tmp_array);

                $all = $value->kur_1994 + $value->kur_2004 + $value->kur_ktsp + $value->kur_2013;
                
                $data_new["item"]["kur"]["all"]["1994"] += $value->kur_1994;
                $data_new["item"]["kur"]["all"]["2004"] += $value->kur_2004;
                $data_new["item"]["kur"]["all"]["ktsp"] += $value->kur_ktsp;
                $data_new["item"]["kur"]["all"]["2013"] += $value->kur_2013;
            }
        }

        // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/akreditasi_detail", $data_send);       
    }

    public function index_rmp(){
        $data = $this->get_jml_satuan_pendidikan("rangkuman_smp");
        // print_r($data);
        $data_new = array(
                            "title_header"=>"",
                            "title"=>array(
                                        "akred"=>array("A","B","C","TT"),
                                        "iso"=>array("1","2","3","4")
                                    ),
                            "item" => array(
                                        "akred"=>array("nama"=>"AKREDITASI", 
                                            "item"=>array(), 
                                            "all"=>array(   "a"=>0,
                                                            "b"=>0,
                                                            "c"=>0,
                                                            "tt"=>0)
                                        ),
                                        "iso"=>array("nama"=>"SERTIFIKASI",
                                            "item"=>array(),
                                            "all"=>array("1"=>0,
                                                        "2"=>0,
                                                        "3"=>0,
                                                        "4"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }
        $option .= "</select>";


        if($data){
            $data_array = json_decode($data);
            $data_new["title_main"] = $data_array->title;
            $data_new["title_header"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "akred_a"=>$value->akred_a,
                                    "akred_b"=>$value->akred_b,
                                    "akred_c"=>$value->akred_c,
                                    "akred_tt"=>$value->akred_tt);

                array_push($data_new["item"]["akred"]["item"], $tmp_array);

                $all = $value->akred_a + $value->akred_b + $value->akred_c + $value->akred_tt;
                
                $data_new["item"]["akred"]["all"]["a"] += $value->akred_a;
                $data_new["item"]["akred"]["all"]["b"] += $value->akred_b;
                $data_new["item"]["akred"]["all"]["c"] += $value->akred_c;
                $data_new["item"]["akred"]["all"]["tt"] += $value->akred_tt;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "iso_1"=>$value->iso_1,
                                    "iso_2"=>$value->iso_2,
                                    "iso_3"=>$value->iso_3,
                                    "iso_4"=>$value->iso_4);

                array_push($data_new["item"]["iso"]["item"], $tmp_array);

                $all = $value->iso_1 + $value->iso_2 + $value->iso_3 + $value->iso_4;
                
                $data_new["item"]["iso"]["all"]["1994"] += $value->iso_1;
                $data_new["item"]["iso"]["all"]["2004"] += $value->iso_2;
                $data_new["item"]["iso"]["all"]["ktsp"] += $value->iso_3;
                $data_new["item"]["iso"]["all"]["2013"] += $value->iso_4;
            }
        }

        // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/akreditasi_detail_iso", $data_send);       
    }

    public function index_ma(){
        $data = $this->get_jml_satuan_pendidikan("rangkuman_sma");
        // print_r($data);
        $data_new = array(
                            "title_header"=>"",
                            "title"=>array(
                                        "akred"=>array("A","B","C","TT"),
                                        "iso"=>array("1","2","3","4")
                                    ),
                            "item" => array(
                                        "akred"=>array("nama"=>"AKREDITASI", 
                                            "item"=>array(), 
                                            "all"=>array(   "a"=>0,
                                                            "b"=>0,
                                                            "c"=>0,
                                                            "tt"=>0)
                                        ),
                                        "iso"=>array("nama"=>"SERTIFIKASI",
                                            "item"=>array(),
                                            "all"=>array("1"=>0,
                                                        "2"=>0,
                                                        "3"=>0,
                                                        "4"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

        foreach ($data_new["item"] as $key => $value) {
            $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
        }
        $option .= "</select>";


        if($data){
            $data_array = json_decode($data);
            $data_new["title_main"] = $data_array->title;
            $data_new["title_header"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "akred_a"=>$value->akred_a,
                                    "akred_b"=>$value->akred_b,
                                    "akred_c"=>$value->akred_c,
                                    "akred_tt"=>$value->akred_tt);

                array_push($data_new["item"]["akred"]["item"], $tmp_array);

                $all = $value->akred_a + $value->akred_b + $value->akred_c + $value->akred_tt;
                
                $data_new["item"]["akred"]["all"]["a"] += $value->akred_a;
                $data_new["item"]["akred"]["all"]["b"] += $value->akred_b;
                $data_new["item"]["akred"]["all"]["c"] += $value->akred_c;
                $data_new["item"]["akred"]["all"]["tt"] += $value->akred_tt;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "jml_sek"=>$value->jml_sek,
                                    "iso_1"=>$value->iso_1,
                                    "iso_2"=>$value->iso_2,
                                    "iso_3"=>$value->iso_3,
                                    "iso_4"=>$value->iso_4);

                array_push($data_new["item"]["iso"]["item"], $tmp_array);

                $all = $value->iso_1 + $value->iso_2 + $value->iso_3 + $value->iso_4;
                
                $data_new["item"]["iso"]["all"]["1994"] += $value->iso_1;
                $data_new["item"]["iso"]["all"]["2004"] += $value->iso_2;
                $data_new["item"]["iso"]["all"]["ktsp"] += $value->iso_3;
                $data_new["item"]["iso"]["all"]["2013"] += $value->iso_4;
            }
        }

        // // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_select"] = $data_array->option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data);

        $this->load->view("pendidikan/akreditasi_detail_iso", $data_send);       
    }



    public function index_ma_req(){
        $msg_detail = array("stsek"=>"", "tahundata"=>"", "jenjang"=>"");
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input()){
            $data = $this->get_jml_satuan_pendidikan("rangkuman_ma_req");
            $data_new = array(
                            "title_header"=>"",
                            "title"=>array(
                                        "akred"=>array("A","B","C","TT"),
                                        "iso"=>array("1","2","3","4")
                                    ),
                            "item" => array(
                                        "akred"=>array("nama"=>"AKREDITASI", 
                                            "item"=>array(), 
                                            "all"=>array(   "a"=>0,
                                                            "b"=>0,
                                                            "c"=>0,
                                                            "tt"=>0)
                                        ),
                                        "iso"=>array("nama"=>"SERTIFIKASI",
                                            "item"=>array(),
                                            "all"=>array("1"=>0,
                                                        "2"=>0,
                                                        "3"=>0,
                                                        "4"=>0)
                                        )
                                    )
                        );

            $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">";

            foreach ($data_new["item"] as $key => $value) {
                $option .= "<option value=\"".$key."\">".$value["nama"]."</option>";
            }
            $option .= "</select>";


            if($data){
                $data_array = json_decode($data);
                $data_new["title_main"] = $data_array->title;
                $data_new["title_header"] = $data_array->title_header;

                foreach ($data_array->item as $key => $value) {
                    $tmp_array = array("nama_kecamatan"=>$value->nama,
                                        "jml_sek"=>$value->jml_sek,
                                        "akred_a"=>$value->akred_a,
                                        "akred_b"=>$value->akred_b,
                                        "akred_c"=>$value->akred_c,
                                        "akred_tt"=>$value->akred_tt);

                    array_push($data_new["item"]["akred"]["item"], $tmp_array);

                    $all = $value->akred_a + $value->akred_b + $value->akred_c + $value->akred_tt;
                    
                    $data_new["item"]["akred"]["all"]["a"] += $value->akred_a;
                    $data_new["item"]["akred"]["all"]["b"] += $value->akred_b;
                    $data_new["item"]["akred"]["all"]["c"] += $value->akred_c;
                    $data_new["item"]["akred"]["all"]["tt"] += $value->akred_tt;


                    $tmp_array = array("nama_kecamatan"=>$value->nama,
                                        "jml_sek"=>$value->jml_sek,
                                        "iso_1"=>$value->iso_1,
                                        "iso_2"=>$value->iso_2,
                                        "iso_3"=>$value->iso_3,
                                        "iso_4"=>$value->iso_4);

                    array_push($data_new["item"]["iso"]["item"], $tmp_array);

                    $all = $value->iso_1 + $value->iso_2 + $value->iso_3 + $value->iso_4;
                    
                    $data_new["item"]["iso"]["all"]["1994"] += $value->iso_1;
                    $data_new["item"]["iso"]["all"]["2004"] += $value->iso_2;
                    $data_new["item"]["iso"]["all"]["ktsp"] += $value->iso_3;
                    $data_new["item"]["iso"]["all"]["2013"] += $value->iso_4;
                }
            }

            // // $option .= "";

            $data_send["main_data"] = $data_new;
            $data_send["option"] = $option;
            $data_send["data_select"] = $data_array->option;
            $data_send["data_json"] = $data_new;

            // print_r($data_array->option);

            // $this->load->view("pendidikan/akreditasi_detail_iso", $data_send);

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail["item"] = $data_send;
        }else{
            $msg_detail["stsek"] = strip_tags(form_error("stsek"));
            $msg_detail["tahundata"] = strip_tags(form_error("tahundata"));
            $msg_detail["jenjang"] = strip_tags(form_error("jenjang"));
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
        // print_r($data);
              
    }

    private function validate_input(){
        $config_val_input = array(
                array(
                    'field'=>'stsek',
                    'label'=>'stsek',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'tahundata',
                    'label'=>'tahundata',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'jenjang',
                    'label'=>'jenjang',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_aw(){
        // $data = $this->get_jml_satuan_pendidikan("rangkuman_sma");
        print_r($_POST);
        // $data = $this->get_jml_satuan_pendidikan("rangkuman_ma_req");

        print_r($data);
    }
}

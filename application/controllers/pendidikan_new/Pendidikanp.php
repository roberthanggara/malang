<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikanp extends CI_Controller{

    public function __construct(){
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index(){
        print_r("surya");
    }
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function get_html($url){
        $content = file_get_contents($url);
        return $content;
    }

    public function get_jml_satuan_pendidikan($param){

        // print_r("surya");
        // JUMLAH SATUAN PENDIDIKAN (SEKOLAH) : KOTA MALANG
        // $content = $this->get_html("http://117.103.70.194:8080/index.php");
        // print_r($content);

            switch ($param) {
                case 'paud':
                    $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                    break;

                case 'dasar':
                    $url = "http://117.103.70.194:8080/api/api_sp_02.php";
                    break;

                case 'menengah':
                    $url = "http://117.103.70.194:8080/api/api_sp_03.php";
                    break;

                case 'non_formal':
                    $url = "http://117.103.70.194:8080/api/api_sp_04.php";
                    break;

                case 'khusus':
                    $url = "http://117.103.70.194:8080/api/api_sp_05.php";
                    break;


                case 'pendidik':
                    $url = "http://117.103.70.194:8080/api/api_ptk_01.php";
                    break;

                case 'peserta_didik':
                    $url = "http://117.103.70.194:8080/api/api_pd_01.php";
                    break;

                case 'rangkuman_dasar':
                    $url = "http://117.103.70.194:8080/api/api_rd_02.php";
                    break;

                case 'rangkuman_smp':
                    $url = "http://117.103.70.194:8080/api/api_rd_03.php";
                    break;

                case 'rangkuman_sma':
                    $url = "http://117.103.70.194:8080/api/api_rd_04.php";
                    break;

                
                default:
                    $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                    break;
            }


            $param = "67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318";
            $fields = array(
               'token' => $param
            );

            $postvars = http_build_query($fields);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
    }

    public function index_paud(){
        $data = $this->get_jml_satuan_pendidikan("paud");
        $data_new = array(
                            "title"=>"",
                            "item" => array(
                                        "tk"=>array("nama"=>"Taman Kanak Kanak", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "kb"=>array("nama"=>"Kelompok Bermain", 
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "tpa"=>array("nama"=>"Tempat Penitipan Anak",
                                            "item"=>array(),
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "sps"=>array("nama"=>"Satuan Paud Sejenis",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        ),
                                        "ra"=>array("nama"=>"Raudatul Athfal",
                                            "item"=>array(), 
                                            "all"=>array("n"=>0,
                                                        "s"=>0,
                                                        "a"=>0)
                                        )
                                    )
                        );

        $option = " <select class=\"form-control\" name=\"select_tipe\" id=\"select_tipe\">
                        <option value=\"tk\">Taman Kanak Kanak</option>
                        <option value=\"kb\">Kelompok Bermain</option>
                        <option value=\"tpa\">Tempat Penitipan Anak</option>
                        <option value=\"sps\">Satuan Paud Sejenis</option>
                        <option value=\"ra\">Raudatul Athfal</option>
                    </select>";

        if($data){
            $data_array = json_decode($data);
            $data_new["title"] = $data_array->title;

            foreach ($data_array->item as $key => $value) {
                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->tkn,
                                    "swasta"=>$value->tks);

                array_push($data_new["item"]["tk"]["item"], $tmp_array);

                $all = $value->tkn + $value->tks;
                
                $data_new["item"]["tk"]["all"]["s"] += $value->tks;
                $data_new["item"]["tk"]["all"]["n"] += $value->tkn;
                $data_new["item"]["tk"]["all"]["a"] += $all;


                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->kbn,
                                    "swasta"=>$value->kbs);

                array_push($data_new["item"]["kb"]["item"], $tmp_array);

                $all = $value->kbn + $value->kbs;
                
                $data_new["item"]["kb"]["all"]["s"] += $value->kbs;
                $data_new["item"]["kb"]["all"]["n"] += $value->kbn;
                $data_new["item"]["kb"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->tpan,
                                    "swasta"=>$value->tpas);

                array_push($data_new["item"]["tpa"]["item"], $tmp_array);

                $all = $value->tpan + $value->tpas;
                
                $data_new["item"]["tpa"]["all"]["s"] += $value->tpas;
                $data_new["item"]["tpa"]["all"]["n"] += $value->tpan;
                $data_new["item"]["tpa"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->spsn,
                                    "swasta"=>$value->spss);

                array_push($data_new["item"]["sps"]["item"], $tmp_array);

                $all = $value->spsn + $value->spss;
                
                $data_new["item"]["sps"]["all"]["s"] += $value->spss;
                $data_new["item"]["sps"]["all"]["n"] += $value->spsn;
                $data_new["item"]["sps"]["all"]["a"] += $all;



                $tmp_array = array("nama_kecamatan"=>$value->nama,
                                    "negeri"=>$value->ran,
                                    "swasta"=>$value->ras);

                array_push($data_new["item"]["ra"]["item"], $tmp_array);

                $all = $value->ran + $value->ras;
                
                $data_new["item"]["ra"]["all"]["s"] += $value->ras;
                $data_new["item"]["ra"]["all"]["n"] += $value->ran;
                $data_new["item"]["ra"]["all"]["a"] += $all;
            }
        }

        // $option .= "";

        $data_send["main_data"] = $data_new;
        $data_send["option"] = $option;
        $data_send["data_json"] = json_encode($data_new);

        // print_r($data_send);

        $this->load->view("pendidikan/pendidikan_detail", $data_send);
    } 
}

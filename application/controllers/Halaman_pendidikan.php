<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman_pendidikan extends CI_Controller {

	public function __construct(){
        parent::__construct();
    }
   
	function index(){
	 	$this->load->view('v_halaman_pendidikan');
	}


	public function get_jml_satuan_pendidikan($param){

    // print_r("surya");
    // JUMLAH SATUAN PENDIDIKAN (SEKOLAH) : KOTA MALANG
    // $content = $this->get_html("http://117.103.70.194:8080/index.php");
    // print_r($content);

        switch ($param) {
            case 'paud':
                $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                break;

            case 'dasar':
                $url = "http://117.103.70.194:8080/api/api_sp_02.php";
                break;

            case 'menengah':
                $url = "http://117.103.70.194:8080/api/api_sp_03.php";
                break;

            case 'non_formal':
                $url = "http://117.103.70.194:8080/api/api_sp_04.php";
                break;

            case 'khusus':
                $url = "http://117.103.70.194:8080/api/api_sp_05.php";
                break;


            case 'pendidik':
                $url = "http://117.103.70.194:8080/api/api_ptk_01.php";
                break;

            case 'peserta_didik':
                $url = "http://117.103.70.194:8080/api/api_pd_01.php";
                break;


            case 'rangkuman_dasar':
                $url = "http://117.103.70.194:8080/api/api_rd_02.php";
                break;

            case 'rangkuman_smp':
                $url = "http://117.103.70.194:8080/api/api_rd_03.php";
                break;

            case 'rangkuman_sma':
                $url = "http://117.103.70.194:8080/api/api_rd_04.php";
                break;


            case 'rangkuman_ma_req':
                $stsek = $this->input->post("stsek");
                $tahundata = (string)$this->input->post("tahundata");
                $jenjang = $this->input->post("jenjang");

                $url = "http://117.103.70.194:8080/api/api_rd_04.php?stsek=".$stsek."&tahundata=".$tahundata."&jenjang=".$jenjang;
                break;



            case 'new_case':
                $url = "http://117.103.70.194:8080/api/api_rd_04.php?jenjang=5&stsek=2&tahundata=20182";
                break;

            
            default:
                $url = "http://117.103.70.194:8080/api/api_sp_01.php";
                break;
        }


        $param = "67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318";
        $fields = array(
           'token' => $param
        );

        $postvars = http_build_query($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
            // return $url;
    }


    public function get_data_local(){
        $item_kecamatan = array(
            "pd_pra"=>array(
                "keterangan"=>"Pendidikan Pra Sekolah Dasar",
                "item"=>array()
            ),
            "dasar"=>array(
                "keterangan"=>"Pendidikan Dasar",
                "item"=>array()
            ),
            "menengah_pertama"=>array(
                "keterangan"=>"Pendidikan Menengah Pertama",
                "item"=>array()
            ),
            "menengah_atas"=>array(
                "keterangan"=>"Pendidikan Menengah Atas",
                "item"=>array()
            ),
            "khusus"=>array(
                "keterangan"=>"Pendidikan Khusus",
                "item"=>array()
            ),
            "non_formal"=>array(
                "keterangan"=>"Pendidikan Non-Formal",
                "item"=>array()
            )
        );

        $option = " <select class=\"form-control\" name=\"select_kec\" id=\"select_kec\">";

        $data_paud = $this->get_jml_satuan_pendidikan("paud");

        if($data_paud){
            $data_array = json_decode($data_paud);
            foreach ($data_array->item as $key => $value) {
                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["pd_pra"]["item"])){
                    $item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();

                    $tmp_kode = str_replace(" ", "", $value->kode_wilayah);

                    $option .= "<option value=\"".$tmp_kode."\">".$value->nama."</option>";
                }
                

                $tmp_item = array(
                        "nama"=>"Taman Kanak Kanak", 
                        "n"=>$value->tkn,
                        "s"=>$value->tks,
                        "a"=>(int)$value->tkn+(int)$value->tks
                    );
                array_push($item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Kelompok Bermain", 
                        "n"=>$value->kbn,
                        "s"=>$value->kbs,
                        "a"=>(int)$value->kbn+(int)$value->kbs
                    );
                array_push($item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Tempat Penitipan Anak", 
                        "n"=>$value->tpan,
                        "s"=>$value->tpas,
                        "a"=>(int)$value->tpan+(int)$value->tpas
                    );
                array_push($item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Satuan Paud Sejenis", 
                        "n"=>$value->spsn,
                        "s"=>$value->spss,
                        "a"=>(int)$value->spsn+(int)$value->spss
                    );
                array_push($item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Raudatul Athfal", 
                        "n"=>$value->ran,
                        "s"=>$value->ras,
                        "a"=>(int)$value->ran+(int)$value->ras
                    );
                array_push($item_kecamatan["pd_pra"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);
                // print_r($value);
            }
        }

        $option .= "</select>";


        $data_dasar = $this->get_jml_satuan_pendidikan("dasar");
        if($data_dasar){
            $data_array = json_decode($data_dasar);
            foreach ($data_array->item as $key => $value) {

                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["dasar"]["item"])){
                    $item_kecamatan["dasar"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["dasar"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();
                }

                $tmp_item = array(
                        "nama"=>"Sekolah Dasar", 
                        "n"=>$value->sdn,
                        "s"=>$value->sds,
                        "a"=>(int)$value->sdn+(int)$value->sds
                    );
                array_push($item_kecamatan["dasar"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Madrasah Iptidaiyah", 
                        "n"=>$value->min,
                        "s"=>$value->mis,
                        "a"=>(int)$value->min+(int)$value->mis
                    );
                array_push($item_kecamatan["dasar"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);



                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["dasar"]["item"])){
                    $item_kecamatan["menengah_pertama"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["menengah_pertama"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();
                }

                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Pertama", 
                        "n"=>$value->smpn,
                        "s"=>$value->smps,
                        "a"=>(int)$value->smpn+(int)$value->smps
                    );
                array_push($item_kecamatan["menengah_pertama"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Madrasah Tsanawiyah", 
                        "n"=>$value->mtsn,
                        "s"=>$value->mtss,
                        "a"=>(int)$value->mtsn+(int)$value->mtss
                    );
                array_push($item_kecamatan["menengah_pertama"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);



                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["dasar"]["item"])){
                    $item_kecamatan["menengah_atas"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["menengah_atas"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();
                }

                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Atas", 
                        "n"=>$value->sman,
                        "s"=>$value->smas,
                        "a"=>(int)$value->sman+(int)$value->smas
                    );
                array_push($item_kecamatan["menengah_atas"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Kejuruan", 
                        "n"=>$value->smkn,
                        "s"=>$value->smks,
                        "a"=>(int)$value->smkn+(int)$value->smks
                    );
                array_push($item_kecamatan["menengah_atas"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Madrasah Aliyah", 
                        "n"=>$value->man,
                        "s"=>$value->mas,
                        "a"=>(int)$value->man+(int)$value->mas
                    );
                array_push($item_kecamatan["menengah_atas"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);
            }
        }


        $data_non_formal = $this->get_jml_satuan_pendidikan("non_formal");
        if($data_non_formal){
            $data_array = json_decode($data_non_formal);
            foreach ($data_array->item as $key => $value) {
                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["non_formal"]["item"])){
                    $item_kecamatan["non_formal"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["non_formal"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();
                }

                $tmp_item = array(
                        "nama"=>"Kursus", 
                        "n"=>$value->kurn,
                        "s"=>$value->kurs,
                        "a"=>(int)$value->kurn+(int)$value->kurs
                    );
                array_push($item_kecamatan["non_formal"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Taman Bacaan Masyarakat", 
                        "n"=>$value->tbmn,
                        "s"=>$value->tbms,
                        "a"=>(int)$value->tbmn+(int)$value->tbms
                    );
                array_push($item_kecamatan["non_formal"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Pusat Kegiatan Belajar Masyarakat", 
                        "n"=>$value->pkbmn,
                        "s"=>$value->pkbms,
                        "a"=>(int)$value->pkbmn+(int)$value->pkbms
                    );
                array_push($item_kecamatan["non_formal"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

            }
        }


        $data_khusus = $this->get_jml_satuan_pendidikan("khusus");
        if($data_khusus){
            $data_array = json_decode($data_khusus);
            foreach ($data_array->item as $key => $value) {

                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["non_formal"]["item"])){
                    $item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();
                }

                $tmp_item = array(
                        "nama"=>"Sekolah Luar Biasa", 
                        "n"=>$value->slbn,
                        "s"=>$value->slbs,
                        "a"=>(int)$value->slbn+(int)$value->slbs
                    );
                array_push($item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $tmp_item = array(
                        "nama"=>"Sekolah Dasar Luar Biasa", 
                        "n"=>$value->sdlbn,
                        "s"=>$value->sdlbs,
                        "a"=>(int)$value->sdlbn+(int)$value->sdlbs
                    );
                array_push($item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Pertama Luar Biasa", 
                        "n"=>$value->smplbn,
                        "s"=>$value->smplbs,
                        "a"=>(int)$value->smplbn+(int)$value->smplbs
                    );
                array_push($item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);


                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Luar Biasa", 
                        "n"=>$value->smlbn,
                        "s"=>$value->smlbs,
                        "a"=>(int)$value->smlbn+(int)$value->smlbs
                    );
                array_push($item_kecamatan["khusus"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);
            }
        }

        // $data["option"] = $option;
        // $data["main_data"] = $item_kecamatan;
        $data["data_json"] = json_encode($item_kecamatan);
        $data["data_json_pendidik"] = $this->get_data_local_pendidik();
        $data["data_json_peserta_didik"] = $this->get_data_local_peserta_didik();


        // print_r($item_kecamatan);
        // print_r(json_encode($item_kecamatan));

        $this->load->view("v_halaman_pendidikan", $data);
    }


    public function get_data_local_pendidik(){
        $item_kecamatan = array(
            "all"=>array(
                "keterangan"=>"Pendidikan Dasar",
                "item"=>array()
            )
        );

        $option = " <select class=\"form-control\" name=\"select_kec\" id=\"select_kec\">";

        $data_pendidik = $this->get_jml_satuan_pendidikan("pendidik");

        // print_r($data_pendidik);
        $total = array(
            array(
                "nama"=>"Sekolah Dasar",
                "n"=>0,
                "s"=>0,
                "a"=>0
            ),
            array(
                "nama"=>"Sekolah Menengah Pertama",
                "n"=>0,
                "s"=>0,
                "a"=>0
            ),
            array(
                "nama"=>"Sekolah Menengah Atas",
                "n"=>0,
                "s"=>0,
                "a"=>0
            ),
            array(
                "nama"=>"Sekolah Menengah Kejuruan",
                "n"=>0,
                "s"=>0,
                "a"=>0
            )
        );

        if($data_pendidik){
            $data_array = json_decode($data_pendidik);
            $item_kecamatan["all"]["keterangan"] = $data_array->title_header;

            foreach ($data_array->item as $key => $value) {
                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["all"]["item"])){
                    $item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();

                    $tmp_kode = str_replace(" ", "", $value->kode_wilayah);

                    $option .= "<option value=\"".$tmp_kode."\">".$value->nama."</option>";
                }
                

                $tmp_item = array(
                        "nama"=>"Sekolah Dasar", 
                        "n"=>$value->sdn,
                        "s"=>$value->sds,
                        "a"=>(int)$value->sdn+(int)$value->sds
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[0]["n"] += (int)$value->sdn;
                $total[0]["s"] += (int)$value->sds;
                $total[0]["a"] += (int)$value->sdn+(int)$value->sds;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Pertama", 
                        "n"=>$value->smpn,
                        "s"=>$value->smps,
                        "a"=>(int)$value->smpn+(int)$value->smps
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[1]["n"] += (int)$value->smpn;
                $total[1]["s"] += (int)$value->smps;
                $total[1]["a"] += (int)$value->smpn+(int)$value->smps;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Atas", 
                        "n"=>$value->sman,
                        "s"=>$value->smas,
                        "a"=>(int)$value->sman+(int)$value->smas
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[2]["n"] += (int)$value->sman;
                $total[2]["s"] += (int)$value->smas;
                $total[2]["a"] += (int)$value->sman+(int)$value->smas;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Kejuruan", 
                        "n"=>$value->smkn,
                        "s"=>$value->smks,
                        "a"=>(int)$value->smkn+(int)$value->smks
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[3]["n"] += (int)$value->smkn;
                $total[3]["s"] += (int)$value->smks;
                $total[3]["a"] += (int)$value->smkn+(int)$value->smks;

            }
        }

        $item_kecamatan["all"]["item"]["all"]["nama"] = "Total Teaga Pendidik";
        $item_kecamatan["all"]["item"]["all"]["item"] = $total;


        $option .= "<option value=\"all\">Tampilkan Semua</option>";
        $option .= "</select>";

        // $data["option"] = $option;
        // // $data["main_data"] = $item_kecamatan;
        // $data["data_json"] = json_encode($item_kecamatan);


        // print_r($item_kecamatan);
        // print_r(json_encode($item_kecamatan));

        // $this->load->view("pendidikan/pendidikan_pendidik", $data);

        return json_encode($item_kecamatan);
    }

    public function get_data_local_peserta_didik(){
        $item_kecamatan = array(
            "all"=>array(
                "keterangan"=>"Pendidikan Dasar",
                "item"=>array()
            )
        );

        $option = " <select class=\"form-control\" name=\"select_kec\" id=\"select_kec\">";

        $data_peserta_didik = $this->get_jml_satuan_pendidikan("peserta_didik");

        // print_r($data_pendidik);

        if($data_peserta_didik){
            $data_array = json_decode($data_peserta_didik);
            $item_kecamatan["all"]["keterangan"] = $data_array->title_header;

            $total = array(
                array(
                    "nama"=>"Sekolah Dasar",
                    "n"=>0,
                    "s"=>0,
                    "a"=>0
                ),
                array(
                    "nama"=>"Sekolah Menengah Pertama",
                    "n"=>0,
                    "s"=>0,
                    "a"=>0
                ),
                array(
                    "nama"=>"Sekolah Menengah Atas",
                    "n"=>0,
                    "s"=>0,
                    "a"=>0
                ),
                array(
                    "nama"=>"Sekolah Menengah Kejuruan",
                    "n"=>0,
                    "s"=>0,
                    "a"=>0
                )
            );

            foreach ($data_array->item as $key => $value) {
                if(!array_key_exists($value->kode_wilayah, $item_kecamatan["all"]["item"])){
                    $item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["nama"] = $value->nama;
                    $item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"] = array();

                    $tmp_kode = str_replace(" ", "", $value->kode_wilayah);

                    $option .= "<option value=\"".$tmp_kode."\">".$value->nama."</option>";
                }
                

                $tmp_item = array(
                        "nama"=>"Sekolah Dasar", 
                        "n"=>$value->sdn,
                        "s"=>$value->sds,
                        "a"=>(int)$value->sdn+(int)$value->sds
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[0]["n"] += (int)$value->sdn;
                $total[0]["s"] += (int)$value->sds;
                $total[0]["a"] += (int)$value->sdn+(int)$value->sds;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Pertama", 
                        "n"=>$value->smpn,
                        "s"=>$value->smps,
                        "a"=>(int)$value->smpn+(int)$value->smps
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[1]["n"] += (int)$value->smpn;
                $total[1]["s"] += (int)$value->smps;
                $total[1]["a"] += (int)$value->smpn+(int)$value->smps;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Atas", 
                        "n"=>$value->sman,
                        "s"=>$value->smas,
                        "a"=>(int)$value->sman+(int)$value->smas
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[2]["n"] += (int)$value->sman;
                $total[2]["s"] += (int)$value->smas;
                $total[2]["a"] += (int)$value->sman+(int)$value->smas;



                $tmp_item = array(
                        "nama"=>"Sekolah Menengah Kejuruan", 
                        "n"=>$value->smkn,
                        "s"=>$value->smks,
                        "a"=>(int)$value->smkn+(int)$value->smks
                    );
                array_push($item_kecamatan["all"]["item"][str_replace(" ", "", $value->kode_wilayah)]["item"], $tmp_item);

                $total[3]["n"] += (int)$value->smkn;
                $total[3]["s"] += (int)$value->smks;
                $total[3]["a"] += (int)$value->smkn+(int)$value->smks;

            }
        }

        $item_kecamatan["all"]["item"]["all"]["nama"] = "Total Teaga Pendidik";
        $item_kecamatan["all"]["item"]["all"]["item"] = $total;


        $option .= "<option value=\"all\">Tampilkan Semua</option>";
        $option .= "</select>";

        // $data["option"] = $option;
        // // $data["main_data"] = $item_kecamatan;
        // $data["data_json"] = json_encode($item_kecamatan);


        // print_r($item_kecamatan);
        // print_r(json_encode($item_kecamatan));
        return json_encode($item_kecamatan);
        // $this->load->view("pendidikan/pendidikan_pendidik", $data);
    }
}
